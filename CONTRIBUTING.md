<!-- TOC -->

- [Contributing](#contributing)
- [About VSCode](#about-vscode)

<!-- TOC -->

# Contributing

> Install the required software following the instructions on the [tutorial](REQUIREMENTS.md).

> [OPTIONAL] Configure authentication on your account to use the SSH protocol instead of HTTP. See this [tutorial](https://confluence.atlassian.com/bitbucketserver/ssh-access-keys-for-system-use-776639781.html)

* Clone this repository to your local system:

```bash
git clone -b develop git@bitbucket.org:sensedia/of-sensedia-whitelabel.git;
```

* Create a branch. Example:

```bash
git checkout -b BRANCH_NAME;
```

* Make sure you are on the correct branch using the following command. The branch in use contains the '*' before the name.

```bash
git branch;
```

* Make your changes and tests to the new branch.
* Commit the changes to the branch.
* Push files to repository remote with command:

```bash
git push --set-upstream origin BRANCH_NAME;
```

* Create a Pull Request (PR) to the `develop` branch. See this [tutorial](https://support.atlassian.com/bitbucket-cloud/docs/create-a-pull-request-to-merge-your-change/)
* Update the content with the suggestions of the reviewer (if necessary).
* After your pull request is merged to the `develop` branch, update your local clone:

```bash
git checkout develop;
git pull;
```

* Clean up after your request is merged with command:

```bash
git branch -d BRANCH_NAME;
```

# About VSCode

Use a IDE (Integrated Development Environment) or text editor of your choice. By default, the use of VSCode is recommended.

VSCode (https://code.visualstudio.com), combined with the following plugins, helps the editing/review process, mainly allowing the preview of the content before the commit, analyzing the Markdown syntax and generating the automatic summary, as the section titles are created/changed.

* Markdown-lint: https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint
* Markdown-toc: https://marketplace.visualstudio.com/items?itemName=AlanWalk.markdown-toc
* Markdown-all-in-one: https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one

Additional plugins:

* Gitlens: https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens
* Docker: https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker
* YAML: https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml

Themes for VSCode:

* https://vscodethemes.com/
* https://code.visualstudio.com/docs/getstarted/themes
* https://dev.to/thegeoffstevens/50-vs-code-themes-for-2020-45cc