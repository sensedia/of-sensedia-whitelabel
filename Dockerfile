# Install dependencies only when needed
FROM node:alpine AS deps
RUN apk add --no-cache libc6-compat
WORKDIR /sensedia
COPY package.json ./
RUN yarn install

# Rebuild the source code only when needed
FROM node:alpine AS builder
WORKDIR /sensedia
COPY . .
COPY --from=deps /sensedia/node_modules ./node_modules
RUN yarn build && yarn install --production --ignore-scripts --prefer-offline

# Production image, copy all the files and run next
FROM node:alpine AS runner
WORKDIR /sensedia

# Informations about image
LABEL maintainer="Sensedia" \
      date_create="19/08/2021" \
      version="0.1.0" \
      description="White Label used in Open Banking" \
      licensce="Apache 2.0"

# Environment variable
ENV NEXT_TELEMETRY_DISABLED 0
ENV NODE_ENV production

# Creating group and user
RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001

# You only need to copy next.config.js if you are NOT using the default configuration
COPY --from=builder /sensedia/next.config.js ./
COPY --from=builder /sensedia/next-i18next.config.js ./
COPY --from=builder --chown=nextjs:nodejs /sensedia/.next ./.next
COPY --from=builder /sensedia/node_modules ./node_modules
COPY --from=builder /sensedia/package.json ./package.json
COPY --from=builder /sensedia/.env ./
COPY --from=builder /sensedia/jest.config.js ./
COPY --from=builder /sensedia/setupTests.js ./

# Set it for subsequent commands
USER nextjs

# Expose the app port
EXPOSE 5000

# Starting the service when starting the container
CMD ["yarn", "start", "--port","5000"]