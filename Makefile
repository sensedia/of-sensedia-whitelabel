SENSEDIA_APP_NAME=of-sensedia-whitelabel

run:
	yarn
	yarn start

test:
	yarn
	yarn test:coverage

build:
	yarn
	yarn run build		

image:
	docker build -t ${SENSEDIA_APP_NAME}:latest .