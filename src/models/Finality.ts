export interface Finality {
   displayName: string
   finalityId: string
}
