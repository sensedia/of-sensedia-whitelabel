export interface Brands {
   organisationId: string
   authorisationServerId: string
   customerFriendlyName: string
   customerFriendlyDescription: string
   customerFriendlyLogoUri: string
   registrationNumber: string
   requiresParticipantTermsAndConditionsSigning: boolean
   brands: []
}
export interface Institution {
   organisationId: string
   authorisationServerId: string
   customerFriendlyName: string
   customerFriendlyDescription: string
   customerFriendlyLogoUri: string
   registrationNumber: string
   requiresParticipantTermsAndConditionsSigning: boolean
   brands: Array<Brands> | []
}
export interface Organisation {
   organisationId: string
   organisationName: string
}
