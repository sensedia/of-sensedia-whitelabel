export interface SharedData {
   dataSumUp: dataInfo[]
   mandatoryData: ParentData[]
   optionalData?: ParentData[]
}

export interface ParentData {
   title: string
   dataEntries: GenericData[]
}

export interface GenericData {
   title: string
   description: string
   value?: string
   selected: boolean
}

export interface dataInfo {
   label: string
   obs: string
}
