import { LoggedUser } from '@services'
import { Organisation } from './Institutions'
import { Finality } from './Finality'
import { User } from './User'

export interface ShareDetails {
   abstract: ShareAbstract
   id: number
   address: Address
   accountType: string
   status: string
   date: string
   bank: string
   name: string
   document: Document
   age: number
   gender: string
   birthday: string
   marital: string
   email: string
   phone: string
   schoolar: string
}

export interface ShareAbstract {
   identification: {
      rel: string
      document: string
   }
   transmitter: string
   goal: string
   confirmationDate: string
   sharingTerm: string
}

export interface Address {
   street: string
   number: string
   city: string
   uf: string
   addon: string
   zip: string
}

export interface Document {
   identification: string
   rel: string
}

export interface AdditionalInfo {
   key: string
   value: string
}

export interface DeadLine {
   total: number
   type: string
}

export interface DataPermission {
   detail: string
   displayName: string
   items: Array<string>
   permissionCode: string
   required: boolean
   selected?: boolean
}

export interface Resource {
   resourceId: string
   status: string
   type: string
   displayName: string
   detail: string
   additionalInfos: Array<AdditionalInfo>
}

export interface ResourceGroup {
   resourceGroupId: string
   dataPermissions: Array<DataPermission>
   displayName: string
   additionalInfos: Array<AdditionalInfo>
   items: Array<string>
   type: string
   resources: Array<Resource>
}

export interface AuthorisationServer {
   openIDDiscoveryDocument: string
   customerFriendlyName: string
   customerFriendlyLogoUri: string
   developerPortalUri: string
   organisationId: string
   payloadSigningCertLocationUri: string
   parentAuthorisationServerId: string
   notificationWebhook: string
   notificationWebhookStatus: string
   customerFriendlyDescription: string
   termsOfServiceUri: string
   autoRegistrationSupported: boolean
   authorisationServerId: string
}

export interface Approvers {
   status: string
   approverId: string
   additionalInfos: Array<AdditionalInfo>
   isConsentOwner: boolean
}

export type ShareType = 'TRANSMITTING' | 'RECEIVING'

export interface Share {
   businessEntity: {
      document: Document
   }
   consentId: string
   shareId: string
   authorisationServer: AuthorisationServer
   organisationId: string
   authorisationServerId: string
   createDateTime: string
   expirationDateTime: string
   loggedUser: {
      document: Document
   }
   resourceGroups: Array<ResourceGroup>
   shareType: ShareType
   status: string
   additionalInfos: Array<AdditionalInfo>
   finality: Finality
   deadLines: Array<DeadLine>
}

export interface ShareTransmitted {
   approvers: Array<Approvers>
   businessEntity: {
      document: Document
   }
   consentId: string
   shareId: string
   authorisationServer: AuthorisationServer
   organisationId: string
   organisationName: string
   authorisationServerId: string
   createDateTime: string
   expirationDateTime: string
   loggedUser: {
      document: Document
   }
   resourceGroups: Array<ResourceGroup>
   shareType: ShareType
   status: string
   additionalInfos: Array<AdditionalInfo>
   deadLines: Array<DeadLine>
   organisation: Organisation
   finality: Finality
}

export interface NewShare {
   authorisationServer: {
      organisationId: string
      authorisationServerId: string
   }
   finality: Finality
   businessEntity?: {
      document: {
         identification: string
         rel: string
      }
   }
   loggedUser: LoggedUser
   additionalInfos?: [
      {
         key: string
         value: string
      },
   ]
}

export interface NewShareResponse {
   businessEntity: {
      document: Document
   }
   consentId?: string
   shareId: string
   authorisationServer: AuthorisationServer
   createDateTime: string
   expirationDateTime?: string
   loggedUser: LoggedUser
   resourceGroups: Array<ResourceGroup>
   shareType: ShareType
   status: string
   additionalInfos?: Array<AdditionalInfo>
   finality: Finality
   deadLines: Array<DeadLine>
}

export interface NewRedirectResponse {
   redirect_uri: string
}

export enum Operation {
   UPDATE = 'UPDATE',
   RENEW = 'RENEW',
}
export interface AlterConsentPayload {
   additionalInfos?: Array<AdditionalInfo>
   businessEntity?: {
      document: Document
   }
   finality?: Finality
   loggedUser?: User
   operation?: Operation
   shareData: {
      deadLine: DeadLine
      dataPermissions: Array<DataPermission>
      redirectUri?: string
   }
}
