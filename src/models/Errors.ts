export interface Error {
   code: string
   details: string
   title: string
}

export interface ResponseError {
   errors: Error[]
}
