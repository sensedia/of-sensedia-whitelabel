import { AdditionalInfo, User } from '@models'

export enum ConsentStatus {
   AwaitingAuthorisation = 'AWAITING_AUTHORISATION',
   Authorised = 'AUTHORISED',
   Rejected = 'REJECTED',
   Consumed = 'CONSUMED',
}

export enum PersonType {
   PessoaNatural = 'PESSOA_NATURAL',
   PessoaJuridica = 'PESSOA_JURIDICA',
}

export enum AccountType {
   CACC = 'CACC',
   SLRY = 'SLRY',
   SVGS = 'SVGS',
   TRAN = 'TRAN',
}

export interface DebtorAccount {
   ispb: string
   issuer: string
   number: string
   accountType: AccountType
   cpfCnpj?: string
   name?: string
   checkDigit?: string
}

export interface BankAccount extends DebtorAccount {
   amount: string
   accountId: string
}

export interface Payment {
   loggedUser: User
   businessEntity: {
      document: {
         identification: string
         rel: string
      }
   }
   creditor: {
      personType: PersonType
      cpfCnpj: string
      name: string
   }
   payment: {
      type: string
      date: string
      currency: string
      amount: string
   }
   debtorAccount: DebtorAccount
   redirectUri?: string
}

export interface PaymentConsent extends Payment {
   consentId: string
   creationDateTime: string
   expirationDateTime: string
   statusUpdateDateTime: string
   status: ConsentStatus
   organisationName: string
   organisationId: string
}
export interface NewPayment extends Payment {
   additionalInfos: Array<AdditionalInfo>
   authorisationServer: {
      organisationId: string
      authorisationServerId: string
      organisationName: string
   }
   localInstrument: string
   proxy: string
   qrCode: string
   remittanceInformation: string
}

export interface PaymentEffectuation extends Payment {
   additionalInfos: Array<AdditionalInfo>
   authorisationServer: {
      authorisationServerId: string
      organisationId: string
      organisationName: string
   }
   consentId: string
   creationDateTime: string
   creditorAccount: {
      ispb: string
      issuer: string
      number: string
      accountType: string
   }
   endToEndId: string
   links: {
      self: string
   }
   localInstrument: string
   meta: {
      totalRecords: 1
      totalPages: 1
      requestDateTime: string
   }
   paymentId: string
   proxy: string
   rejectionReason: string
   remittanceInformation: string
   status: string
   statusUpdateDateTime: string
}
export interface DataPayment {
   payment: {
      amount: string
      currency: string
   }
   identification: {
      company: string
      cpfCnpj: string
      name: string
   }
   receiverData: {
      cpfCnpj: string
      name: string
   }
   debtorAccount: DebtorAccount
}
