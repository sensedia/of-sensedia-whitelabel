export interface AuthModel {
   account: string
   agency: string
   password: string
}
