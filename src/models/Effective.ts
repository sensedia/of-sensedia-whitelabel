import { ShareTransmitted } from './Shares'

export interface DataPeople {
   document: Document
   name: string
}

interface Document {
   identification: string
   rel: string
}

export interface EffectiveModel extends ShareTransmitted {}
