export interface User {
   document: {
      identification: string
      rel: string
   }
   name: string
   email: string
}
