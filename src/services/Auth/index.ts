import Axios, { AxiosResponse } from 'axios'
import { AuthModel } from '@models'
import { FAKE_API } from '../../appConstants'

export class AuthService {
   static auth(authParams: AuthModel): Promise<AxiosResponse> {
      return Axios.post(`${FAKE_API}/auth`, authParams)
   }
}
