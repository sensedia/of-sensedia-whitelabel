import Axios, { AxiosResponse } from 'axios'
import { Institution } from '@models'
import { FAKE_API, JOURNEY_TPP } from '../../appConstants'

export class Institutions {
   static get(): Promise<AxiosResponse<Institution[]>> {
      return Axios.get(`${JOURNEY_TPP}/organisations`)
   }

   static getInstitution(id: any): Promise<AxiosResponse<Institution>> {
      return Axios.get(`${FAKE_API}/institutions/${id}`)
   }

   static getParticipants(): Promise<AxiosResponse<Institution[]>> {
      return Axios.get(`${FAKE_API}/participants`)
   }
}
