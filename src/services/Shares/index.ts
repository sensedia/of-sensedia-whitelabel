import Axios, { AxiosResponse } from 'axios'
import {
   Share,
   NewShare,
   NewShareResponse,
   NewRedirectResponse,
   ShareTransmitted,
   Operation,
} from '@models'
import {
   JOURNEY_TPP_API,
   JOURNEY_TPP,
   JOURNEY_ASPSP_API,
   JOURNEY_ASPSP,
} from '../../appConstants'
export class Shares {
   static get(): Promise<AxiosResponse<Array<Share>>> {
      return Axios.get(`${JOURNEY_TPP_API}/shares`)
   }

   static getDetails(id: string): Promise<AxiosResponse<Share>> {
      return Axios.get(`${JOURNEY_TPP}/shares/${id}`)
   }

   static getByUser(
      userIdentification: string,
   ): Promise<AxiosResponse<Array<Share>>> {
      return Axios.get(`${JOURNEY_TPP}/shares?shareType=RECEIVING`, {
         headers: { 'x-user-identification': userIdentification },
      })
   }

   static create(
      newShareData: NewShare,
   ): Promise<AxiosResponse<NewShareResponse>> {
      return Axios.post(`${JOURNEY_TPP}/shares`, newShareData)
   }

   static update(
      id: string,
      shareData: any,
   ): Promise<AxiosResponse<NewRedirectResponse>> {
      return Axios.patch(`${JOURNEY_TPP}/shares/${id}`, shareData)
   }

   static finish(id: string): Promise<AxiosResponse> {
      return Axios.delete(`${JOURNEY_TPP}/shares/${id}`)
   }

   static getByConsentId(id: string): Promise<AxiosResponse<ShareTransmitted>> {
      return Axios.get(`${JOURNEY_ASPSP}/shares/consents/${id}`)
   }

   static getDetailsTransmitter(
      id: string,
   ): Promise<AxiosResponse<ShareTransmitted>> {
      return Axios.get(`${JOURNEY_ASPSP_API}/shares/${id}`)
   }

   static confirmationShareTransmitter(
      id: string,
   ): Promise<AxiosResponse<ShareTransmitted>> {
      return Axios.put(`${JOURNEY_ASPSP}/shares/${id}/confirmations`)
   }

   static alterShare(
      shareId: string,
      operation: Operation,
   ): Promise<AxiosResponse<NewShareResponse>> {
      return Axios.put(
         `${JOURNEY_TPP}/shares/${shareId}?operation=${operation}`,
         {},
      )
   }

   static alterConsent(shareId: string, data: any): Promise<AxiosResponse> {
      return Axios.put(`${JOURNEY_TPP}/shares/${shareId}/consents`, data)
   }

   static confirmationResource(
      shareId: string,
      data: any,
   ): Promise<AxiosResponse> {
      return Axios.put(`${JOURNEY_ASPSP}/shares/${shareId}/resources`, data)
   }
}
