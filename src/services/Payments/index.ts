import Axios, { AxiosResponse } from 'axios'
import {
   Payment,
   DataPayment,
   PaymentEffectuation,
   PaymentConsent,
} from '@models'
import {
   JOURNEY_ASPSP_API,
   JOURNEY_TPP_API,
   SANDBOX_API,
} from '../../appConstants'

interface EffectuationParams {
   authorizationCode: string
   requestId: string
}
export class Payments {
   static getByConsentId(id: string): Promise<AxiosResponse<PaymentConsent>> {
      return Axios.get(`${JOURNEY_ASPSP_API}/payments/consents/${id}`)
   }

   static approve(id: string, data: Pick<Payment, 'debtorAccount'>) {
      return Axios.patch(
         `${JOURNEY_ASPSP_API}/payments/consents/${id}/authorised`,
         data,
      )
   }

   static getByUser(
      userIdentification: string,
   ): Promise<AxiosResponse<PaymentConsent[]>> {
      return Axios.get(`${JOURNEY_ASPSP_API}/payments/consents`, {
         headers: { 'x-user-identification': userIdentification },
      })
   }

   static post(params: Payment): Promise<AxiosResponse<Payment>> {
      return Axios.post(`${JOURNEY_TPP_API}/payments/consents`, params)
   }

   static effectuationPost(
      params: EffectuationParams,
   ): Promise<AxiosResponse<PaymentEffectuation>> {
      return Axios.post(`${JOURNEY_TPP_API}/payments/pix/payments`, params)
   }

   static getDataPayment(): Promise<AxiosResponse<DataPayment>> {
      return Axios.get(`${SANDBOX_API}/data-payment`)
   }
}
