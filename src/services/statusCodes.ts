export type statusCode = {
   ok: 200
   created: 201
   noContent: 204
   badRequest: 400
   unauthorized: 401
   forbidden: 403
   notFound: 404
   methodNotAllowed: 405
   notAcceptable: 406
   tooManyRequests: 429
   serverError: 500
}
