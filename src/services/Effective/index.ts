import { EffectiveModel } from '@models'
import { JOURNEY_TPP } from '@constants'
import Axios, { AxiosResponse } from 'axios'
export interface EffectiveServiceParams {
   requestId: string
   authorizationCode: string
}
export class EffectiveService {
   static getShared(
      params: EffectiveServiceParams,
   ): Promise<AxiosResponse<EffectiveModel>> {
      return Axios.post(`${JOURNEY_TPP}/shares/consents/confirmations/`, params)
   }
}
