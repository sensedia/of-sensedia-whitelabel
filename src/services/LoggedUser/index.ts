import { SANDBOX_API } from '@constants'
import { Finality, User as Model } from '@models'
import Axios, { AxiosResponse } from 'axios'

export class LoggedUser {
   static get(): Promise<AxiosResponse<Model>> {
      return Axios.get(`${SANDBOX_API}/logged-user`)
   }

   static getFinality(): Promise<AxiosResponse<Finality>> {
      return Axios.get(`${SANDBOX_API}/logged-user/finality`)
   }
}
