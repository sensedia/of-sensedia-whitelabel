import { ACCOUNTS_API } from '@constants'
import { BankAccount } from '@models'
import Axios, { AxiosResponse } from 'axios'

export class Accounts {
   static get(): Promise<AxiosResponse<BankAccount[]>> {
      return Axios.get(ACCOUNTS_API)
   }
}
