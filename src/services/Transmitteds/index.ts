import Axios, { AxiosResponse } from 'axios'
import { Share, ShareTransmitted } from '@models'
import { JOURNEY_ASPSP_API, JOURNEY_ASPSP } from '../../appConstants'

export class Transmitteds {
   static get(): Promise<AxiosResponse<Array<ShareTransmitted>>> {
      return Axios.get(`${JOURNEY_ASPSP_API}/shares`)
   }

   static getByUser(
      userIdentification: string,
   ): Promise<AxiosResponse<Array<ShareTransmitted>>> {
      return Axios.get(`${JOURNEY_ASPSP}/shares?shareType=TRANSMITTING`, {
         headers: { 'x-user-identification': userIdentification },
      })
   }

   static getDetails(
      id: string,
   ): Promise<AxiosResponse<Share & ShareTransmitted>> {
      return Axios.get(`${JOURNEY_ASPSP}/shares/${id}`)
   }

   static finish(id: string): Promise<AxiosResponse> {
      return Axios.delete(`${JOURNEY_ASPSP}/shares/${id}`)
   }
}
