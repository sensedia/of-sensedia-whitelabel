import { Text, Divider, Box } from '@chakra-ui/react'
import { LogoObp } from '../components/LogoOpb'
import { useTranslation } from 'react-i18next'
import Link from 'next/link'
import {
   FlagIcon,
   EditIcon,
   CardIcon,
   ModalComponent,
   IconTextBox,
   TextBox,
} from '@components'
import { openBankingSite } from '@constants'
import { OpbInfo } from '../components/OpbInfo'
import { Terms } from '../components/Terms'

function Index() {
   const { t } = useTranslation()

   return (
      <Box bg="gray.50" padding="1rem 1.5rem">
         <LogoObp />

         <Link href="/compartilhamentos">
            <a>
               <IconTextBox
                  content={t('home.button.shared')}
                  icon={<FlagIcon boxSize={10} color="white" />}
               />
            </a>
         </Link>

         <Link href="/pagamentos">
            <a>
               <IconTextBox
                  content={t('home.button.transactions')}
                  icon={<CardIcon boxSize={10} color="white" />}
               />
            </a>
         </Link>

         <Link href="/novo-compartilhamento">
            <a>
               <IconTextBox
                  content={t('home.button.new')}
                  icon={<EditIcon boxSize={10} color="white" />}
               />
            </a>
         </Link>

         <Divider margin="1.5rem 0" borderColor="gray.500" />

         <ModalComponent
            activator={<TextBox content={t('home.button.opbinfo')} />}
            footerButtons={[
               {
                  caption: t('default.word.continue'),
                  close: true,
               },
            ]}
         >
            <OpbInfo></OpbInfo>
         </ModalComponent>

         <ModalComponent
            activator={<TextBox content={t('home.button.terms')} />}
            header={t('home.popup.terms.title')}
         >
            <Terms></Terms>
         </ModalComponent>

         <Link href={openBankingSite}>
            <a>
               <Text
                  cursor="pointer"
                  textDecoration="underline"
                  fontWeight="700"
                  margin="3rem 0"
                  padding="0 1.25rem"
                  fontSize="md"
                  color="brand.600"
                  textAlign="center"
               >
                  {t('home.link.citzen')}
               </Text>
            </a>
         </Link>
      </Box>
   )
}

export default Index
