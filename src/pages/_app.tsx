import { ChakraProvider } from '@chakra-ui/react'
import { Global } from '@emotion/react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { I18nextProvider } from 'react-i18next'

import theme from '../styles/theme'
import { AppProps } from 'next/app'
import { GlobalStyles } from '../styles/GlobalStyles'
import i18n from '../locales/i18nt'
import { Layout } from '../components'

function MyApp({ Component, pageProps }: AppProps) {
   const queryProvider = new QueryClient()

   return (
      <I18nextProvider i18n={i18n}>
         <ChakraProvider resetCSS theme={theme}>
            <QueryClientProvider client={queryProvider}>
               <Layout>
                  <Component {...pageProps} />
               </Layout>
            </QueryClientProvider>
            <Global styles={GlobalStyles} />
         </ChakraProvider>
      </I18nextProvider>
   )
}

export default MyApp
