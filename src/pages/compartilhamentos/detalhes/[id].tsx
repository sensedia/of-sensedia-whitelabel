import React from 'react'
import { useEffect, useState } from 'react'
import { LoggedUser, Shares, Transmitteds } from '@services'
import { useRouter } from 'next/router'
import { useQuery } from 'react-query'
import { useUpdateEffect } from '@hooks'
import { Document } from '@models'
import { useTranslation } from 'react-i18next'
import { ReceivedShare, TransmittedShare } from '@components'
import { Box, useToast, Stack, Text, Collapse } from '@chakra-ui/react'
import { PageActions, PageHeader } from '@pages-components/compartilhamentos'
import { SharedDetailsPageProvider } from '@pages-components/compartilhamentos/context'
import { ChevronDownIcon } from '@chakra-ui/icons'
import { CollapsableContainer } from '@components/CollapsableContainer'
import { CardInfo } from '@components/CardInfo'

function CompartilhamentoDetails() {
   const toast = useToast()
   const router = useRouter()
   const { t } = useTranslation()
   const [showSharedData, setShowSharedData] = useState(false)

   const { id, ['shared-type']: sharedType } = router.query

   const {
      data: details,
      isLoading,
      refetch,
      isError,
   } = useQuery('details', getDetails)

   const { data: user, isLoading: isLoadingUser } = useQuery(
      'loggedUser',
      LoggedUser.get,
   )

   function getDetails() {
      if (!router.isReady) {
         return
      }
      if (sharedType === 'RECEIVING') {
         return Shares.getDetails(id as '')
      }
      return Transmitteds.getDetails(id as '')
   }

   useUpdateEffect(() => {
      refetch()
   }, [router.isReady])

   useEffect(() => {
      if (isError) {
         launchToast()
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [isError])

   function launchToast() {
      return toast({
         title: t('default.request.error'),
         status: 'error',
         isClosable: true,
      })
   }

   return (
      <Box>
         <SharedDetailsPageProvider
            value={{ details: details?.data as any, isLoading }}
         >
            <Stack p="1.1rem">
               <PageHeader />
            </Stack>
            <Stack backgroundColor="gray.100">
               <Box p="1.1rem">
                  {sharedType === 'RECEIVING' ? (
                     <ReceivedShare
                        finality={details?.data.finality.displayName}
                        document={user?.data.document as Document}
                        name={user?.data.name as ''}
                        deadline={details?.data.deadLines[0].total}
                        expirationDateTime={details?.data.expirationDateTime}
                        confirmationDate={details?.data.createDateTime}
                        isLoading={isLoading || isLoadingUser}
                     />
                  ) : (
                     <TransmittedShare
                        identification={{
                           cpf: user?.data.document.identification as string,
                           name: user?.data.name as string,
                        }}
                        expirationDateTime={details?.data.expirationDateTime}
                        confirmationDate={details?.data.createDateTime}
                        isLoading={isLoading || isLoadingUser}
                     />
                  )}
               </Box>
               <Box>
                  <Stack>
                     <Stack
                        align="center"
                        borderBottomWidth=".1rem"
                        borderColor="gray.400"
                        direction="row"
                        justify="space-between"
                        onClick={() => setShowSharedData(!showSharedData)}
                        px="1.1rem"
                     >
                        <Text color="gray.900" fontSize="2xl" fontWeight={700}>
                           {t('shared.page.share.details.abstract.shared.data')}
                        </Text>
                        <Box
                           transform={`rotate(${showSharedData ? -180 : 0}deg)`}
                           transitionDuration=".3s"
                        >
                           <ChevronDownIcon color="brand.600" w={10} h={10} />
                        </Box>
                     </Stack>
                     <Collapse
                        style={{ marginTop: 0 }}
                        in={showSharedData}
                        animateOpacity={false}
                        unmountOnExit={true}
                     >
                        {details &&
                           details.data.resourceGroups.map((rg) => (
                              <CollapsableContainer
                                 key={rg.resourceGroupId}
                                 title={rg.displayName}
                              >
                                 {rg.dataPermissions.map((dp, index) => (
                                    <CardInfo
                                       key={index}
                                       title={dp.displayName}
                                       body={dp.detail}
                                       itemDescription={dp.displayName}
                                       scrollBoxBody={
                                          dp.items &&
                                          dp.items.map((item) => ({
                                             label: item,
                                          }))
                                       }
                                    />
                                 ))}
                              </CollapsableContainer>
                           ))}
                     </Collapse>
                  </Stack>
               </Box>
               <PageActions />
            </Stack>
         </SharedDetailsPageProvider>
      </Box>
   )
}

export default CompartilhamentoDetails
