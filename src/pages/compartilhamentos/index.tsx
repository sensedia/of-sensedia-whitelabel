import { useTranslation } from 'react-i18next'
import { useQuery } from 'react-query'
import { Shares, Transmitteds, LoggedUser } from '@services'
import { sortSharesByDate } from '@utils'
import { BackButton, CardShareSkeleton, CardShare } from '@components'
import {
   Text,
   Stack,
   useToast,
   Box,
   Link as LinkStyle,
   Tab,
   TabList,
   TabPanel,
   TabPanels,
   Tabs,
} from '@chakra-ui/react'

function CompartilhamentosPage() {
   const { t } = useTranslation()
   const toast = useToast()
   const userQuery = useQuery('loggedUser', LoggedUser.get)
   const shareQuery = useQuery(
      'sharesByUser',
      () =>
         Shares.getByUser(
            userQuery.data?.data.document.identification.replace(
               /[^Z0-9 ]/g,
               '',
            ) as string,
         ).then((res) => ({ ...res, data: sortSharesByDate(res.data) })),
      {
         enabled: !!userQuery.data?.data.document.identification,
         onError: () =>
            toast({
               title: t('shared.page.share.request.error'),
               status: 'error',
               isClosable: true,
            }),
      },
   )
   const transmittedQuery = useQuery(
      'transmittedData',
      () =>
         Transmitteds.getByUser(
            userQuery.data?.data.document.identification.replace(
               /[^Z0-9 ]/g,
               '',
            ) as string,
         ).then((res) => ({ ...res, data: sortSharesByDate(res.data) })),
      {
         enabled: !!userQuery.data?.data.document.identification,
         onError: () =>
            toast({
               title: t('shared.page.share.request.error'),
               status: 'error',
               isClosable: true,
            }),
      },
   )

   return (
      <Box height="100%">
         <Stack p="1.25rem">
            <BackButton path="/" />
            <Text
               p="0.625rem"
               fontSize="2xl"
               color="gray.900"
               fontWeight="bold"
            >
               {t('shared.page.title')}
            </Text>
         </Stack>
         <Stack height="100%" backgroundColor="gray.100">
            <Tabs width="100%">
               <TabList
                  display="flex"
                  justifyContent="center"
                  backgroundColor="white"
               >
                  <Tab width="calc(50% - 1.875rem)">
                     {t('shared.page.received')}
                  </Tab>
                  <Tab width="calc(50% - 1.875rem)">
                     {t('shared.page.transmitted')}
                  </Tab>
               </TabList>
               <TabPanels backgroundColor="gray.100" p="0.625rem">
                  <TabPanel height="100%">
                     {(shareQuery.isLoading || shareQuery.error) && (
                        <>
                           <CardShareSkeleton />
                           <CardShareSkeleton />
                           <CardShareSkeleton />
                        </>
                     )}
                     {shareQuery.data &&
                        shareQuery.data.data.map((data, key) => (
                           <CardShare
                              id={data.shareId}
                              date={data.createDateTime}
                              key={key}
                              status={data.status}
                              title={
                                 data.authorisationServer?.customerFriendlyName
                              }
                              type={data.shareType}
                           />
                        ))}
                     <LinkStyle>
                        <Text
                           color="brand.600"
                           fontWeight="bold"
                           mt="4rem"
                           textDecoration="underline"
                           textAlign="center"
                        >
                           {t('shared.page.viewAll')}
                        </Text>
                     </LinkStyle>
                  </TabPanel>
                  <TabPanel height="100%">
                     {(transmittedQuery.isLoading ||
                        transmittedQuery.error) && (
                        <>
                           <CardShareSkeleton />
                           <CardShareSkeleton />
                           <CardShareSkeleton />
                        </>
                     )}
                     {transmittedQuery.data &&
                        transmittedQuery.data.data.map((data, key) => (
                           <CardShare
                              id={data.shareId}
                              date={data.createDateTime}
                              key={key}
                              status={data.status}
                              title={data.organisationName}
                              type={data.shareType}
                           />
                        ))}
                  </TabPanel>
               </TabPanels>
            </Tabs>
         </Stack>
      </Box>
   )
}

export default CompartilhamentosPage
