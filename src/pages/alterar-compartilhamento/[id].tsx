/* eslint-disable no-unused-vars */
import { useState } from 'react'

import { Step01, Step02 } from 'pages-components/alterar-compartilhamento/index'

import {
   AlterConsentPayload,
   Finality,
   Institution,
   Operation,
   User,
} from '@models'

import {
   Box,
   Stack,
   Text,
   Flex,
   TabList,
   Tab,
   Tabs,
   TabPanels,
   TabPanel,
   useToast,
} from '@chakra-ui/react'
import { BackButton } from '@components'

import { useTranslation } from 'react-i18next'
import {
   AlterShareContextState,
   StepsAlterShareProvider,
} from '@pages-components/alterar-compartilhamento/context'
import { useQuery } from 'react-query'
import { LoggedUser } from 'services/LoggedUser'
import { useEffect } from 'react'
import { Shares } from '@services'
import { useRouter } from 'next/router'
import { useUpdateEffect } from '@hooks'

enum Steps {
   One = 0,
   Two = 1,
}

export default function AlterarCompartilhamento() {
   const { t } = useTranslation()
   const toast = useToast()
   const { data: userData } = useQuery('loggedUser', LoggedUser.get)
   const { data: finalityData } = useQuery('finality', LoggedUser.getFinality)

   const [stepActive, setStepActive] = useState<Steps>(Steps.One)
   const [urlRedirect, setUrlRedirect] = useState<string>('')

   const router = useRouter()
   const { id, operation } = router.query

   function checkOperation() {
      if (operation === 'RENEW') {
         return Operation.RENEW
      }
      return Operation.UPDATE
   }

   const [state, setState] = useState<AlterShareContextState>({
      loggedUser: {} as User,
      finality: {} as Finality,
      deadline: 6,
      institution: {} as Institution,
   })

   const {
      data: shareDetails,
      isLoading,
      refetch,
      isError,
   } = useQuery('shareDetails', getShareDetails)

   function getShareDetails() {
      if (!router.isReady) {
         return
      }
      return Shares.alterShare(id as string, checkOperation())
   }

   useUpdateEffect(() => {
      refetch()
   }, [router.isReady])

   useEffect(() => {
      if (isError) {
         launchErrorToast()
         router.push(`/compartilhamentos/detalhes/${id}?shared-type=RECEIVING`)
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [isError])

   useEffect(() => {
      setState((state) => ({
         ...state,
         NewShareResponse: shareDetails?.data,
      }))
   }, [shareDetails])

   function launchErrorToast() {
      return toast({
         title: t('default.request.error'),
         status: 'error',
         isClosable: true,
      })
   }

   useEffect(() => {
      if (userData?.data) {
         setState((state) => ({ ...state, loggedUser: userData.data }))
      }
   }, [userData])

   useEffect(() => {
      if (finalityData?.data) {
         setState((state) => ({ ...state, finality: finalityData.data }))
      }
   }, [finalityData])

   function confirmShare({ shareData }: AlterConsentPayload) {
      const body = {
         additionalInfos: state.NewShareResponse?.additionalInfos,
         businessEntity: state.NewShareResponse?.businessEntity,
         finality: state.NewShareResponse?.finality,
         loggedUser: state.NewShareResponse?.loggedUser,
         operation: checkOperation(),
         shareData: {
            deadLine: shareData.deadLine,
            dataPermissions: shareData.dataPermissions,
            redirectUri: null,
         },
      }

      state.NewShareResponse?.shareId &&
         Shares.alterConsent(state.NewShareResponse?.shareId, body)
            .then((response) => {
               console.log(response.data.redirect_uri)
               setUrlRedirect(response.data.redirect_uri)
               setStepActive(Steps.Two)
            })
            .catch(() => {
               toast({
                  title: t('default.request.error'),
                  status: 'error',
                  isClosable: true,
               })
            })
   }

   return (
      <>
         <StepsAlterShareProvider value={[state, setState]}>
            <Box height="100%" backgroundColor="white">
               <Stack p="1.25rem 1.25rem 0.313rem 1.25rem">
                  <Flex justifyContent="space-between" alignContent="center">
                     <BackButton path="/compartilhamentos" />
                     <Tabs
                        display="flex"
                        alignItems="center"
                        index={stepActive}
                        onChange={(index) => setStepActive(index)}
                     >
                        <TabList border="0" justifyContent="space-around">
                           <Tab
                              padding="0"
                              width="0.5rem"
                              height="0.5rem"
                              borderRadius="0.5rem"
                              marginRight="0.5rem"
                              bg="brand.300"
                              isDisabled={stepActive !== Steps.One}
                              _selected={{ bg: 'brand.600', width: '2.125rem' }}
                           ></Tab>
                           <Tab
                              padding="0"
                              marginRight="0.625rem"
                              width="0.5rem"
                              height="0.5rem"
                              borderRadius="0.5rem"
                              bg="brand.300"
                              isDisabled={stepActive !== Steps.Two}
                              _selected={{ bg: 'brand.600', width: '2.125rem' }}
                           ></Tab>
                        </TabList>
                     </Tabs>
                  </Flex>
                  <Text
                     p="0.625rem"
                     fontSize="2xl"
                     color="gray.900"
                     fontWeight="bold"
                  >
                     {checkOperation() === Operation.RENEW
                        ? t('renew.share.page.title')
                        : t('alter.share.page.title')}
                  </Text>
               </Stack>
               <Stack backgroundColor="white">
                  <Tabs index={stepActive}>
                     <TabPanels>
                        <TabPanel padding={0}>
                           <Step01
                              isLoading={isLoading}
                              confirmShare={(data) => confirmShare(data)}
                              operation={checkOperation()}
                           ></Step01>
                        </TabPanel>
                        <TabPanel>
                           <Step02 urlRedirect={urlRedirect}></Step02>
                        </TabPanel>
                     </TabPanels>
                  </Tabs>
               </Stack>
            </Box>
         </StepsAlterShareProvider>
      </>
   )
}
