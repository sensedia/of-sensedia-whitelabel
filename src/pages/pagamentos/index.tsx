import Link from 'next/link'
import { useTranslation } from 'react-i18next'
import { BackButton } from '@components'
import {
   Text,
   Stack,
   Box,
   Flex,
   Divider,
   useToast,
   Skeleton,
} from '@chakra-ui/react'
import { useQuery } from 'react-query'
import { LoggedUser, Payments } from '@services'
import { formatCurrency, formatDateWithHour } from '@utils'
import { ConsentStatus } from '@models'

export default function Pagamentos() {
   const { t } = useTranslation()
   const toast = useToast()
   const {
      data: userData,
      isLoading: userLoading,
      isError: userError,
   } = useQuery('loggedUser', LoggedUser.get)
   const { data, isLoading, isError } = useQuery(
      'pendingPayments',
      () =>
         Payments.getByUser(
            userData?.data.document.identification.replace(
               /[^Z0-9 ]/g,
               '',
            ) as string,
         ).then((res) => ({
            ...res,
            data: res.data.sort((a, b) =>
               a.creationDateTime > b.creationDateTime
                  ? -1
                  : a.creationDateTime < b.creationDateTime
                  ? 1
                  : 0,
            ),
         })),
      {
         enabled: !!userData?.data.document.identification,
         onError: () =>
            toast({
               title: t('shared.page.share.request.error'),
               status: 'error',
               isClosable: true,
            }),
      },
   )

   const showLoading = isLoading || isError || userLoading || userError

   return (
      <Box height="100%" bg="gray.200">
         <Box p={6} pt={4} bg="gray.50">
            <Flex
               alignItems="center"
               justifyContent="space-between"
               h={10}
               ml={-1}
            >
               <BackButton path="/" />
            </Flex>
            <Text fontSize="2xl" color="gray.900" fontWeight="bold" mt={2}>
               {t('payments.page.title')}
            </Text>
         </Box>
         <Stack p={6} spacing={4} bg="gray.200">
            {showLoading && (
               <>
                  <PaymentSkeletonCard />
                  <PaymentSkeletonCard />
               </>
            )}
            {data &&
               (!!data.data.length ? (
                  data.data.map((payment) => (
                     <Link
                        href={`pagamentos/detalhes/${payment.consentId}`}
                        passHref
                        key={payment.consentId}
                     >
                        <Box
                           p={4}
                           borderRadius=".5rem"
                           bg="white"
                           shadow="lg"
                           cursor="pointer"
                        >
                           <Flex justify="space-between" mb={2}>
                              <Text fontSize="xs" fontWeight={700}>
                                 {t('payment.card.status')}
                              </Text>
                              <Text
                                 fontSize="xs"
                                 fontWeight={700}
                                 color="gray.600"
                              >
                                 {formatDateWithHour(payment.creationDateTime)}
                              </Text>
                           </Flex>
                           <Text
                              fontSize="sm"
                              fontWeight={700}
                              color={
                                 payment.status === ConsentStatus.Consumed
                                    ? 'success'
                                    : payment.status ===
                                      ConsentStatus.AwaitingAuthorisation
                                    ? 'warning'
                                    : payment.status ===
                                      ConsentStatus.Authorised
                                    ? 'warning'
                                    : 'gray.600'
                              }
                           >
                              {t(`payment.status.${payment.status}`)}
                           </Text>
                           <Divider my={4} />
                           <Text fontWeight={700} color="gray.900">
                              {payment.organisationName}
                           </Text>
                           <Text
                              fontSize="2xl"
                              fontWeight={700}
                              color="gray.900"
                           >
                              {formatCurrency(payment.payment.amount)}
                           </Text>
                        </Box>
                     </Link>
                  ))
               ) : (
                  <Text
                     fontSize="lg"
                     fontWeight={700}
                     color="gray.700"
                     align="center"
                  >
                     {t('payments.page.no.payments')}
                  </Text>
               ))}
         </Stack>
      </Box>
   )
}

function PaymentSkeletonCard() {
   return (
      <Box p={4} borderRadius=".5rem" bg="white" shadow="lg" cursor="pointer">
         <Flex justify="space-between" mb={2}>
            <Skeleton height="0.875rem" width="40%" />
            <Skeleton height="0.875rem" width="30%" />
         </Flex>
         <Skeleton height="0.875rem" width="30%" />
         <Divider my={4} />
         <Skeleton height="0.875rem" width="20%" />
         <Skeleton height="2rem" width="40%" mt={4} />
      </Box>
   )
}
