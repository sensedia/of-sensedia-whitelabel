import { useTranslation } from 'react-i18next'
import { useRouter } from 'next/router'
import { useQuery } from 'react-query'
import {
   Text,
   Box,
   Flex,
   useToast,
   Spinner,
   Skeleton,
   Divider,
   Button,
} from '@chakra-ui/react'
import { Payments } from '@services'
import { BackButton, PaymentCheckout, LabelValue } from '@components'
import {
   formatCNPJ,
   formatCPF,
   formatCurrency,
   formatDate,
   formatDateWithHour,
} from '@utils'
import { ConsentStatus } from '@models'

export default function DetalhesPagamento() {
   const toast = useToast()
   const { t } = useTranslation()
   const {
      query: { id },
      push,
   } = useRouter()

   const { data, isLoading, isError } = useQuery(
      ['payment', id],
      () => Payments.getByConsentId(String(id)).then((data) => data.data),
      {
         enabled: !!id,
         refetchInterval: 1000 * 60,
         onError: () => {
            toast({
               title: t('confirm.payment.page.request.error'),
               status: 'error',
               isClosable: true,
            })
         },
      },
   )

   const isConsentAwaiting = data?.status === 'AWAITING_AUTHORISATION'

   return (
      <Box bg="gray.100">
         <Box p={6} pt={4} bg="gray.50">
            <Flex
               alignItems="center"
               justifyContent="space-between"
               h={10}
               ml={-1}
            >
               <BackButton path="/pagamentos" />
            </Flex>

            {data ? (
               <Box mt={6}>
                  <Flex justify="space-between" mb={1}>
                     <Text fontSize="sm" fontWeight={700}>
                        {t('payment.details.page.status')}
                     </Text>
                     <Text
                        fontSize="sm"
                        fontWeight={700}
                        color={
                           data.status === ConsentStatus.Authorised
                              ? 'warning'
                              : 'gray.600'
                        }
                     >
                        {formatDateWithHour(data.creationDateTime)}
                     </Text>
                  </Flex>
                  <Text fontSize="md" fontWeight={700} color="warning">
                     {t(`payment.status.${data.status}`)}
                  </Text>
               </Box>
            ) : (
               <Box mt={6}>
                  <Flex justify="space-between" mb={1}>
                     <Skeleton height="0.875rem" width="40%" />
                     <Skeleton height="0.875rem" width="30%" />
                  </Flex>
                  <Skeleton height="1rem" width="30%" />
               </Box>
            )}
         </Box>
         <Box p={6} bg="gray.100">
            {(isLoading || isError) && (
               <Flex
                  width="100%"
                  height="100%"
                  alignItems="center"
                  justify="center"
               >
                  <Spinner />
               </Flex>
            )}
            {data &&
               (isConsentAwaiting ? (
                  <>
                     <Text fontSize="lg" color="gray.800" fontWeight={700}>
                        {t('payment.details.page.subtitle')}
                     </Text>
                     <PaymentCheckout
                        payment={data}
                        onCancelPath="/pagamentos"
                     />
                  </>
               ) : (
                  <>
                     <Text fontSize="lg" color="gray.800" fontWeight={700}>
                        {t('payment.details.page.subtitle')}
                     </Text>
                     <Box
                        bg="white"
                        borderRadius=".5rem"
                        shadow="xl"
                        p={4}
                        mt={6}
                     >
                        <Text color="gray.900" fontWeight={700} fontSize="md">
                           {t('payment.details.amount')}
                        </Text>
                        <Text fontSize="2xl" fontWeight={700} color="gray.900">
                           {formatCurrency(data.payment.amount)}
                        </Text>
                        <Divider my={4} />
                        <LabelValue
                           label={t('payment.details.receiver')}
                           values={[
                              `${
                                 data.creditor.personType === 'PESSOA_NATURAL'
                                    ? `CPF ${formatCPF(data.creditor.cpfCnpj)}`
                                    : `CNPJ ${formatCNPJ(
                                         data.creditor.cpfCnpj,
                                      )}`
                              }`,
                              data.creditor.name,
                           ]}
                        />
                        <LabelValue
                           label={t('payment.details.date')}
                           values={[formatDate(data.creationDateTime)]}
                        />
                        <LabelValue
                           label={t('payment.details.source')}
                           values={[
                              `${
                                 data.debtorAccount.accountType === 'CACC'
                                    ? 'CC'
                                    : 'CI'
                              } ${data.debtorAccount.number}`,
                           ]}
                        />
                        <LabelValue
                           label={t('payment.details.type')}
                           values={['Pix - Open Banking']}
                        />
                     </Box>
                     <Button
                        color="brand.500"
                        variant="ghost"
                        isFullWidth
                        onClick={() => push('/pagamentos')}
                        mt={6}
                     >
                        {t('default.word.cancel')}
                     </Button>
                  </>
               ))}
         </Box>
      </Box>
   )
}
