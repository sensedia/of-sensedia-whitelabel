import * as Yup from 'yup'
import Link from 'next/link'
import { useState } from 'react'
import { useRouter } from 'next/router'
import { AuthModel } from '@models'
import { useFormik } from 'formik'
import { useTranslation } from 'react-i18next'
import { ModalAuthError } from '@pages-components/auth'
import {
   Box,
   Button,
   Input,
   Link as LinkStyle,
   Stack,
   Text,
   useDisclosure,
} from '@chakra-ui/react'
import { AuthService } from '@services'

function AuthPayments() {
   const router = useRouter()
   const { intent_id } = router.query
   const { t } = useTranslation()
   const [isLoading, setLoading] = useState<boolean>(false)
   const { isOpen, onOpen, onClose } = useDisclosure()
   const validationSchema = Yup.object({
      agency: Yup.string().required(),
      account: Yup.string().required(),
      password: Yup.string().required(),
   })
   const formik = useFormik({
      initialValues: {
         agency: '',
         account: '',
         password: '',
      },
      onSubmit: (values) => {
         submitForm(values)
      },
      validationSchema,
      validateOnMount: true,
   })

   function submitForm(values: AuthModel) {
      setLoading(true)
      AuthService.auth(values)
         .then(() => {
            router.push('/pagamentos/confirmar-pagamento?id=' + intent_id)
            setLoading(false)
         })
         .catch(() => {
            onOpen()
            setLoading(false)
         })
   }

   return (
      <Box align="center" bg="gray.100" height="100%" justify="center">
         <Box bg="gray.100" py="1.5rem">
            <ModalAuthError isOpen={isOpen} onClose={onClose} />
            <form onSubmit={formik.handleSubmit}>
               {formik.touched.agency && formik.errors.agency ? (
                  <Text align="center" color="danger" mb="1rem">
                     {t('login.page.input.invalid.agency')}
                  </Text>
               ) : formik.touched.account && formik.errors.account ? (
                  <Text align="center" color="danger" mb="1rem">
                     {t('login.page.input.invalid.account')}
                  </Text>
               ) : formik.touched.password && formik.errors.password ? (
                  <Text align="center" color="danger" mb="1rem">
                     {t('login.page.input.invalid.password')}
                  </Text>
               ) : null}
               <Box px="1.5rem">
                  <Stack spacing="1.2rem">
                     <Input
                        borderColor="gray.300"
                        height="3.25rem"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        name="agency"
                        placeholder={t('login.page.input.placeholder.agency')}
                        type="agency"
                        value={formik.values.agency}
                     />
                     <Input
                        borderColor="gray.300"
                        height="3.25rem"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        name="account"
                        placeholder={t('login.page.input.placeholder.account')}
                        type="account"
                        value={formik.values.account}
                     />
                     <Input
                        borderColor="gray.300"
                        height="3.25rem"
                        onBlur={formik.handleBlur}
                        name="password"
                        placeholder={t('login.page.input.placeholder.password')}
                        type="password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                     />
                     <Button
                        height="3.25rem"
                        isLoading={isLoading}
                        isFullWidth
                        mb="3rem"
                        type="submit"
                     >
                        {t('login.page.button')}
                     </Button>
                  </Stack>
               </Box>
            </form>
            <Box px="1.5rem">
               <Text align="center">
                  {t('login.page.forgotPassword.label')}
               </Text>
               <Link href="" passHref>
                  <LinkStyle>
                     <Text
                        color="brand.600"
                        fontWeight="bold"
                        mt="0.5rem"
                        textDecoration="underline"
                        textAlign="center"
                     >
                        {t('login.page.forgotPassword.textLink')}
                     </Text>
                  </LinkStyle>
               </Link>
            </Box>
         </Box>
      </Box>
   )
}

export default AuthPayments
