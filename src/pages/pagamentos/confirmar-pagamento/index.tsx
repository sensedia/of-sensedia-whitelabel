import { useQuery } from 'react-query'
import { useRouter } from 'next/router'
import { useTranslation } from 'react-i18next'
import { Box, Text, Flex, useToast, Spinner } from '@chakra-ui/react'
import { Payments } from '@services'
import { BackButton, PaymentCheckout } from '@components'

export default function ConfirmarPagamento() {
   const toast = useToast()
   const { t } = useTranslation()
   const {
      query: { id },
   } = useRouter()

   const { data, isLoading, isError } = useQuery(
      ['payment', id],
      () => Payments.getByConsentId(String(id)).then((data) => data.data),
      {
         enabled: !!id,
         refetchInterval: 1000 * 60,
         onError: () => {
            toast({
               title: t('confirm.payment.page.request.error'),
               status: 'error',
               isClosable: true,
            })
         },
      },
   )

   const isConsentRejected = data?.status === 'REJECTED'
   const isConsentAwaiting = data?.status === 'AWAITING_AUTHORISATION'

   if (!id) {
      return (
         <Box p={6} bg="gray.50">
            <Flex
               alignItems="center"
               justifyContent="space-between"
               h={10}
               ml={-1}
            >
               <BackButton path="/" />
            </Flex>
            <Box bg="gray.200" p={4} borderRadius=".5rem" mt={6}>
               <Text fontSize="lg" align="center">
                  {t('confirm.payment.page.id.error')}
               </Text>
            </Box>
         </Box>
      )
   }

   return (
      <Box p={6} pt={4} bg="gray.100">
         <Flex
            alignItems="center"
            justifyContent="space-between"
            h={10}
            ml={-1}
         >
            <BackButton path="/" />
         </Flex>

         <Box mt={2}>
            <Text fontSize="2xl" color="gray.900" fontWeight="bold" mt={6}>
               {t('confirm.payment.page.title')}
            </Text>
         </Box>

         {(isLoading || isError) && (
            <Flex
               width="100%"
               height="100%"
               alignItems="center"
               justify="center"
            >
               <Spinner />
            </Flex>
         )}

         {data &&
            (isConsentRejected ? (
               <Box bg="gray.200" p={4} borderRadius=".5rem" mt={6}>
                  <Text fontSize="lg" align="center">
                     {t('confirm.payment.page.rejected.status')}
                  </Text>
               </Box>
            ) : isConsentAwaiting ? (
               <PaymentCheckout payment={data} onCancelPath="/" />
            ) : (
               <Box bg="gray.200" p={4} borderRadius=".5rem" mt={6}>
                  <Text fontSize="lg" align="center">
                     {t('confirm.payment.page.confirmed.status')}
                  </Text>
               </Box>
            ))}
      </Box>
   )
}
