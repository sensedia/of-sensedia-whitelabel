import React, { useEffect, useState } from 'react'
import {
   Step01,
   Step02,
   Step03,
} from 'pages-components/pagamentos/novo-pagamento'
import {
   NewPaymentContext,
   NewPaymentType,
} from '@pages-components/pagamentos/novo-pagamento/context'
import { Institution, NewPayment, PersonType } from '@models'
import { useRouter } from 'next/router'
import {
   Box,
   Stack,
   Flex,
   TabList,
   Tab,
   Tabs,
   TabPanels,
   TabPanel,
   useToast,
} from '@chakra-ui/react'
import { BackButton, RedirecionamentoPopup } from '@components'
import ModalSelectInstitution from '@components/SelecionarInstituicao'
import { useTranslation } from 'react-i18next'
import { LoggedUser, Payments } from '@services'
import { useQuery } from 'react-query'

enum Steps {
   One = 0,
   Two = 1,
   Three = 2,
}

export default function NovoPagamento() {
   const router = useRouter()
   const toast = useToast()
   const { t } = useTranslation()

   const [stepActive, setStepActive] = useState<Steps>(Steps.One)

   const [openInstitutionModal, setOpenInstitutionModal] = useState(false)
   const [state, setState] = useState<NewPaymentType>({
      payment: {} as NewPayment,
   })
   const [redirect, setRedirect] = useState<boolean>(false)
   const [loading, setLoading] = useState<boolean>(false)
   const [bank, setBank] = useState({
      img: '',
      name: 'Sensedia Bank',
      link: '/',
   })

   const { data: user } = useQuery('loggedUser', LoggedUser.get)
   const { data: dataPayment } = useQuery(
      'dataPayment',
      Payments.getDataPayment,
   )

   useEffect(() => {
      user &&
         setState({
            payment: {
               ...state.payment,
               loggedUser: user.data,
               localInstrument: 'DICT',
            },
         })
   }, [user])

   useEffect(() => {
      dataPayment &&
         setState({
            payment: {
               ...state.payment,
               payment: {
                  ...state.payment.payment,
                  currency: dataPayment.data.payment.currency,
                  amount: dataPayment.data.payment.amount,
               },
               debtorAccount: dataPayment.data.debtorAccount,
               creditor: {
                  personType: PersonType.PessoaNatural,
                  cpfCnpj: dataPayment.data.identification.cpfCnpj,
                  name: dataPayment.data.identification.name,
               },
            },
         })
   }, [dataPayment])

   const selectInstitution = (institution: Institution) => {
      setState({
         payment: {
            ...state.payment,
            authorisationServer: {
               ...state.payment.authorisationServer,
               organisationId: institution.organisationId,
               organisationName: institution.customerFriendlyName,
               authorisationServerId: institution.authorisationServerId,
            },
         },
      })
      setStepActive(Steps.Two)
      setOpenInstitutionModal(false)
   }

   const cancel = () => {
      router.push('/pagamentos')
   }

   const handleSetState = (data: NewPayment) => {
      setState({
         payment: data,
      })
   }

   const patchPayment = () => {
      setLoading(true)
      Payments.post(state.payment)
         .then((data) => {
            setBank({
               ...bank,
               name: 'Sensedia Bank',
               link: data.data.redirectUri || '',
            })
            setRedirect(true)
            setLoading(false)
         })
         .catch(() => {
            setLoading(false)
            toast({
               title: t('default.request.error'),
               status: 'error',
               isClosable: true,
            })
         })
   }

   return (
      <>
         <NewPaymentContext.Provider
            value={{ state, setState: handleSetState }}
         >
            <Box height="100%" background="gray.100">
               <Stack p="1.25rem 1.25rem 0.313rem 1.25rem">
                  <Flex justifyContent="space-between" alignContent="center">
                     <BackButton path="/pagamentos" />
                     <Tabs
                        display="flex"
                        alignItems="center"
                        index={stepActive}
                        onChange={(index) => setStepActive(index)}
                     >
                        <TabList border="0" justifyContent="space-around">
                           <Tab
                              padding="0"
                              width="0.5rem"
                              height="0.5rem"
                              borderRadius="0.5rem"
                              marginRight="0.5rem"
                              bg="brand.300"
                              isDisabled={stepActive !== Steps.One}
                              _selected={{ bg: 'brand.600', width: '2.125rem' }}
                           ></Tab>
                           <Tab
                              padding="0"
                              marginRight="0.625rem"
                              width="0.5rem"
                              height="0.5rem"
                              borderRadius="0.5rem"
                              bg="brand.300"
                              isDisabled={stepActive !== Steps.Two}
                              _selected={{ bg: 'brand.600', width: '2.125rem' }}
                           ></Tab>
                           <Tab
                              padding="0"
                              width="0.5rem"
                              height="0.5rem"
                              borderRadius="0.5rem"
                              bg="brand.300"
                              isDisabled={stepActive !== Steps.Three}
                              _selected={{ bg: 'brand.600', width: '2.125rem' }}
                           ></Tab>
                        </TabList>
                     </Tabs>
                  </Flex>
               </Stack>
               <Stack backgroundColor="white">
                  <Tabs index={stepActive}>
                     <TabPanels>
                        <TabPanel padding={0}>
                           <Step01
                              openModalSelectInstitution={() =>
                                 setOpenInstitutionModal(true)
                              }
                           />
                        </TabPanel>
                        <TabPanel padding={0}>
                           <Step02
                              onClickContinue={() => setStepActive(Steps.Three)}
                              onClickCancel={() => cancel()}
                           />
                        </TabPanel>
                        <TabPanel padding={0}>
                           <Step03
                              loading={loading}
                              dataPayment={dataPayment?.data}
                              onClickContinue={() => patchPayment()}
                              onClickCancel={() => cancel()}
                           />
                        </TabPanel>
                     </TabPanels>
                  </Tabs>
               </Stack>
            </Box>
            <ModalSelectInstitution
               title={t('new.payment.checkout.modal.institution.title')}
               description={t(
                  'new.payment.checkout.modal.institution.description',
               )}
               isOpen={openInstitutionModal}
               closeModal={() => setOpenInstitutionModal(false)}
               changeInstitution={(data) => selectInstitution(data)}
            />
            <RedirecionamentoPopup
               isOpen={redirect}
               onClose={() => setRedirect(false)}
               bank={bank}
               message={t('new.payment.checkout.warning.redirect.description')}
               redirectMessage={t(
                  'new.payment.checkout.warning.redirect.message',
               )}
            />
         </NewPaymentContext.Provider>
      </>
   )
}
