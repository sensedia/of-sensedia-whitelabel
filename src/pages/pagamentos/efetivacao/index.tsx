import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { Box, Stack, Flex, Text, Divider, useToast } from '@chakra-ui/react'
import { Button, Card, CardShareSkeleton, LabelValue } from '@components'
import { Trans, useTranslation } from 'react-i18next'
import { CheckIcon } from '@chakra-ui/icons'
import { LoggedUser, Payments } from '@services'
import { PaymentEffectuation } from '@models'
import { formatDate } from '@utils'
import { useQuery } from 'react-query'

export default function EfetivacaoPagamento() {
   const router = useRouter()
   const toast = useToast()
   const { t } = useTranslation()
   const [loading, setLoading] = useState<boolean>(true)
   const [data, setData] = useState<PaymentEffectuation>()
   const { data: user } = useQuery('loggedUser', LoggedUser.get)

   const goBack = () => {
      router.push('/')
   }

   const { query } = useRouter()

   useEffect(() => {
      if (router.asPath.indexOf('#') > -1) {
         router.push(router.asPath.replace('#', '?'))
      } else {
         query.state &&
            query.code &&
            Payments.effectuationPost({
               authorizationCode: query.code as string,
               requestId: query.state as string,
            })
               .then((data) => {
                  setData(data.data)
                  setLoading(false)
               })
               .catch(() => {
                  launchToastError()
               })
      }
   }, [router.asPath, query])

   function launchToastError() {
      return toast({
         title: t('default.request.error'),
         status: 'error',
         isClosable: true,
      })
   }

   return (
      <Box p="1.25rem" background="white">
         {loading && (
            <>
               <CardShareSkeleton />
               <CardShareSkeleton />
               <CardShareSkeleton />
            </>
         )}
         {!loading && data && (
            <>
               <Text
                  py="0.625rem"
                  fontSize="2xl"
                  color="gray.900"
                  fontWeight="bold"
               >
                  {t('new.payment.effectuation.title')}
               </Text>
               <Flex
                  backgroundColor="successLighter"
                  direction="column"
                  justify="center"
                  alignItems="center"
                  borderRadius="0.5rem"
                  p="1rem"
               >
                  <Flex
                     backgroundColor="successLight"
                     justify="center"
                     alignItems="center"
                     width="4.5rem"
                     height="4.5rem"
                     borderRadius="50%"
                     mb="0.5rem"
                  >
                     <CheckIcon color="success" height="2rem" width="2rem" />
                  </Flex>
                  <Text align="center" color="success" fontSize="xl">
                     <Trans
                        defaults="new.payment.effectuation.success.header.message"
                        components={[<strong key={0} />, <br key={1} />]}
                     />
                  </Text>
               </Flex>
               <Stack data-testid="wrapper-main">
                  <Card my="1rem">
                     <Text color="gray.900" fontWeight={700} fontSize="md">
                        {t('new.payment.checkout.total.label')}
                     </Text>
                     <Text fontSize="2xl" fontWeight="bold" color="gray.900">
                        {[
                           new Intl.NumberFormat('pt-br', {
                              style: 'currency',
                              currency: 'BRL',
                           }).format(
                              parseFloat(
                                 data?.payment?.amount as string,
                              ) as number,
                           ),
                        ]}
                     </Text>
                     <Text fontSize="md" color="gray.900">
                        {data?.remittanceInformation}
                     </Text>
                     <Divider background="gray.300" my="1rem" />
                     {data?.creditor && (
                        <LabelValue
                           label={t(
                              'new.payment.effectuation.recipient.data.label',
                           )}
                           isLoading={false}
                           values={[
                              `CPF ${data?.creditor?.cpfCnpj}`,
                              `${data?.creditor?.name}`,
                              `${data?.authorisationServer?.organisationName}`,
                              `Ag ${data?.creditorAccount.issuer} | ${data?.creditorAccount.accountType} ${data?.creditorAccount.number}`,
                           ]}
                        />
                     )}
                     <LabelValue
                        label={t('new.payment.effectuation.payer.data.label')}
                        isLoading={false}
                        values={[
                           `${user?.data.document.rel} ${user?.data.document.identification}`,
                           `${user?.data.name}`,
                        ]}
                     />
                     <LabelValue
                        label={t('new.payment.effectuation.payment.date.label')}
                        isLoading={false}
                        values={[formatDate(data?.creationDateTime)]}
                     />
                  </Card>
                  <Button isFullWidth mb="0.75rem" onClick={() => goBack()}>
                     {t('new.payment.effectuation.payment.button.label')}
                  </Button>
               </Stack>
            </>
         )}
      </Box>
   )
}
