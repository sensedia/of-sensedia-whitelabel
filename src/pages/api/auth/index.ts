import { NextApiRequest, NextApiResponse } from 'next'

export default function Auth(req: NextApiRequest, res: NextApiResponse) {
   if (req.body.agency === '123123') {
      res.status(200).send({ message: 'Ok' })
   } else {
      res.status(400).send({ message: 'Bad Request!' })
   }
}
