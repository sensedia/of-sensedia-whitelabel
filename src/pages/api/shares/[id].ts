import { ShareDetails } from '@models'
import { NextApiRequest, NextApiResponse } from 'next'

const mock: ShareDetails = require('@tests/mock-shares-details')

export default function getShareByID(
   req: NextApiRequest,
   res: NextApiResponse,
) {
   if (req.method === 'GET') {
      res.status(200).send(mock)
   } else {
      res.status(405).send({ message: 'Method not allowed' })
   }
}
