import { NextApiRequest, NextApiResponse } from 'next'

export default function shares(req: NextApiRequest, res: NextApiResponse) {
   if (req.method === 'GET') {
      res.status(200).send([
         {
            businessEntity: {
               document: {
                  identification: '11111111111111',
                  rel: 'CNPJ',
               },
            },
            consentId: '1',
            createDateTime: '2021-05-31T18:23:03.112Z',
            resourceGroups: [
               {
                  resourceGroupId: '1',
                  resources: [
                     {
                        additionalInfo: [
                           {
                              key: 'string',
                              value: 'string',
                           },
                        ],
                        displayName: 'Nubank',
                        resourceId: 'string',
                        status: 'AVAILABLE',
                        type: 'ACCOUNT',
                     },
                  ],
                  dataPermissions: [
                     {
                        detail: 'Valor Utilizado',
                        displayName: 'Limites',
                        items: ['string'],
                        permissionCode: 'ACCOUNTS_OVERDFRAFT_LIMITS_READ',
                     },
                  ],
                  displayName: 'Sensedia Bank',
               },
            ],
            expirationDateTime: '2021-05-31T18:23:03.112Z',
            loggedUser: {
               document: {
                  identification: '11111111111',
                  rel: 'CPF',
               },
            },
            organisationId: 'string',
            shareId: '1',
            shareType: 'TRANSMITTING',
            status: 'ACTIVE',
         },
      ])
   } else {
      res.status(405).send({ message: 'Method not allowed' })
   }
}
