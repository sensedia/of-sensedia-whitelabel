import { EffectiveModel } from '@models'
import { NextApiRequest, NextApiResponse } from 'next'

const mock0: EffectiveModel = require('@tests/mock-effective-scenario-1')
const mock1: EffectiveModel = require('@tests/mock-effective-scenario-2')
const mock2: EffectiveModel = require('@tests/mock-effective-scenario-3')

export default function getEffectiveByID(
   req: NextApiRequest,
   res: NextApiResponse,
) {
   const id = req.query.id
   if (id === '1') {
      res.status(200).send(mock0)
   } else if (id === '2') {
      res.status(200).send(mock1)
   } else {
      res.status(200).send(mock2)
   }
}
