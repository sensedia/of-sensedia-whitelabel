import { NextApiRequest, NextApiResponse } from 'next'

export default function transmitteds(
   req: NextApiRequest,
   res: NextApiResponse,
) {
   if (req.method === 'GET') {
      res.status(200).send([
         {
            businessEntity: {
               document: {
                  identification: '11111111111111',
                  rel: 'CNPJ',
               },
            },
            consentId: 'string',
            createDateTime: '2021-05-31T18:23:03.112Z',
            resourceGroups: [
               {
                  resourceGroupId: '1',
                  resources: [
                     {
                        additionalInfo: [
                           {
                              key: 'string',
                              value: 'string',
                           },
                        ],
                        displayName: 'Dados de Conta',
                        resourceId: 'string',
                        status: 'AVAILABLE',
                        type: 'ACCOUNT',
                     },
                  ],
                  dataPermissions: [
                     {
                        detail: 'Valor Utilizado',
                        displayName: 'Limites',
                        items: ['string'],
                        permissionCode: 'ACCOUNTS_OVERDFRAFT_LIMITS_READ',
                     },
                  ],
                  displayName: 'Dados da Conta',
               },
            ],
            expirationDateTime: '2021-05-31T18:23:03.112Z',
            loggedUser: {
               document: {
                  identification: '11111111111',
                  rel: 'CPF',
               },
            },
            organisationId: 'string',
            shareId: 'string',
            shareType: 'TRANSMITTING',
            status: 'ACTIVE',
         },
         {
            businessEntity: {
               document: {
                  identification: '11111111111111',
                  rel: 'CNPJ',
               },
            },
            consentId: 'string',
            createDateTime: '2021-05-31T18:23:03.112Z',
            resourceGroups: [
               {
                  resourceGroupId: '1',
                  resources: [
                     {
                        additionalInfo: [
                           {
                              key: 'string',
                              value: 'string',
                           },
                        ],
                        displayName: 'Dados de Conta',
                        resourceId: 'string',
                        status: 'AVAILABLE',
                        type: 'ACCOUNT',
                     },
                  ],
                  dataPermissions: [
                     {
                        detail: 'Valor Utilizado',
                        displayName: 'Limites',
                        items: ['string'],
                        permissionCode: 'ACCOUNTS_OVERDFRAFT_LIMITS_READ',
                     },
                  ],
                  displayName: 'Dados da Conta',
               },
            ],
            expirationDateTime: '2021-05-31T18:23:03.112Z',
            loggedUser: {
               document: {
                  identification: '11111111111',
                  rel: 'CPF',
               },
            },
            organisationId: 'string',
            shareId: 'string',
            shareType: 'TRANSMITTING',
            status: 'PENDING',
         },
      ])
   } else {
      res.status(405).send({ message: 'Method not allowed' })
   }
}
