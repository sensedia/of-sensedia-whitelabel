import { NextApiRequest, NextApiResponse } from 'next'

export default function Institutions(
   req: NextApiRequest,
   res: NextApiResponse,
) {
   if (req.method === 'GET') {
      res.status(200).send([
         {
            organisationId: '74e929d9-33b6-4d85-8ba7-c146c867a817',
            authorisationServerId: '9d2b7710-b412-4a12-a385-02df9d56d6c6',
            customerFriendlyName: 'OPENDATA_INSTITUICAO',
            customerFriendlyDescription: 'Breve descrição do servidor',
            customerFriendlyLogoUri: 'https://www.instituicao.com.br/logo.png',
            registrationNumber: '1335323600189',
            requiresParticipantTermsAndConditionsSigning: true,
            brands: [],
         },
         {
            organisationId: '74e929d9-33b6-4d85-8ba7-c146c867a817',
            authorisationServerId: '03aec37e-2a7a-4a39-8cd7-4620f2cafd33',
            customerFriendlyName: 'Sandbox Directory',
            customerFriendlyLogoUri: 'https://notused.com',
            customerFriendlyDescription: 'Breve descrição do servidor',
            registrationNumber: '1335323600189',
            requiresParticipantTermsAndConditionsSigning: true,
            brands: [],
         },
         {
            organisationId: '74e929d9-33b6-4d85-8ba7-c146c867a817',
            authorisationServerId: '9e9b3556-9b2c-49a0-825d-c9b28b31d5c9',
            customerFriendlyName: 'Raidiam Mock Bank - Dev',
            customerFriendlyLogoUri: 'https://tpp.localhost/logo.png',
            customerFriendlyDescription: 'Breve descrição do servidor',
            registrationNumber: '1335323600189',
            requiresParticipantTermsAndConditionsSigning: true,
            brands: [],
         },
         {
            organisationId: '2a01e202-e8f0-5f5a-9651-ebc257371e6e',
            authorisationServerId: '016d83ad-7fcf-495c-9d30-cb55bc04b5e5',
            customerFriendlyName: 'BANRISUL',
            customerFriendlyDescription: 'Servidor de autenticacao',
            customerFriendlyLogoUri:
               'https://www.banrisul.com.br/naoseaplica.png',
            registrationNumber: '92702067000196',
            requiresParticipantTermsAndConditionsSigning: true,
            brands: [
               {
                  organisationId: '74e929d9-33b6-4d85-8ba7-c146c867a817',
                  authorisationServerId: 'c8f0bf49-4744-4933-8960-7add6e590841',
                  customerFriendlyName: 'Mock Bank',
                  customerFriendlyDescription: 'Breve descrição do servidor',
                  customerFriendlyLogoUri: 'https://mockbank.com/logo.png',
                  registrationNumber: '1335323600189',
                  requiresParticipantTermsAndConditionsSigning: true,
                  brands: [],
               },
               {
                  organisationId: '74e929d9-33b6-4d85-8ba7-c146c867a817',
                  authorisationServerId: '3d7d6dba-d1a1-492f-8a2d-ee5c84e9f88f',
                  customerFriendlyName: 'WebhookDevServer',
                  customerFriendlyLogoUri: 'https://www.invalid.com',
                  customerFriendlyDescription: 'Breve descrição do servidor',
                  registrationNumber: '1335323600189',
                  requiresParticipantTermsAndConditionsSigning: true,
                  brands: [],
               },
               {
                  organisationId: '74e929d9-33b6-4d85-8ba7-c146c867a817',
                  authorisationServerId: '3d7d6dba-d1a1-492f-8a2d-ee5c84e9f88f',
                  customerFriendlyName: 'TESTE 1',
                  customerFriendlyLogoUri: 'https://www.invalid.com',
                  customerFriendlyDescription: 'Breve descrição do servidor',
                  registrationNumber: '1335323600189',
                  requiresParticipantTermsAndConditionsSigning: true,
                  brands: [],
               },
            ],
         },
         {
            organisationId: '2a01e202-e8f0-5f5a-9651-ebc257371e6e',
            authorisationServerId: '016d83ad-7fcf-495c-9d30-cb55bc04b5e5',
            customerFriendlyName: 'Bradesco',
            customerFriendlyDescription: 'Servidor de autenticacao',
            customerFriendlyLogoUri:
               'https://www.banrisul.com.br/naoseaplica.png',
            registrationNumber: '92702067000196',
            requiresParticipantTermsAndConditionsSigning: true,
            brands: [
               {
                  organisationId: '74e929d9-33b6-4d85-8ba7-c146c867a817',
                  authorisationServerId: 'c8f0bf49-4744-4933-8960-7add6e590841',
                  customerFriendlyName: 'Next',
                  customerFriendlyDescription: 'Breve descrição do servidor',
                  customerFriendlyLogoUri: 'https://mockbank.com/logo.png',
                  registrationNumber: '1335323600189',
                  requiresParticipantTermsAndConditionsSigning: true,
                  brands: [],
               },
            ],
         },
         {
            organisationId: '2a01e202-e8f0-5f5a-9651-ebc257371e6e',
            authorisationServerId: '016d83ad-7fcf-495c-9d30-cb55bc04b5e5',
            customerFriendlyName: 'Santander',
            customerFriendlyDescription: 'Servidor de autenticacao',
            customerFriendlyLogoUri:
               'https://www.banrisul.com.br/naoseaplica.png',
            registrationNumber: '92702067000196',
            requiresParticipantTermsAndConditionsSigning: true,
            brands: [
               {
                  organisationId: '74e929d9-33b6-4d85-8ba7-c146c867a817',
                  authorisationServerId: 'c8f0bf49-4744-4933-8960-7add6e590841',
                  customerFriendlyName: 'Way',
                  customerFriendlyDescription: 'Breve descrição do servidor',
                  customerFriendlyLogoUri: 'https://mockbank.com/logo.png',
                  registrationNumber: '1335323600189',
                  requiresParticipantTermsAndConditionsSigning: true,
                  brands: [],
               },
            ],
         },
      ])
   } else {
      res.status(400).send({ message: 'Bad Request!' })
   }
}
