import { NextApiRequest, NextApiResponse } from 'next'

export default function getInstitutionByID(
   req: NextApiRequest,
   res: NextApiResponse,
) {
   if (req.method === 'GET') {
      const id = req.query.id
      res.status(200).send({
         id: id,
         name: 'Wiscredi',
         description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in tincidunt sapien. Sed ac velit in nunc aliquam condimentum.',
         participants: [
            'Instituição 1',
            'Instituição 2',
            'Instituição 3',
            'Instituição 4',
            'Instituição 5',
            'Instituição 6',
            'Instituição 7',
            'Instituição 8',
            'Instituição 9',
            'Instituição 10',
            'Instituição 11',
         ],
      })
   } else {
      res.status(405).send({ message: 'Method not allowed' })
   }
}
