import { NextApiRequest, NextApiResponse } from 'next'

export default function shareddata(req: NextApiRequest, res: NextApiResponse) {
   if (req.method === 'GET') {
      res.status(200).send({
         dataSumUp: [
            { label: 'CPF' },
            { label: 'Telefone' },
            { label: 'E-mail' },
            { label: 'Data de nascimento' },
            { label: 'Estado civil' },
            { label: 'Filiação' },
            { label: 'Nacionalidade' },
            {
               label: 'Endereço residencial',
               obs: '(Inclui nome da rua, número, complemento, cidade, UF, país e CEP)',
            },
         ],
         mandatoryData: [
            {
               title: 'Dados Cadastrais',
               dataEntries: [
                  {
                     title: 'Dados Cadastrais',
                     description: 'Nome, RG, Endereço Residencial, etc.',
                  },
                  {
                     title: 'Informações Complementares',
                     description:
                        'Qualificação Financeira, Dados Bancários, etc.',
                  },
               ],
            },
            {
               title: 'Dados da Conta',
               dataEntries: [
                  {
                     title: 'Saldos',
                     description: 'Saldo disponível em conta corrente.',
                  },
                  {
                     title: 'Extratos',
                     description: 'Movimentações da sua conta corrente.',
                  },
               ],
            },
         ],
         optionalData: [
            {
               title: 'Dados da Conta',
               dataEntries: [
                  {
                     title: 'Limites | Cheque Especial',
                     description:
                        'Tipo de conta, Valor utilizado e valor disponível.',
                     value: 'cheque',
                     selected: true,
                  },
               ],
            },
         ],
      })
   } else {
      res.status(405).send({ message: 'Method not allowed' })
   }
}
