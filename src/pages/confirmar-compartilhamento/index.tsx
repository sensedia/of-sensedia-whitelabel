import {
   Box,
   Stack,
   Tab,
   TabList,
   TabPanel,
   TabPanels,
   Tabs,
   Text,
   useToast,
   Spinner,
   Flex,
} from '@chakra-ui/react'
import { BackButton, RedirecionamentoPopup } from '@components'
import FooterButtons from '@pages-components/confirmar-compartilhamento/FooterButtons'
import { useRouter } from 'next/router'
import { Step01, Step02 } from 'pages-components/confirmar-compartilhamento'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { SwipeEventData, useSwipeable } from 'react-swipeable'
import { useQuery, useMutation } from 'react-query'
import { Shares } from '@services'
import { JANS_AUTH } from '@constants'

function ConfirmarCompartilhamentoPage() {
   const toast = useToast()
   const { t } = useTranslation()
   const handlers = useSwipeable({
      onSwipedLeft: (eventData) => handleSliderChange(eventData),
      onSwipedRight: (eventData) => handleSliderChange(eventData),
   })

   const {
      query: { intent_id },
   } = useRouter()

   const { isLoading, data } = useQuery(
      'intent_id',
      () => Shares.getByConsentId(intent_id as string),
      {
         enabled: !!intent_id,
         onSuccess: (data) => {
            setBank({
               ...bank,
               name: data.data.organisationName,
            })
         },
         onError: () =>
            toast({
               title: t('shared.page.share.request.error'),
               status: 'error',
               isClosable: true,
            }),
         refetchOnWindowFocus: false,
      },
   )

   const confirmationMutation = useMutation(
      (shareId: string) => Shares.confirmationShareTransmitter(shareId),
      {
         onSuccess: () => setRedirect(true),
         onError: () => {
            toast({
               title: t('default.request.error'),
               status: 'error',
               isClosable: true,
            })
         },
      },
   )

   const [tabIndex, setTabIndex] = useState(0)
   const [redirect, setRedirect] = useState<boolean>(false)
   const [bank, setBank] = useState({
      img: '',
      name: '',
      link: JANS_AUTH,
   })
   const { push } = useRouter()

   function handleSliderChange(event: SwipeEventData) {
      if (event.dir === 'Left' && tabIndex === 0) {
         return setTabIndex((state) => state + 1)
      } else if (event.dir === 'Right' && tabIndex === 1) {
         return setTabIndex((state) => state - 1)
      }
   }

   function handleContinueButton() {
      if (tabIndex === 0) {
         return setTabIndex(1)
      } else {
         const shareId = data?.data.shareId
         if (!!shareId) {
            const body = {
               resources: data?.data.resourceGroups.map((x) => ({
                  status: 'AVAILABLE',
                  type: x.type,
               })),
            }

            Shares.confirmationResource(shareId, body)
               .then(() => {
                  confirmationMutation.mutate(shareId)
               })
               .catch(() => {
                  toast({
                     title: t('default.request.error'),
                     status: 'error',
                     isClosable: true,
                  })
               })
         }
      }
   }

   function handleCancelButton() {
      if (tabIndex === 1) {
         return setTabIndex(0)
      }
      return push('/')
   }

   return (
      <Box height="100%">
         <Tabs
            {...handlers}
            index={tabIndex}
            onChange={setTabIndex}
            variant="unstyled"
            align="start"
         >
            <TabList
               border="0"
               justifyContent="center"
               alignItems="space-between"
               flexDirection="column"
               bg="white"
            >
               <Stack
                  flexDirection="row"
                  justifyContent="space-between"
                  alignItems="center"
                  padding="1.2rem 1.5rem 1.25rem 1.25rem"
               >
                  <Stack onClick={handleCancelButton}>
                     {tabIndex === 1 && (
                        <BackButton path={'?id=' + intent_id} />
                     )}
                  </Stack>

                  <Box
                     marginTop="0!important"
                     display="flex"
                     justifyContent="space-between"
                     height="2rem"
                     alignItems="center"
                  >
                     <Tab
                        padding="0"
                        width="0.5rem"
                        height="0.5rem"
                        borderRadius="0.5rem"
                        marginRight="0.5rem"
                        bg="brand.300"
                        _selected={{ bg: 'brand.600', width: '2.125rem' }}
                     ></Tab>
                     <Tab
                        padding="0"
                        width="0.5rem"
                        height="0.5rem"
                        borderRadius="0.5rem"
                        bg="brand.300"
                        _selected={{ bg: 'brand.600', width: '2.125rem' }}
                     ></Tab>
                  </Box>
               </Stack>

               <Box
                  display="flex"
                  flexDirection="column"
                  alignItems="start"
                  backgroundColor="white"
                  paddingX="1.25rem"
               >
                  <Text fontSize="2xl" color="gray.900" fontWeight="bold">
                     {t('confirm.sharing.resume.page.title')}
                  </Text>
               </Box>
            </TabList>
            {isLoading && (
               <Flex w="100%" py={10} alignItems="center" justify="center">
                  <Spinner />
               </Flex>
            )}
            {!isLoading && data && (
               <TabPanels>
                  <TabPanel padding="0">
                     <Step01 data={data.data} />
                  </TabPanel>
                  <TabPanel padding="0">
                     <Step02 data={data.data} />
                  </TabPanel>
               </TabPanels>
            )}
         </Tabs>
         <Stack direction="column" width="100%" p="1.25rem" bg="white">
            <FooterButtons
               isLoading={confirmationMutation.isLoading}
               onClickContinue={handleContinueButton}
               onClickCancel={handleCancelButton}
            />
         </Stack>

         <RedirecionamentoPopup
            isOpen={redirect}
            onClose={() => setRedirect(false)}
            bank={bank}
            message={t('redirect.popup.message')}
            redirectMessage={t('redirect.popup.redirect.message.to.receiver')}
         />
      </Box>
   )
}

export default ConfirmarCompartilhamentoPage
