/* eslint-disable no-unused-vars */
import { useState } from 'react'

import { Step01, Step02, Step03 } from 'pages-components/novo-compartilhamento/'

import { Finality, Institution, NewShare, User } from '@models'

import {
   Box,
   Stack,
   Text,
   Flex,
   TabList,
   Tab,
   Tabs,
   TabPanels,
   TabPanel,
   useToast,
} from '@chakra-ui/react'
import { BackButton } from '@components'
import ModalSelectInstitution from '@components/SelecionarInstituicao'

import { useTranslation } from 'react-i18next'
import {
   NewShareContextState,
   StepsNewShareProvider,
} from '@pages-components/novo-compartilhamento/context'
import { useQuery } from 'react-query'
import { LoggedUser } from 'services/LoggedUser'
import { useEffect } from 'react'
import { Shares } from '@services'

enum Steps {
   One = 0,
   Two = 1,
   Three = 2,
}

export default function NovoCompartilhamento() {
   const { t } = useTranslation()
   const toast = useToast()
   const { data: userData } = useQuery('loggedUser', LoggedUser.get)
   const { data: finalityData } = useQuery('finality', LoggedUser.getFinality)

   const [stepActive, setStepActive] = useState<Steps>(Steps.One)
   const [urlRedirect, setUrlRedirect] = useState<string>('')
   const [isLoading, setLoading] = useState<boolean>(false)

   const [modalSelectInstitutionIsOpen, setModalSelectInstitutionIsOpen] =
      useState(false)

   const [state, setState] = useState<NewShareContextState>({
      loggedUser: {} as User,
      finality: {} as Finality,
      deadline: 6,
      institution: {} as Institution,
   })

   useEffect(() => {
      if (userData?.data) {
         setState((state) => ({ ...state, loggedUser: userData.data }))
      }
   }, [userData])

   useEffect(() => {
      if (finalityData?.data) {
         setState((state) => ({ ...state, finality: finalityData.data }))
      }
   }, [finalityData])

   function openModalSelectInstitution() {
      setModalSelectInstitutionIsOpen(!modalSelectInstitutionIsOpen)
   }

   function closeModalSelectInstitution() {
      setModalSelectInstitutionIsOpen(!modalSelectInstitutionIsOpen)
   }

   function selectInstitution(institution: Institution) {
      if (!isLoading) {
         setLoading(true)
         const data: NewShare = {
            authorisationServer: {
               organisationId: institution.organisationId,
               authorisationServerId: institution.authorisationServerId,
            },
            loggedUser: {
               document: {
                  identification:
                     userData?.data.document.identification.replace(
                        /[^Z0-9 ]/g,
                        '',
                     ),
                  rel: userData?.data.document.rel,
               },
            },
            finality: finalityData?.data as Finality,
         }

         Shares.create(data)
            .then((response) => {
               setState((state) => ({
                  ...state,
                  institution,
                  NewShareResponse: response.data,
               }))
               closeModalSelectInstitution()
               setStepActive(Steps.Two)
               setLoading(false)
            })
            .catch(() => {
               setLoading(false)
               setStepActive(Steps.One)
               launchToastError()
            })
      }
   }

   function launchToastError() {
      return toast({
         title: t('new.share.select.institution.error'),
         status: 'error',
         isClosable: true,
      })
   }

   function confirmShare(data: any) {
      setLoading(true)
      state.NewShareResponse?.shareId &&
         Shares.update(state.NewShareResponse?.shareId, {
            ...data,
            redirectUri: `${window.location.origin}/efetivacao-compartilhamento/${state.NewShareResponse?.shareId}`,
         })
            .then((response) => {
               setLoading(false)
               setUrlRedirect(response.data.redirect_uri)
               setStepActive(Steps.Three)
            })
            .catch(() => {
               setLoading(false)
               toast({
                  title: t('default.request.error'),
                  status: 'error',
                  isClosable: true,
               })
            })
   }

   return (
      <>
         <StepsNewShareProvider value={[state, setState]}>
            <Box height="100%" backgroundColor="white">
               <Stack p="1.25rem 1.25rem 0.313rem 1.25rem">
                  <Flex justifyContent="space-between" alignContent="center">
                     <BackButton path="/" />
                     <Tabs
                        display="flex"
                        alignItems="center"
                        index={stepActive}
                        onChange={(index) => setStepActive(index)}
                     >
                        <TabList border="0" justifyContent="space-around">
                           <Tab
                              padding="0"
                              width="0.5rem"
                              height="0.5rem"
                              borderRadius="0.5rem"
                              marginRight="0.5rem"
                              bg="brand.300"
                              isDisabled={stepActive !== Steps.One}
                              _selected={{ bg: 'brand.600', width: '2.125rem' }}
                           ></Tab>
                           <Tab
                              padding="0"
                              marginRight="0.625rem"
                              width="0.5rem"
                              height="0.5rem"
                              borderRadius="0.5rem"
                              bg="brand.300"
                              isDisabled={stepActive !== Steps.Two}
                              _selected={{ bg: 'brand.600', width: '2.125rem' }}
                           ></Tab>
                           <Tab
                              padding="0"
                              width="0.5rem"
                              height="0.5rem"
                              borderRadius="0.5rem"
                              bg="brand.300"
                              isDisabled={stepActive !== Steps.Three}
                              _selected={{ bg: 'brand.600', width: '2.125rem' }}
                           ></Tab>
                        </TabList>
                     </Tabs>
                  </Flex>
                  <Text
                     p="0.625rem"
                     fontSize="2xl"
                     color="gray.900"
                     fontWeight="bold"
                  >
                     {t('new.share.page.title')}
                  </Text>
               </Stack>
               <Stack backgroundColor="white">
                  <Tabs index={stepActive}>
                     <TabPanels>
                        <TabPanel padding={0}>
                           <Step01
                              openModalSelectInstitution={
                                 openModalSelectInstitution
                              }
                           ></Step01>
                        </TabPanel>
                        <TabPanel padding={0}>
                           <Step02
                              isLoading={isLoading}
                              confirmShare={(data) => confirmShare(data)}
                              openModalSelectInstitution={
                                 openModalSelectInstitution
                              }
                           ></Step02>
                        </TabPanel>
                        <TabPanel>
                           <Step03 urlRedirect={urlRedirect}></Step03>
                        </TabPanel>
                     </TabPanels>
                  </Tabs>
               </Stack>
            </Box>
            <ModalSelectInstitution
               title={t('institution.page.abstract.title')}
               isOpen={modalSelectInstitutionIsOpen}
               closeModal={() => closeModalSelectInstitution()}
               changeInstitution={(index) => selectInstitution(index)}
            />
         </StepsNewShareProvider>
      </>
   )
}
