import { useEffect, useState } from 'react'
import { useQuery } from 'react-query'
import { useRouter } from 'next/router'
import { EffectiveModel } from '@models'
import { EffectiveService, LoggedUser } from '@services'
import { useTranslation, Trans } from 'react-i18next'
import { Header } from '@pages-components/efetivacao-compartilhamento'
import { TransmittedShare, SharedDataCollapse } from '@components'
import { Button, Stack, Text, useToast } from '@chakra-ui/react'

export default function EfetivacaoCompartilhamento() {
   const router = useRouter()
   const toast = useToast()
   const { t } = useTranslation()

   const [isLoading, setLoading] = useState<boolean>(true)
   const [data, setData] = useState<EffectiveModel>()
   const { data: userData } = useQuery('loggedUser', LoggedUser.get)

   const { query } = useRouter()

   useEffect(() => {
      setLoading(true)
      if (router.asPath.indexOf('#') > -1) {
         router.push(router.asPath.replace('#', '?'))
      } else {
         query.id_token &&
            query.code &&
            EffectiveService.getShared({
               authorizationCode: query.code as string,
               requestId: query.state as string,
            })
               .then((data) => {
                  setData(data.data)
                  setLoading(false)
               })
               .catch(() => {
                  toast({
                     title: t('default.request.error'),
                     status: 'error',
                     isClosable: true,
                  })
               })
      }
   }, [router.asPath, query])

   function newRequest() {
      return router.push('/novo-compartilhamento')
   }
   function continueProcess() {
      return router.push('/')
   }

   return (
      <Stack backgroundColor="gray.100">
         <Stack p="1rem 1.1rem" spacing="2rem">
            <Text color="gray.900" fontSize="2xl" fontWeight={700}>
               {t('share.page.request.success.title')}
            </Text>
            <Header
               institution={data?.organisation?.organisationName as ''}
               isLoading={isLoading}
               status={data?.status as ''}
            />
            {userData && (
               <TransmittedShare
                  identification={{
                     cpf: userData.data.document.identification,
                     name: userData.data.name,
                  }}
                  finality={data?.finality.displayName}
                  expirationDateTime={data?.expirationDateTime}
                  confirmationDate={data?.createDateTime}
                  isLoading={isLoading}
               />
            )}
         </Stack>
         <SharedDataCollapse
            isLoading={isLoading}
            resourceGroup={data?.resourceGroups}
         />
         <Stack backgroundColor="white" padding="1rem" mt="0rem !important">
            {data?.status === 'ACTIVE' ? (
               <>
                  <Text color="gray.900" fontSize="md" mb="0.5rem">
                     <Trans
                        defaults="share.page.request.success.footer.message"
                        components={[<strong key={0} />]}
                     />
                  </Text>
                  <Button height="3rem" variant="outline" onClick={newRequest}>
                     {t('share.page.request.success.button.new.request')}
                  </Button>
                  <Button height="3rem" onClick={continueProcess}>
                     {t('share.page.request.success.button.continue')}
                  </Button>
               </>
            ) : (
               <Button height="3rem">
                  {t(
                     'share.page.request.success.button.continue.multiple.approvers',
                  )}
               </Button>
            )}
         </Stack>
      </Stack>
   )
}
