import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import pt from './pt.json'

i18n.use(initReactI18next).init({
   lng: 'pt',
   resources: {
      pt,
   },
   fallbackLng: 'pt',
   debug: false,
   ns: ['translations'],
   defaultNS: 'translations',
   keySeparator: false,
   interpolation: {
      escapeValue: false,
      formatSeparator: ',',
   },
   react: {
      wait: true,
   },
   initImmediate: false,
})
export default i18n
