import { server } from '@mocks/server'
import { screen, waitForElementToBeRemoved } from '@testing-library/react'
import { renderWithClient } from '@tests/utils'
import ConfirmarPagamento from '../../../pages/pagamentos/confirmar-pagamento'
import { useRouter } from 'next/router'

jest.mock('next/router', () => ({
   useRouter: jest.fn(),
}))

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

describe('<ConfirmaPagamento />', () => {
   it('displays a message if no id is provided', () => {
      ;(useRouter as jest.Mock).mockImplementation(() => ({
         query: { id: undefined },
      }))

      renderWithClient(<ConfirmarPagamento />)

      expect(
         screen.getByText('confirm.payment.page.id.error'),
      ).toBeInTheDocument()
   }),
      it('renders payment and account information when payment consent status is AWAITING_AUTHORISATION', async () => {
         ;(useRouter as jest.Mock).mockImplementation(() => ({
            query: { id: '123' },
         }))

         renderWithClient(<ConfirmarPagamento />)

         await waitForElementToBeRemoved(() =>
            screen.getByText(/loading\.\.\./i),
         )
         await waitForElementToBeRemoved(() =>
            document.querySelector('.chakra-skeleton'),
         )

         expect(screen.getByText(/r\$ 100,12/i)).toBeInTheDocument()
         expect(
            screen.getByText(/payment\.checkout\.available r\$ 8\.200,00/i),
         ).toBeInTheDocument()
      }),
      it('displays a message when payment consent status is REJECTED', async () => {
         ;(useRouter as jest.Mock).mockImplementation(() => ({
            query: { id: '456' },
         }))

         renderWithClient(<ConfirmarPagamento />)

         await waitForElementToBeRemoved(() =>
            screen.getByText(/loading\.\.\./i),
         )

         expect(
            screen.getByText('confirm.payment.page.rejected.status'),
         ).toBeInTheDocument()
      }),
      it('displays a message when payment consent status is AUTHORISED', async () => {
         ;(useRouter as jest.Mock).mockImplementation(() => ({
            query: { id: '678' },
         }))

         renderWithClient(<ConfirmarPagamento />)

         await waitForElementToBeRemoved(() =>
            screen.getByText(/loading\.\.\./i),
         )

         expect(
            screen.getByText('confirm.payment.page.confirmed.status'),
         ).toBeInTheDocument()
      })
})
