import { server } from '@mocks/server'
import { screen, waitForElementToBeRemoved } from '@testing-library/react'
import { renderWithClient } from '@tests/utils'
import PagamentosDetalhes from '../../../pages/pagamentos/detalhes/[id]'
import { useRouter } from 'next/router'

jest.mock('next/router', () => ({
   useRouter: jest.fn(),
}))

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

describe('<ConfirmaPagamento />', () => {
   it('renders payment and account information when payment consent status is AWAITING_AUTHORISATION', async () => {
      ;(useRouter as jest.Mock).mockImplementation(() => ({
         query: { id: '123' },
      }))

      renderWithClient(<PagamentosDetalhes />)

      await waitForElementToBeRemoved(() => screen.getByText(/loading\.\.\./i))
      await waitForElementToBeRemoved(() =>
         document.querySelector('.chakra-skeleton'),
      )

      expect(screen.getByText(/r\$ 100,12/i)).toBeInTheDocument()
      expect(
         screen.getByText(/payment\.checkout\.available r\$ 8\.200,00/i),
      ).toBeInTheDocument()
   }),
      it('displays payment consent information when status is REJECTED', async () => {
         ;(useRouter as jest.Mock).mockImplementation(() => ({
            query: { id: '456' },
         }))

         renderWithClient(<PagamentosDetalhes />)

         await waitForElementToBeRemoved(() =>
            screen.getByText(/loading\.\.\./i),
         )
         expect(screen.getByText('payment.status.REJECTED')).toBeInTheDocument()
      }),
      it('displays payment consent information when status is AUTHORISED', async () => {
         ;(useRouter as jest.Mock).mockImplementation(() => ({
            query: { id: '678' },
         }))

         renderWithClient(<PagamentosDetalhes />)

         await waitForElementToBeRemoved(() =>
            screen.getByText(/loading\.\.\./i),
         )

         expect(
            screen.getByText('payment.status.AUTHORISED'),
         ).toBeInTheDocument()
      })
})
