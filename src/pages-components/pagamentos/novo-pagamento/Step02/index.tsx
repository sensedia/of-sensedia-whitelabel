import React, { useContext } from 'react'
import { Box, Text, Flex } from '@chakra-ui/layout'
import { Input } from '@chakra-ui/react'
import { Button, SelectCustom } from '@components'
import { useTranslation } from 'react-i18next'
import { NewPaymentContext } from '../context'

interface Step01Props {
   onClickContinue: () => void
   onClickCancel: () => void
}

export function Step02({ onClickContinue, onClickCancel }: Step01Props) {
   const { t } = useTranslation()

   const {
      setState,
      state: { payment },
   } = useContext(NewPaymentContext)

   const handleSetState = (value: string) => {
      setState({
         ...payment,
         proxy: value,
      })
   }

   const pixKeyIsValid = () => {
      const pixKey = payment.proxy ? payment.proxy : ''
      switch (pixKey) {
         case pixKey.match('^[0-9]{11}$')?.input: //CPF
            return true
         case pixKey.match('^[0-9]{14}$')?.input: //CNPJ
            return true
         case pixKey.match('^(?!.{78})[a-z0-9.]+@[a-z0-9]+.[a-z]+.?([a-z]+)$')
            ?.input: //EMAIL
            return true
         case pixKey.match('^[+][0-9]{14}$')?.input: //PHONE
            return true
         case pixKey.match(
            '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}',
         )?.input: //EVP
            return true
         default:
            return false
      }
   }

   return (
      <Flex
         direction="column"
         justifyContent="space-between"
         minHeight="calc(100vh - 135px)"
         px="1.80rem"
         background="gray.100"
      >
         <Box>
            <Text
               py="0.625rem"
               fontSize="2xl"
               color="gray.900"
               fontWeight="bold"
            >
               {t('new.payment.transaction.amount.step.two.title')}
            </Text>
            <Text fontSize="lg" color="gray.800">
               {t('new.payment.transaction.amount.step.two.subtitle')}
            </Text>
            <Text
               py="0.625rem"
               fontSize="lg"
               color="brand.600"
               fontWeight="bold"
            >
               {payment?.authorisationServer?.organisationName}
            </Text>
            <SelectCustom
               label={t('new.payment.payment.methods.select.label')}
               options={[
                  {
                     label: 'PIX',
                     value: 'pix',
                  },
               ]}
               setValue={() => {}}
            />
            <Box mb="5rem">
               <Text
                  py="0.625rem"
                  fontSize="md"
                  color="gray.900"
                  fontWeight="bold"
               >
                  {t('new.payment.beneficiary.data.select.label')}
               </Text>
               <Input
                  data-testid="input-proxy"
                  height="3.5rem"
                  variant="outline"
                  placeholder={t('new.payment.pix.key.input.placeholder')}
                  borderColor="gray.900"
                  border="0.0625rem solid"
                  color="gray.900"
                  value={payment.proxy || ''}
                  onChange={(e) => handleSetState(e.target.value)}
               />
            </Box>
         </Box>
         <Box>
            <Button
               isFullWidth
               mb="0.75rem"
               onClick={() => onClickContinue()}
               disabled={!pixKeyIsValid()}
            >
               {t('default.word.continue')}
            </Button>
            <Button
               isFullWidth
               variant="outline"
               mb="0.75rem"
               onClick={() => onClickCancel()}
            >
               {t('default.word.cancel')}
            </Button>
         </Box>
      </Flex>
   )
}
