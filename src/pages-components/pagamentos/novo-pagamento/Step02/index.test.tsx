import { Step02 } from '.'

import { fireEvent, render, screen } from '@testing-library/react'
import { NewPaymentContext } from '../context'
import { NewPayment } from '@models'

const state = {
   payment: {} as NewPayment,
}

const mockCallback = jest.fn()

describe('Step02 Component', () => {
   test('should render Step02 correctly', () => {
      render(
         <NewPaymentContext.Provider value={{ state, setState: mockCallback }}>
            <Step02
               onClickContinue={mockCallback}
               onClickCancel={mockCallback}
            ></Step02>
         </NewPaymentContext.Provider>,
      )

      expect(
         screen.getByText('new.payment.transaction.amount.step.two.title'),
      ).toBeInTheDocument()

      fireEvent.change(screen.getByTestId('input-proxy'), {
         target: { value: 'new-pix-value' },
      })
      expect(mockCallback).toBeCalled()
   })
})
