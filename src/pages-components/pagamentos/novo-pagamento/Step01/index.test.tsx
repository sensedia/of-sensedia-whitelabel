import { Step01 } from '.'

import { render, screen } from '@testing-library/react'
import { NewPaymentContext } from '../context'
import { NewPayment } from '@models'

const state = {
   payment: {
      payment: {
         amount: '100,00',
      },
   } as NewPayment,
}

const mockCallback = jest.fn()

describe('Step01 Component', () => {
   test('should render Step01 correctly', () => {
      render(
         <NewPaymentContext.Provider value={{ state, setState: mockCallback }}>
            <Step01 openModalSelectInstitution={() => false}></Step01>
         </NewPaymentContext.Provider>,
      )

      const buttonSelectInstitution = screen.getByRole('button', {
         name: 'new.payment.payment.methods.pix',
      })

      expect(
         screen.getByText('new.payment.transaction.amount.step.one.title'),
      ).toBeInTheDocument()
      expect(screen.getByText('R$ 100,00')).toBeInTheDocument()
      expect(
         screen.getByText('new.payment.payment.methods'),
      ).toBeInTheDocument()

      expect(buttonSelectInstitution).toBeInTheDocument()
   })
})
