import React, { useContext } from 'react'
import { Box, Text, Flex } from '@chakra-ui/layout'
import theme from 'styles/theme'
import { Button } from '@components'
import { useTranslation } from 'react-i18next'
import { NewPaymentContext } from '../context'
import { formatCurrency } from '@utils'

interface Step01Props {
   openModalSelectInstitution: () => void
}

export function Step01({ openModalSelectInstitution }: Step01Props) {
   const { t } = useTranslation()

   const {
      state: { payment },
   } = useContext(NewPaymentContext)

   return (
      <Flex
         direction="column"
         justifyContent="space-between"
         minHeight="calc(100vh - 135px)"
         px="1.80rem"
         background="gray.100"
      >
         <Box position="relative">
            <Text
               py="0.625rem"
               fontSize="2xl"
               color="gray.900"
               fontWeight="bold"
            >
               {t('new.payment.transaction.amount.step.one.title')}
            </Text>
            <Text
               color={`${theme.colors.brand[600]}`}
               fontWeight="600"
               fontSize="1.5rem"
               bottom=".2rem"
               borderBottom={`2px solid ${theme.colors.brand[600]}`}
            >
               {' '}
               {formatCurrency(payment?.payment?.amount)}
            </Text>
         </Box>
         <Box>
            <Text
               py="0.625rem"
               fontSize="lg"
               color="gray.900"
               fontWeight="bold"
            >
               {t('new.payment.payment.methods')}
            </Text>
            <Button
               isFullWidth
               variant="outline"
               mb="0.75rem"
               onClick={() => openModalSelectInstitution()}
            >
               {t('new.payment.payment.methods.card')}
            </Button>
            <Button
               isFullWidth
               variant="outline"
               mb="0.75rem"
               onClick={() => openModalSelectInstitution()}
            >
               {t('new.payment.payment.methods.bank.slip')}
            </Button>
            <Button
               isFullWidth
               variant="outline"
               mb="0.75rem"
               onClick={() => openModalSelectInstitution()}
            >
               {t('new.payment.payment.methods.pix')}
            </Button>
         </Box>
      </Flex>
   )
}
