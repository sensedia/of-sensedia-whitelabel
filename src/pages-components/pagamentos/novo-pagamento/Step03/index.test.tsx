import { Step03 } from '.'

import { fireEvent, render, screen } from '@testing-library/react'
import { NewPaymentContext } from '../context'
import { DataPayment, NewPayment } from '@models'

const state = {
   payment: {
      authorisationServer: {
         organisationName: 'Crediconta',
      },
   } as NewPayment,
}

const dataPayment = {
   payment: {
      amount: '1000',
      currency: 'BRL',
   },
   identification: {
      company: 'Sensedia Testing',
      cpfCnpj: '111.222.333-44',
      name: 'Name Testing',
   },
   receiverData: {
      cpfCnpj: '999.888.777-66',
      name: 'Name Receiver',
   },
   debtorAccount: {
      ispb: '123',
      issuer: '1234',
      number: '12345',
      accountType: 'CACC',
      cpfCnpj: '999.888.777-44',
      name: 'Name Debtor',
      checkDigit: '9',
   },
} as DataPayment

const mockCallback = jest.fn()

describe('Step03 Component', () => {
   test('should render Step02 correctly', () => {
      render(
         <NewPaymentContext.Provider value={{ state, setState: mockCallback }}>
            <Step03
               loading={false}
               dataPayment={dataPayment}
               onClickContinue={mockCallback}
               onClickCancel={mockCallback}
            ></Step03>
         </NewPaymentContext.Provider>,
      )

      expect(
         screen.getByText('new.payment.transaction.amount.step.three.title'),
      ).toBeInTheDocument()
      expect(screen.getByText('Sensedia Testing')).toBeInTheDocument()
      expect(screen.getByText('Name Testing')).toBeInTheDocument()
      expect(screen.getByText('Crediconta')).toBeInTheDocument()
      expect(screen.getByText('AG 1234')).toBeInTheDocument()
      expect(screen.getByText('Cc 12345-9')).toBeInTheDocument()
      expect(screen.getByText('CPF 999.888.777-44')).toBeInTheDocument()
      expect(screen.getByText('Name Debtor')).toBeInTheDocument()
      expect(screen.getByText('CNPJ 999.888.777-66')).toBeInTheDocument()
      expect(screen.getByText('Name Receiver')).toBeInTheDocument()

      fireEvent.change(screen.getByTestId('input-pix-message'), {
         target: { value: 'new-pix-value' },
      })
      expect(mockCallback).toBeCalled()
   })
})
