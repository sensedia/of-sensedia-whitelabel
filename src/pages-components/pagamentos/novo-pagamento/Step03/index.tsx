import React, { useContext } from 'react'
import { Box, Text, Flex } from '@chakra-ui/layout'
import { Input, Divider } from '@chakra-ui/react'
import theme from 'styles/theme'
import { Button, Card, LabelValue } from '@components'
import { useTranslation } from 'react-i18next'
import { NewPaymentContext } from '../context'
import { DataPayment } from '@models'
import { formatCurrency } from '@utils'

interface Step01Props {
   onClickContinue: () => void
   onClickCancel: () => void
   dataPayment?: DataPayment
   loading: boolean
}

export function Step03({
   onClickContinue,
   onClickCancel,
   dataPayment,
   loading,
}: Step01Props) {
   const { t } = useTranslation()

   const {
      setState,
      state: { payment },
   } = useContext(NewPaymentContext)

   const handleSetState = (value: string) => {
      setState({
         ...payment,
         remittanceInformation: value,
      })
   }

   return (
      <Flex
         direction="column"
         justifyContent="space-between"
         minHeight="calc(100vh - 135px)"
         px="1.80rem"
         background="gray.100"
      >
         <Box>
            <Text
               py="0.625rem"
               fontSize="2xl"
               color="gray.900"
               fontWeight="bold"
            >
               {t('new.payment.transaction.amount.step.three.title')}
            </Text>
            <Card>
               <Text color="gray.900" fontWeight={700} fontSize="md">
                  {t('new.payment.checkout.total.label')}
               </Text>
               <Text fontSize="2xl" fontWeight="bold" color="gray.900">
                  {formatCurrency(payment?.payment?.amount)}
               </Text>
               <Divider background="gray.300" my="1rem" />
               {dataPayment?.identification && (
                  <LabelValue
                     label={t('new.payment.checkout.from.label')}
                     isLoading={false}
                     values={[
                        dataPayment?.identification.name,
                        dataPayment?.identification.company,
                     ]}
                  />
               )}
               <LabelValue
                  label={t('new.payment.checkout.to.label')}
                  isLoading={false}
                  values={[
                     payment?.authorisationServer?.organisationName,
                     `AG ${dataPayment?.debtorAccount.issuer}`,
                     `Cc ${dataPayment?.debtorAccount.number}-${dataPayment?.debtorAccount.checkDigit}`,
                     `CPF ${dataPayment?.debtorAccount.cpfCnpj}`,
                     `${dataPayment?.debtorAccount.name}`,
                  ]}
               />
               <LabelValue
                  label={t('new.payment.checkout.recipient.data.label')}
                  isLoading={false}
                  values={[
                     `CNPJ ${dataPayment?.receiverData.cpfCnpj}`,
                     dataPayment?.receiverData?.name || '',
                  ]}
               />
               <LabelValue
                  label={t('new.payment.checkout.payment.source.label')}
                  isLoading={false}
                  values={[`Pix Open Banking`]}
               />
               <Divider background="gray.300" my="1rem" />
               <Text color="gray.900" fontWeight={700} fontSize="md">
                  {t('new.payment.checkout.pix.message.label')}
               </Text>
               <Input
                  data-testid="input-pix-message"
                  pl="0rem"
                  mb="1rem"
                  color="gray.900"
                  fontWeight="500"
                  fontSize="lg"
                  border="0"
                  borderRadius="0"
                  borderBottom={`2px solid ${theme.colors.brand[600]}`}
                  placeholder={t(
                     'new.payment.checkout.pix.message.placeholder',
                  )}
                  onChange={(event) => handleSetState(event.target.value)}
                  value={payment?.remittanceInformation || ''}
               />
               <LabelValue
                  label={t('new.payment.checkout.payment.date.label')}
                  isLoading={false}
                  values={[new Intl.DateTimeFormat('pt-BR').format(new Date())]}
               />
            </Card>
            <Box
               background="gray.200"
               padding="1.5rem"
               borderRadius=".75rem"
               mb="1rem"
            >
               <Text
                  color="gray.900"
                  textTransform="uppercase"
                  textAlign="center"
                  fontSize="md"
                  fontWeight="bold"
               >
                  {t('new.payment.checkout.warning.box.title')}
               </Text>
               <Text
                  color="gray.900"
                  textAlign="center"
                  fontSize="md"
                  my="1rem"
               >
                  {t('new.payment.checkout.warning.box.description')}
               </Text>
               <Text
                  color="brand.600"
                  textAlign="center"
                  fontSize="md"
                  fontWeight="bold"
                  textDecoration="underline"
               >
                  {t('new.payment.checkout.warning.box.link')}
               </Text>
            </Box>
         </Box>
         <Box>
            <Button
               isFullWidth
               mb="0.75rem"
               onClick={() => onClickContinue()}
               isLoading={loading}
            >
               {t('default.word.continue')}
            </Button>
            <Button
               isFullWidth
               variant="outline"
               mb="0.75rem"
               onClick={() => onClickCancel()}
            >
               {t('default.word.cancel')}
            </Button>
         </Box>
      </Flex>
   )
}
