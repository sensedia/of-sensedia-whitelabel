import { NewPayment } from '@models'
import { createContext } from 'react'

export type NewPaymentContextType = {
   state: {
      payment: NewPayment
   }
   setState: (payment: NewPayment) => void
}

export const NewPaymentContext = createContext<NewPaymentContextType>({
   setState: () => {},
   state: {
      payment: {} as NewPayment,
   },
})

export type NewPaymentType = NewPaymentContextType['state']
