import React from 'react'
import { Header } from '.'
import { render } from '@testing-library/react'

describe('<Header />', () => {
   it('render correctly', () => {
      const { getByTestId } = render(
         <Header institution="Sensedia" status="PENDING" />,
      )

      expect(getByTestId('wrapper-main')).toBeTruthy()
   })
})
