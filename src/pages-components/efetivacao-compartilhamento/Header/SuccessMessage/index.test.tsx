import { SuccessMessage } from '.'
import { screen, render } from '@testing-library/react'

describe('<SuccessMessage />', () => {
   it('Should render correctly', () => {
      render(<SuccessMessage />)
      expect(screen.getByTestId('wrapper-main')).toBeTruthy()
   })
})
