import { Trans } from 'react-i18next'
import { CheckIcon } from '@chakra-ui/icons'
import { Flex, Stack, Text } from '@chakra-ui/react'

export function SuccessMessage() {
   return (
      <Stack data-testid="wrapper-main">
         <Flex
            backgroundColor="successLighter"
            direction="column"
            justify="center"
            alignItems="center"
            borderRadius="0.5rem"
            p="1rem"
         >
            <Flex
               backgroundColor="successLight"
               justify="center"
               alignItems="center"
               width="4.5rem"
               height="4.5rem"
               borderRadius="50%"
               mb="0.5rem"
            >
               <CheckIcon color="success" height="2rem" width="2rem" />
            </Flex>
            <Text align="center" color="success" fontSize="xl">
               <Trans
                  defaults="share.page.request.success.header.message"
                  components={[<strong key={0} />, <br key={1} />]}
               />
            </Text>
         </Flex>
      </Stack>
   )
}
