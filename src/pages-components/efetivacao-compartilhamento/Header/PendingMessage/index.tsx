import { TimeIcon } from '@chakra-ui/icons'
import { Flex, Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

export function PendingMessage() {
   const { t } = useTranslation()
   return (
      <Flex
         direction="column"
         justify="center"
         alignItems="center"
         bg="gray.200"
         borderRadius="0.5rem"
         py="1rem"
      >
         <Flex
            bg="gray.400"
            justify="center"
            alignItems="center"
            width="4.5rem"
            height="4.5rem"
            borderRadius="50%"
            mb="0.5rem"
         >
            <TimeIcon width="2rem" height="2rem" />
         </Flex>
         <Text fontSize="lg" color="gray.900" fontWeight="bold">
            {t('effective.sharing.data.access')}
         </Text>
         <Text fontSize="lg" color="gray.900">
            {t('effective.sharing.pending')}
         </Text>
      </Flex>
   )
}
