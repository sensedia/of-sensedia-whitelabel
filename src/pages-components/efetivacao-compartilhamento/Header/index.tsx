import React from 'react'
import { Box, Stack, Text } from '@chakra-ui/react'
import { Trans, useTranslation } from 'react-i18next'
import { SuccessMessage } from './SuccessMessage'
import { PendingMessage } from './PendingMessage'

interface HeaderPendingProps {
   institution: string
   isLoading?: boolean
   status: string
}

export function Header(props: HeaderPendingProps) {
   const { t } = useTranslation()

   return (
      <Stack py="1.25rem" bg="gray.100" data-testid="wrapper-main">
         {!props.isLoading ? (
            props.status === 'ACTIVE' ? (
               <SuccessMessage />
            ) : (
               <PendingMessage />
            )
         ) : null}
         {props.status !== 'ACTIVE' && !props.isLoading && (
            <Box py="1rem">
               <Text fontSize="lg" color="gray.900">
                  {t('effective.sharing.message')}
               </Text>
               <Text fontSize="lg" color="gray.900">
                  <Trans
                     i18nKey="effective.sharing.message.check.deadline"
                     values={{
                        institution: props.institution,
                     }}
                  />
               </Text>
            </Box>
         )}
      </Stack>
   )
}
