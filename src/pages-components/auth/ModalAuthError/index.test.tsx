import { ModalAuthError } from '.'
import { fireEvent, render, screen } from '@testing-library/react'

const onClick = jest.fn()

describe('<ModalAuthError />', () => {
   it('Render correctly', () => {
      render(<ModalAuthError isOpen={true} onClose={onClick} />)
      expect(screen.getByText('login.page.modal.error.title')).toBeTruthy()
   })

   it('Call close function on press exit', () => {
      render(<ModalAuthError isOpen={true} onClose={onClick} />)
      fireEvent.click(screen.getByTestId('close-button'))
      expect(onClick).toBeCalled()
   })

   it('Call close function on press footer button', () => {
      render(<ModalAuthError isOpen={true} onClose={onClick} />)
      fireEvent.click(screen.getByTestId('agree-button'))
      expect(onClick).toBeCalled()
   })
})
