import { useTranslation, Trans } from 'react-i18next'
import {
   Button,
   chakra,
   ListItem,
   Modal,
   ModalOverlay,
   ModalContent,
   ModalHeader,
   ModalFooter,
   ModalBody,
   ModalCloseButton,
   UnorderedList,
   Stack,
   Text,
} from '@chakra-ui/react'

interface ModalAuthErrorProps {
   isOpen: boolean
   onClose: () => void
}

export function ModalAuthError({ isOpen, onClose }: ModalAuthErrorProps) {
   const { t } = useTranslation()

   return (
      <Modal onClose={onClose} isOpen={isOpen}>
         <ModalOverlay />
         <ModalContent>
            <ModalHeader flexDirection="row">
               <Text color="brand.600">
                  {t('login.page.modal.error.title')}
               </Text>
               <ModalCloseButton color="danger" data-testid="close-button" />
            </ModalHeader>
            <ModalBody
               borderBottomWidth=".1rem"
               borderTopWidth=".1rem"
               py="1.5em"
               fontSize="lg"
            >
               <Text color="gray.900">
                  <Trans
                     defaults="login.page.modal.error.description.one"
                     components={[
                        <strong key="strong" />,
                        <chakra.span
                           color="brand.600"
                           key="color"
                           fontWeight={700}
                        />,
                     ]}
                  />
               </Text>
               <Text mt="1rem" color="gray.900">
                  <Trans
                     defaults="login.page.modal.error.description.two"
                     components={[<strong />]}
                  />
               </Text>
            </ModalBody>
            <ModalFooter flexDirection="column">
               <Stack
                  backgroundColor="gray.200"
                  borderRadius="0.4rem"
                  padding="1rem"
                  spacing="1rem"
               >
                  <Text color="gray.900" fontWeight={700}>
                     {t('login.page.modal.error.tips.title')}
                  </Text>
                  <UnorderedList spacing=".5rem">
                     <ListItem ml="1rem">
                        <Text color="gray.900">
                           <Trans
                              defaults="login.page.modal.error.tips.itemOne"
                              components={[<chakra.span color="brand.600" />]}
                           />
                        </Text>
                     </ListItem>
                     <ListItem ml="1rem">
                        <Text color="gray.900">
                           <Trans
                              defaults="login.page.modal.error.tips.itemTwo"
                              components={[<chakra.span color="brand.600" />]}
                           />
                        </Text>
                     </ListItem>
                  </UnorderedList>
               </Stack>
               <Button
                  data-testid="agree-button"
                  height="3.25rem"
                  isFullWidth
                  onClick={onClose}
                  mt="2rem"
               >
                  {t('login.page.modal.error.button')}
               </Button>
            </ModalFooter>
         </ModalContent>
      </Modal>
   )
}
