import { createContext, useContext } from 'react'

import { Share, ShareTransmitted } from '@models'

export interface SharedDetailsState {
   details?: Share & ShareTransmitted
   isLoading?: boolean
}

export const SharedDetailsPageContext = createContext({} as SharedDetailsState)

export const SharedDetailsPageProvider = SharedDetailsPageContext.Provider

export function useSharedDetailsPageContext() {
   return useContext(SharedDetailsPageContext)
}
