import React from 'react'
import { fireEvent, screen, render } from '@testing-library/react'
import { ModalActions } from '.'
import { Text } from '@chakra-ui/layout'
import { Button } from '@components'

describe('<ModalComponent />', () => {
   it('renders component correctly', () => {
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalActions
            header="Header"
            activator={activator}
            body="Content"
            buttonConfirmAction={<button>Button</button>}
         />,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Header')).toBeTruthy()
      expect(getByText('Content')).toBeTruthy()
      expect(getByText('Button')).toBeInTheDocument()
   })

   it('fires event to close modal with header button', () => {
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalActions
            header="Header"
            body="Content"
            activator={activator}
            buttonConfirmAction={<Button>Test</Button>}
         />,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Content')).toBeTruthy()

      fireEvent.click(screen.getByTestId('header-close-button'))

      expect(getByText('Content')).not.toBeVisible()
   })

   it('fires event to close modal with cancel button', () => {
      const onClick = jest.fn()
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalActions
            header="Header"
            body="Content"
            activator={activator}
            buttonConfirmAction={<button>Button</button>}
         />,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Header')).toBeTruthy()
      expect(getByText('Content')).toBeTruthy()
      expect(getByText('Button')).toBeTruthy()

      const cancelButton = getByText('default.word.cancel')

      fireEvent.click(cancelButton)

      expect(cancelButton).not.toBeVisible()
   })
})
