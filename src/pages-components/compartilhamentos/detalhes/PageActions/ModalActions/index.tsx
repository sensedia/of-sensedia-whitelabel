import {
   Text,
   useDisclosure,
   Modal,
   ModalOverlay,
   ModalContent,
   ModalHeader,
   ModalBody,
   ModalCloseButton,
   Divider,
   ModalFooter,
   Box,
   Button,
   Stack,
} from '@chakra-ui/react'
import React, { ReactNode } from 'react'
import theme from 'styles/theme'
import { useTranslation } from 'react-i18next'

type ModalActionsProps = {
   activator: ReactNode
   header: ReactNode
   footer?: ReactNode
   body: ReactNode
   padding?: string
   buttonConfirmAction: ReactNode
}

export function ModalActions(props: ModalActionsProps) {
   const { t } = useTranslation()
   const { isOpen, onOpen, onClose } = useDisclosure()

   return (
      <>
         <Box onClick={onOpen}>{props.activator}</Box>

         <Modal scrollBehavior="inside" isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />

            <ModalContent margin="11vh 1.5rem" height="auto" maxHeight="80vh">
               <ModalHeader display={props.header ? 'block' : 'none'}>
                  <Text color="gray.900">{props.header}</Text>
                  <Divider
                     position="absolute"
                     marginTop="0.75rem"
                     borderColor="gray.500"
                     left="0"
                  />
               </ModalHeader>
               <ModalCloseButton
                  display={props.header ? 'block' : 'none'}
                  paddingTop="0.2rem"
                  fontWeight="bold"
                  color="brand.600"
                  data-testid="header-close-button"
               />
               <ModalBody
                  css={{
                     '&::-webkit-scrollbar': {
                        width: '0.25rem',
                     },
                     '&::-webkit-scrollbar-track': {
                        width: '0.375rem',
                     },
                     '&::-webkit-scrollbar-thumb': {
                        background: `${theme.colors.brand[600]}`,
                        borderRadius: '1.5rem',
                     },
                  }}
                  overflow="scroll"
                  padding={!props.padding ? '1rem' : props.padding}
                  {...props}
               >
                  {props.body}
               </ModalBody>

               <ModalFooter padding="0">
                  <Stack
                     direction="column"
                     width="100%"
                     padding="0rem 1.25rem 1.25rem"
                  >
                     {props.buttonConfirmAction}
                     <Button isFullWidth onClick={onClose} variant="outline">
                        {t('default.word.cancel')}
                     </Button>
                  </Stack>
               </ModalFooter>
            </ModalContent>
         </Modal>
      </>
   )
}
