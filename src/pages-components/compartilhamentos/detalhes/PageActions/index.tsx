import React from 'react'
import { Shares, Transmitteds } from '@services'
import { useRouter } from 'next/router'
import { Trans, useTranslation } from 'react-i18next'
import { useSharedDetailsPageContext } from '@pages-components/compartilhamentos/context'
import { Box, chakra, Flex, Text, useBoolean, useToast } from '@chakra-ui/react'
import { CardActions, Button } from '@components'
import { ModalActions } from './ModalActions'

export function PageActions() {
   const toast = useToast()
   const { t } = useTranslation()
   const { back } = useRouter()
   const router = useRouter()
   const { details } = useSharedDetailsPageContext()
   const [isLoading, setIsLoading] = useBoolean(false)

   const onPressChange = () => {
      router.push(`/alterar-compartilhamento/${details?.shareId}`)
   }

   const onPressRenew = () => {
      router.push(
         `/alterar-compartilhamento/${details?.shareId}?operation=RENEW`,
      )
   }

   function onPressBack() {
      back()
   }

   function finishShare() {
      setIsLoading.on()
      if (details?.shareType === 'RECEIVING') {
         callFinishShareReceiving()
      } else {
         callFinishShareTransmitting()
      }
   }

   function callFinishShareReceiving() {
      Shares.finish(details?.shareId as '')
         .then(() => {
            successFinish()
         })
         .catch(() => {
            launchToastError()
         })
   }

   function callFinishShareTransmitting() {
      Transmitteds.finish(details?.shareId as '')
         .then(() => {
            successFinish()
         })
         .catch(() => {
            launchToastError()
         })
   }

   async function successFinish() {
      setIsLoading.off()
      launchToastSuccess()
      onPressBack()
   }

   function launchToastSuccess() {
      return toast({
         title: t('shared.page.share.request.finish.success'),
         status: 'success',
         isClosable: true,
      })
   }

   function launchToastError() {
      setIsLoading.off()
      return toast({
         title: t('default.request.error'),
         status: 'error',
         isClosable: true,
      })
   }

   return (
      <Flex p="1.1rem" direction="column">
         {details?.status === 'ACTIVE' && (
            <>
               <Text fontSize="lg" fontWeight={700} mb="0.75rem">
                  {t('shared.page.share.details.actions')}
               </Text>
               {/* Encerramento */}
               <ModalActions
                  activator={
                     <CardActions
                        title={t(
                           'shared.page.share.details.actions.finish.title',
                        )}
                        description={t(
                           'shared.page.share.details.actions.finish.description',
                        )}
                     />
                  }
                  header={t(
                     'shared.page.share.details.actions.finish.modal.title',
                  )}
                  body={
                     <Box px="0.5rem">
                        <Text color="gray.900" marginBottom="1.5rem">
                           <Trans
                              defaults="shared.page.share.details.actions.finish.modal.description"
                              components={[
                                 <strong key="strong" />,
                                 <chakra.span color="danger" key="color" />,
                              ]}
                           />
                        </Text>
                        <Text>
                           <Text color="gray.900" fontWeight={700}>
                              {t(
                                 'shared.page.share.details.actions.finish.modal.confirmMessage',
                              )}
                           </Text>
                        </Text>
                     </Box>
                  }
                  buttonConfirmAction={
                     <Button
                        isFullWidth
                        isLoading={isLoading}
                        onClick={finishShare}
                        bg="danger"
                        _active={{ backgroundColor: 'dangerHover' }}
                        _hover={{ backgroundColor: 'dangerHover' }}
                     >
                        {t(
                           'shared.page.share.details.actions.finish.modal.accept',
                        )}
                     </Button>
                  }
               />
            </>
         )}
         {details?.shareType === 'RECEIVING' && details?.status === 'ACTIVE' && (
            <>
               {/* Alteração */}
               <ModalActions
                  activator={
                     <CardActions
                        title={t(
                           'shared.page.share.details.actions.change.title',
                        )}
                        description={t(
                           'shared.page.share.details.actions.change.description',
                        )}
                     />
                  }
                  header={t('default.word.alter')}
                  body={
                     <Box px="0.5rem">
                        <Text color="gray.900" marginBottom="1.5rem">
                           <Trans
                              defaults="modal.alter.shares.confirm.message"
                              components={[<strong key="strong" />]}
                           />
                        </Text>
                        <Text>
                           <Text color="gray.900" fontWeight={700}>
                              {t('modal.alter.shares.confirm.question')}
                           </Text>
                        </Text>
                     </Box>
                  }
                  buttonConfirmAction={
                     <Button isFullWidth onClick={onPressChange} bg="brand.500">
                        {t('modal.alter.shares.confirm.button')}
                     </Button>
                  }
               />

               {/* Renovação */}
               <ModalActions
                  activator={
                     <CardActions
                        title={t(
                           'shared.page.share.details.actions.renew.title',
                        )}
                        description={t(
                           'shared.page.share.details.actions.renew.description',
                        )}
                     />
                  }
                  header={t('default.word.renew')}
                  body={
                     <Box px="0.5rem">
                        <Text color="gray.900" marginBottom="1.5rem">
                           <Trans
                              defaults="modal.renew.shares.confirm.message"
                              components={[
                                 <chakra.span color="danger" key="color" />,
                              ]}
                           />
                        </Text>
                        <Text>
                           <Text color="gray.900" fontWeight={700}>
                              {t('modal.renew.shares.confirm.question')}
                           </Text>
                        </Text>
                     </Box>
                  }
                  buttonConfirmAction={
                     <Button isFullWidth onClick={onPressRenew} bg="brand.500">
                        {t('modal.renew.shares.confirm.button')}
                     </Button>
                  }
               />
            </>
         )}
         <Button variant="outline" onClick={onPressBack} mt={4}>
            {t('shared.page.share.details.button')}
         </Button>
      </Flex>
   )
}
