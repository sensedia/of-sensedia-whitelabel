import {
   Flex,
   Skeleton,
   Menu,
   MenuButton,
   MenuList,
   Text,
   Link as LinkStyle,
} from '@chakra-ui/react'
import { BackButton, DotsIcon } from '@components'
import { shareStatusEnum } from '@constants'
import { useSharedDetailsPageContext } from '@pages-components/compartilhamentos/context'
import { formatDateWithHour } from '@utils'
import React from 'react'
import { useTranslation } from 'react-i18next'

export function PageHeader() {
   const { t } = useTranslation()
   const { isLoading, details } = useSharedDetailsPageContext()

   function shareStatusColor() {
      return details?.status === shareStatusEnum.ACTIVE
         ? 'success'
         : details?.status === shareStatusEnum.PENDING
         ? 'warning'
         : 'gray.600'
   }

   const onPressHistory = () => {}

   return (
      <>
         <BackButton path="/compartilhamentos" />
         <Flex align="center" justify="space-between" size="lg">
            {isLoading || !details ? (
               <>
                  <Skeleton height="1.25rem" width="35%" />
                  <Skeleton height="1.25rem" width="45%" />
               </>
            ) : (
               <>
                  <Text
                     fontSize="md"
                     fontWeight={700}
                     color={shareStatusColor()}
                  >
                     {t('shared.page.share.status')}
                     {details?.status
                        ? t(
                             `shared.component.cardShare.status.${details.status}`,
                          )
                        : null}
                  </Text>
                  <Text fontSize="md" fontWeight={700} color="gray.600">
                     {formatDateWithHour(details.createDateTime)}
                  </Text>
               </>
            )}
         </Flex>
         <Flex align="center" justify="space-between" size="lg">
            {isLoading ? (
               <Skeleton height="1.25rem" width="25%" />
            ) : (
               <>
                  <Text fontSize="2xl" fontWeight={700} color="gray.900">
                     {details?.shareType === 'RECEIVING'
                        ? details?.authorisationServer?.customerFriendlyName
                        : details?.organisationName}
                  </Text>
                  <Menu isLazy>
                     <MenuButton>
                        <DotsIcon h={25} w={25} />
                     </MenuButton>
                     <MenuList>
                        <LinkStyle onClick={onPressHistory}>
                           <Text
                              align="center"
                              color="brand.600"
                              decoration="underline"
                              fontSize="md"
                              fontWeight={700}
                           >
                              {t('shared.page.share.details.menu')}
                           </Text>
                        </LinkStyle>
                     </MenuList>
                  </Menu>
               </>
            )}
         </Flex>
      </>
   )
}
