import { Box, Stack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Button } from '@components'

interface FooterButtonProps {
   onClickContinue: () => void
   onClickCancel: () => void
   isLoading: boolean
}

export default function FooterButtons({
   onClickContinue,
   onClickCancel,
   isLoading,
}: FooterButtonProps) {
   const { t } = useTranslation()

   return (
      <Box height="100%" data-testid="footer-bottons">
         <Stack direction="column" width="100%" py="0.5rem" fontSize="lg">
            <Button
               bg="brand.500"
               variant="solid"
               mb="1rem"
               isFullWidth
               onClick={onClickContinue}
               data-testid="button-continue"
               isLoading={isLoading}
            >
               {t('default.word.continue')}
            </Button>
            <Button
               color="brand.500"
               variant="link"
               isFullWidth
               onClick={onClickCancel}
               data-testid="button-cancel"
            >
               {t('default.word.cancel')}
            </Button>
         </Stack>
      </Box>
   )
}
