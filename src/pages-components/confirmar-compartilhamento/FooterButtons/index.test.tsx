import React from 'react'
import FooterButtons from '.'
import { fireEvent, render, screen } from '@testing-library/react'

describe('<CollapsableButton />', () => {
   it('render correctly', () => {
      render(
         <FooterButtons onClickContinue={() => {}} onClickCancel={() => {}} />,
      )
      expect(screen.getByTestId('footer-bottons')).toBeTruthy()
   })

   it('should call callback functions', () => {
      const onClickContinue = jest.fn()
      const onClickCancel = jest.fn()
      render(
         <FooterButtons
            onClickContinue={onClickContinue}
            onClickCancel={onClickCancel}
         />,
      )

      fireEvent.click(screen.getByTestId('button-continue'))

      expect(onClickContinue).toBeCalled()

      fireEvent.click(screen.getByTestId('button-cancel'))

      expect(onClickCancel).toBeCalled()
   })
})
