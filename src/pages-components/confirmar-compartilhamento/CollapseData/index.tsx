import { Box, Stack, Text, Collapse, useDisclosure } from '@chakra-ui/react'
import { ToggleSize } from '@components'
import React from 'react'

interface CollapseDataProps {
   title: string | React.ReactNode
   children?: React.ReactNode
}

export default function CollapseData(props: CollapseDataProps) {
   const { isOpen, onToggle } = useDisclosure()

   return (
      <Stack shadow="md" width="100%" borderRadius="0.5rem" marginBottom="1rem">
         <Box
            bg="white"
            padding="1rem"
            borderTopRadius="0.5rem"
            borderBottom="0.063rem solid"
            borderColor="gray.200"
            marginBottom="-0.5rem"
         >
            <Text fontSize="md" color="gray.900" fontWeight="bold">
               {props.title}
            </Text>
         </Box>
         <Box>
            <Collapse in={isOpen} animateOpacity={false} unmountOnExit={true}>
               <Box padding="1rem">{props.children}</Box>
            </Collapse>
            <ToggleSize show={isOpen} onClick={onToggle} />
         </Box>
      </Stack>
   )
}
