import React from 'react'
import CollapsableButton from '.'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'

describe('<CollapsableButton />', () => {
   it('render correctly', () => {
      render(<CollapsableButton title="Card content test"></CollapsableButton>)
      expect(screen.getByText('Card content test')).toBeTruthy()
   })

   it('should render child component', async () => {
      render(
         <CollapsableButton title="Card content test">
            <span data-testid="children"></span>
         </CollapsableButton>,
      )

      fireEvent.click(screen.getByRole('expand-collapse'))

      await waitFor(() => {
         expect(screen.getByTestId('children')).toBeTruthy()
      })
   })
})
