import { Box, Stack, Text } from '@chakra-ui/react'
import { Card, LabelValue } from '@components'
import { useTranslation } from 'react-i18next'
import { formatDate } from '@utils'
import { useQuery } from 'react-query'
import { LoggedUser } from 'services/LoggedUser'
import { ShareTransmitted, DeadLine } from '@models'

interface Props {
   data: ShareTransmitted
}

export interface Approver {
   cpf: string
   name: string
}

export function Step02({ data }: Props) {
   const { t } = useTranslation()
   const { data: userData } = useQuery('loggedUser', LoggedUser.get)

   const converDateExpiration = (date: string) => {
      if (date) {
         return formatDate(date)
      }
      return ''
   }

   const convertDeadLine = (deadlines: DeadLine[]) => {
      return `${deadlines[0].total} meses`
   }

   return (
      <Box height="100%" backgroundColor="gray.50">
         <Stack p="1.538rem" backgroundColor="gray.50">
            <Card backgroundColor="gray.200">
               <Text fontSize="md" color="gray.800">
                  {t('confirm.shares.header.subtitle')}
               </Text>
               <Text
                  fontSize="2xl"
                  color="gray.900"
                  fontWeight="bold"
                  mt="1rem"
               >
                  {data.organisationName}
               </Text>
            </Card>
            <Text fontSize="lg" color="gray.900" fontWeight="bold" mt="1.2rem">
               {t('confirm.sharing.resume.sub.title')}
            </Text>
            <Card marginTop="1.438rem !important">
               <LabelValue
                  label={t('confirm.sharing.resume.client.identity')}
                  isLoading={false}
                  values={[
                     `${userData?.data.document.rel}: ${userData?.data.document.identification}`,
                     `${userData?.data.name}`,
                  ]}
               />
               <LabelValue
                  label={t('confirm.sharing.resume.receiver')}
                  isLoading={false}
                  values={[data.organisationName]}
               />
               <LabelValue
                  label={t('confirm.sharing.resume.deadline')}
                  isLoading={false}
                  values={[
                     `${convertDeadLine(
                        data.deadLines,
                     )} | ${converDateExpiration(data.expirationDateTime)}`,
                  ]}
               />
            </Card>

            {data.approvers.length > 1 && (
               <>
                  <Text
                     fontSize="lg"
                     color="gray.900"
                     fontWeight="bold"
                     marginTop="2rem!important"
                  >
                     {t('confirm.shares.authorization')}
                  </Text>
                  <Text color="gray.800" mt="1.2rem" fontSize="md">
                     {t('confirm.shares.authorization.message')}
                  </Text>
                  {data.approvers.map((approver, index) => (
                     <Card marginTop="1.438rem!important" key={index}>
                        <LabelValue
                           label={t('confirm.shares.aprove.label')}
                           isLoading={false}
                           values={[approver.approverId]}
                        />
                     </Card>
                  ))}
               </>
            )}
         </Stack>
      </Box>
   )
}
