import { Box, Stack, Text } from '@chakra-ui/react'
import { SelectCustom } from '@components'
import { ShareTransmitted, DataPermission } from '@models'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Step01Provider } from './context'
import { PersonalData } from './PersonalData'

interface Props {
   data: ShareTransmitted
}

export function Step01({ data }: Props) {
   const { t } = useTranslation()

   return (
      <Stack
         margin="0"
         bg="gray.100"
         borderBottom="0.063rem solid"
         borderColor="gray.200"
      >
         <Box
            display="flex"
            flexDirection="column"
            alignItems="start"
            backgroundColor="white"
            borderBottom="0.063rem solid"
            borderColor="gray.200"
            paddingX="0.875rem"
         >
            <Text p="0.625rem" fontSize="lg" color="gray.800" fontWeight="bold">
               {t('confirm.share.select.data.origin')}
            </Text>
            <Text p="0.625rem" fontSize="md" color="gray.800">
               {t('confirm.share.message.advice')}
            </Text>
            <Text
               p="0.625rem"
               fontSize="2xl"
               color="gray.900"
               fontWeight="bold"
            >
               {t('confirm.share.data.share')}
            </Text>
         </Box>
         <Step01Provider value={data}>
            {data &&
               data.resourceGroups.map((rg) => {
                  return (
                     <Box
                        marginTop="0!important"
                        display="flex"
                        flexDirection="column"
                        alignItems="start"
                        height="auto"
                        paddingX="1.5rem"
                        key={rg.resourceGroupId}
                     >
                        <Text
                           fontSize="lg"
                           color="gray.800"
                           fontWeight="bold"
                           marginY="1rem"
                        >
                           {rg.displayName}
                        </Text>
                        {rg?.resources?.filter((r) => r.type === 'ACCOUNT')
                           .length > 0 && (
                           <SelectCustom
                              label={t('confirm.shares.origin.data')}
                              options={rg?.resources
                                 .filter((r) => r.type === 'ACCOUNT')
                                 .map((r) => ({
                                    value: r.resourceId,
                                    label: r.displayName,
                                 }))}
                           />
                        )}
                        {rg.dataPermissions &&
                           rg.dataPermissions.map((dp, index) => (
                              <PersonalData
                                 data={dp as DataPermission}
                                 key={index}
                              />
                           ))}
                     </Box>
                  )
               })}
         </Step01Provider>
      </Stack>
   )
}
