import React from 'react'
import { PersonalData } from '.'
import { render, screen } from '@testing-library/react'

describe('<PersonalData />', () => {
   it('render correctly', () => {
      render(
         <PersonalData
            data={{
               detail: 'test',
               displayName: 'test',
               items: ['test1', 'test2'],
               permissionCode: 'test',
               required: false,
            }}
         />,
      )
      expect(screen.getByText('card.component.expand')).toBeTruthy()
   })
})
