import CollapseData from '../../CollapseData'
import React from 'react'
import { DataPermission } from '@models'
import { Text, Divider } from '@chakra-ui/react'

interface Props {
   data: DataPermission
}

export function PersonalData({ data }: Props) {
   return (
      <CollapseData title={data?.displayName}>
         <Text color="gray.900" fontWeight={700} fontSize="sm">
            {data.detail}
         </Text>
         {!!data.items?.length && <Divider my="1rem" />}
         {data.items?.map((item) => (
            <Text color="gray.900" fontWeight={700} fontSize="sm" key={item}>
               {item}
            </Text>
         ))}
      </CollapseData>
   )
}
