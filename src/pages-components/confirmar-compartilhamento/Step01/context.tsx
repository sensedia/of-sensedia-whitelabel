import { ShareTransmitted } from '@models'
import { createContext, useContext } from 'react'

export const Step01Context = createContext({} as ShareTransmitted)

export const Step01Provider = Step01Context.Provider

export function useStep01() {
   const context = useContext(Step01Context)

   return context
}
