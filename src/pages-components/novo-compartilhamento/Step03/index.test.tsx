import { Step03 } from '.'

import { StepsNewShareProvider } from '../../novo-compartilhamento/context'
import { render, screen } from '@testing-library/react'

const state = [
   {
      loggedUser: {
         document: {
            identification: '111111111111',
            rel: 'CPF',
         },
         name: 'Fulado de Tal',
         email: 'email@email.com',
      },
      finality: {
         displayName: 'Abertura de Conta Corrente',
         finalityId: 'string',
      },
      institution: {
         id: '1',
         name: 'Wiscredi',
         description:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum in tincidunt sapien. Sed ac velit in nunc aliquam condimentum.',
         participants: [
            'Instituição 1',
            'Instituição 2',
            'Instituição 3',
            'Instituição 4',
            'Instituição 5',
            'Instituição 6',
            'Instituição 7',
            'Instituição 8',
            'Instituição 9',
            'Instituição 10',
            'Instituição 11',
         ],
      },
      deadline: 6,
   },
]

describe('Step03 Component', () => {
   test('renders Step03 correctly ', () => {
      render(
         <StepsNewShareProvider value={state}>
            <Step03></Step03>
         </StepsNewShareProvider>,
      )
      expect(
         screen.getByRole('button', { name: 'default.word.continue' }),
      ).toBeInTheDocument()
   })
})
