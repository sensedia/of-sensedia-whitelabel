import { createContext, Dispatch, SetStateAction, useContext } from 'react'

import { Finality, Institution, NewShareResponse, User } from '@models'

export interface NewShareContextState {
   loggedUser: User
   finality: Finality
   institution: Institution
   deadline: number
   NewShareResponse?: NewShareResponse
}

export const StepsNewShareContext = createContext(
   [] as unknown as [
      NewShareContextState,
      Dispatch<SetStateAction<NewShareContextState>>,
   ],
)

export const StepsNewShareProvider = StepsNewShareContext.Provider

export function useNewShareContext() {
   return useContext(StepsNewShareContext)
}
