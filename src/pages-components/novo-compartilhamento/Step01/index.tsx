import { Box, Stack, Text } from '@chakra-ui/layout'
import { Button, Card, LabelValue } from '@components'

import { useTranslation } from 'react-i18next'

import { useNewShareContext } from '../context'

interface Step01Props {
   openModalSelectInstitution: () => void
}

export function Step01({ openModalSelectInstitution }: Step01Props) {
   const { t } = useTranslation()

   const [{ loggedUser, finality }] = useNewShareContext()

   return (
      <Box px="1.80rem">
         <Card>
            <LabelValue
               label={t('new.share.client.identity')}
               isLoading={!loggedUser.document}
               values={[
                  `${loggedUser?.document?.rel}: ${loggedUser?.document?.identification}`,
                  loggedUser?.name,
               ]}
            />
         </Card>
         <Card>
            <LabelValue
               label={t('default.data.use.purpose')}
               isLoading={!finality.displayName}
               values={[finality.displayName]}
            />
         </Card>
         <Box p="0.375rem">
            <Text fontSize="md">
               {t('new.share.select.financial.institution')}
            </Text>
            <Text fontSize="xl" color="gray.900">
               <b>{t('new.share.select.financial.additional.text')}</b>
            </Text>
            <Stack mt="1.25rem">
               <Button onClick={openModalSelectInstitution}>
                  {t('new.share.select.institution')}
               </Button>
            </Stack>
         </Box>
      </Box>
   )
}
