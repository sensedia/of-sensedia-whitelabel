import { Step01 } from '.'

import { StepsNewShareProvider } from '../../novo-compartilhamento/context'

import { render, screen } from '@testing-library/react'

const state = [
   {
      loggedUser: {
         document: {
            identification: '068.498.166-59',
            rel: 'CPF',
         },
         name: 'Miguel Silva',
         email: 'miguel.silva@exemplo.com',
      },

      finality: {
         displayName: 'Abertura de Conta Corrente',
         finalityId: 'string',
      },

      deadline: 6,
      institution: {
         id: '',
         name: '',
         description: '',
         participants: [],
      },
   },
]

describe('Step01 Component', () => {
   test('should render Step01 correctly', () => {
      render(
         <StepsNewShareProvider value={state}>
            <Step01 OpenModalSelectInstitution={() => false}></Step01>
         </StepsNewShareProvider>,
      )

      const buttonSelectInstitution = screen.getByRole('button', {
         name: 'new.share.select.institution',
      })

      expect(screen.getByText('new.share.client.identity')).toBeInTheDocument()
      expect(screen.getByText('default.data.use.purpose')).toBeInTheDocument()
      expect(
         screen.getByText('new.share.select.financial.institution'),
      ).toBeInTheDocument()
      expect(
         screen.getByText('new.share.select.financial.additional.text'),
      ).toBeInTheDocument()

      expect(buttonSelectInstitution).toBeInTheDocument()
   })
})
