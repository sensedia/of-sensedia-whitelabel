import { DataPermission, ResourceGroup, DeadLine } from '@models'

import { useState } from 'react'
import { useUpdateEffect } from '@hooks'
import { Box, Checkbox, Divider, Flex, Text } from '@chakra-ui/react'
import { SwapIcon, Card, Button, SelectCustom } from '@components'
import { useTranslation, Trans } from 'react-i18next'
import { CollapsableContainer } from './CollapsableContainer'
import { CardInfo } from './CardInfo'
import { LinkBrand } from './LinkBrand'
import { useNewShareContext } from '../context'
import { calculateDeadlineDate } from '@utils'

interface Step02Props {
   confirmShare: (data: any) => void
   openModalSelectInstitution: () => void
   isLoading: boolean
}

interface Payload {
   dataPermissions: Array<DataPermission>
   deadLine: DeadLine
}

export function Step02({
   confirmShare,
   openModalSelectInstitution,
   isLoading,
}: Step02Props) {
   const { t } = useTranslation()
   const [dataRequired, setDataRequired] = useState<Array<ResourceGroup>>()
   const [dataOptional, setDataOptional] = useState<Array<ResourceGroup>>()
   const [dataPayload, setDataPayload] = useState<Payload>({
      deadLine: {
         total: 0,
         type: '',
      },
      dataPermissions: [],
   })
   const [{ deadline, institution, NewShareResponse }, setState] =
      useNewShareContext()

   useUpdateEffect(() => {
      if (NewShareResponse) {
         setDataRequired(
            NewShareResponse?.resourceGroups.map((rg) => ({
               ...rg,
               dataPermissions: rg.dataPermissions.filter(
                  (dp) => dp.required === true,
               ),
            })),
         )
         setDataOptional(
            NewShareResponse?.resourceGroups
               .map((rg) => ({
                  ...rg,
                  dataPermissions: rg.dataPermissions
                     .filter((dp) => dp.required === false)
                     .map((dp) => ({ ...dp, selected: true })),
               }))
               .filter((rg) =>
                  rg.dataPermissions.some((dp) => dp.required === false),
               ),
         )
      }
   }, [NewShareResponse])

   useUpdateEffect(() => {
      confirmShare(dataPayload)
   }, [dataPayload])

   function changeCheck(indexResource: number, indexPermission: number) {
      if (dataOptional) {
         const newCheckItems = [...dataOptional]
         newCheckItems[indexResource].dataPermissions[
            indexPermission
         ].selected =
            !newCheckItems[indexResource].dataPermissions[indexPermission]
               .selected
         setDataOptional(newCheckItems)
      }
   }

   function selectAllOptional() {
      if (dataOptional) {
         const newCheckItems = [...dataOptional]
         newCheckItems.forEach((rg) =>
            rg.dataPermissions.forEach((dp) => (dp.selected = true)),
         )
         setDataOptional(newCheckItems)
      }
   }

   function changeDeadline(value: number) {
      setState((state) => ({ ...state, deadline: value }))
   }

   function constructPayload() {
      const temp = dataRequired?.map((dr) => {
         const permissionsSelected = dataOptional
            ?.find((x) => x.resourceGroupId === dr.resourceGroupId)
            ?.dataPermissions.filter((x) => x.selected)
         return permissionsSelected?.length
            ? dr.dataPermissions.concat(permissionsSelected)
            : dr.dataPermissions
      })
      temp &&
         setDataPayload({
            deadLine: {
               total: deadline,
               type: 'MONTHS',
            },
            dataPermissions: Array.prototype.concat.apply([], temp),
         })
   }

   return (
      <>
         <Box bg="gray.50">
            <Box bg="gray.50" px="1.80rem">
               <Text mb="1rem" color="gray.800" fontSize="md">
                  <Trans defaults="dateselect.accountinfo" />
               </Text>
               <Card p={0} mb="3rem">
                  <Text p="0.75rem 1rem" color="gray.900" fontWeight="bold">
                     {t('dateselect.selected.institution')}
                  </Text>
                  <Divider borderColor="gray.400"></Divider>
                  <Flex
                     p="1rem"
                     alignItems="center"
                     justifyContent="space-between"
                  >
                     <Text fontSize="lg" color="gray.900" fontWeight="bold">
                        {institution?.customerFriendlyName}
                     </Text>
                     <SwapIcon
                        cursor="pointer"
                        onClick={openModalSelectInstitution}
                        color="brand.600"
                        boxSize={6}
                     />
                  </Flex>
               </Card>
               <Text
                  color="gray.900"
                  fontSize="lg"
                  fontWeight="bold"
                  mb="0.75rem"
               >
                  {t('dateselect.mandatory.data')}
               </Text>
            </Box>
            {dataRequired &&
               dataRequired
                  .filter((rg) =>
                     rg.dataPermissions.some((dp) => dp.required === true),
                  )
                  .map((rg) => (
                     <CollapsableContainer
                        key={rg.resourceGroupId}
                        title={rg.displayName}
                     >
                        {rg.dataPermissions.map((dp, index) => (
                           <CardInfo
                              key={index}
                              title={dp.displayName}
                              body={dp.detail}
                              itemDescription={dp.displayName}
                              scrollBoxBody={
                                 dp.items &&
                                 dp.items.map((item) => ({
                                    label: item,
                                 }))
                              }
                           />
                        ))}
                     </CollapsableContainer>
                  ))}
            <Divider borderColor="gray.300" mb="0.75rem" />
            <Flex
               width="100%"
               justifyContent="space-between"
               py="0.375rem"
               px="1.80rem"
               bg="gray.50"
               mb="0.75rem"
            >
               <Text color="gray.900" fontSize="lg" fontWeight="bold">
                  {t('dateselect.optional.data')}
               </Text>
               <LinkBrand
                  action={() => {
                     selectAllOptional()
                  }}
                  text={t('dateselect.selectall')}
               />
            </Flex>
            {dataOptional &&
               dataOptional.map((rg, indexR) => (
                  <CollapsableContainer
                     key={rg.resourceGroupId}
                     title={rg.displayName}
                  >
                     {rg.dataPermissions.map((dp, indexP) => (
                        <CardInfo
                           key={indexP}
                           title={
                              <Checkbox
                                 isChecked={dp.selected}
                                 value={''}
                                 colorScheme="purple"
                                 onChange={() => changeCheck(indexR, indexP)}
                              >
                                 {dp.displayName}
                              </Checkbox>
                           }
                           itemDescription={dp.displayName}
                           body={dp.detail}
                           scrollBoxBody={
                              dp.items &&
                              dp.items.map((item) => ({
                                 label: item,
                              }))
                           }
                        />
                     ))}
                  </CollapsableContainer>
               ))}
            <Divider borderColor="gray.300" mb="1rem" />
            <Box py="0.375rem" px="1.80rem">
               <Text fontWeight="bold" mb="1rem">
                  {t('dateselect.sharing.deadline')}
               </Text>
               <Text fontSize="lg" color="brand.600" mb="1rem">
                  {t('dateselect.sharing.deadline.info')}
               </Text>

               <SelectCustom
                  label={t('dateselect.deadline')}
                  options={[
                     { label: '6 meses', value: 6 },
                     { label: '1 ano', value: 12 },
                  ]}
                  setValue={(val) => {
                     changeDeadline(val)
                  }}
               />
               <Flex color="brand.600">
                  <Text mr="1rem">
                     {t('dateselect.sharing.deadline.expires')}
                  </Text>
                  <Text fontWeight="bold">
                     {calculateDeadlineDate(deadline)}
                  </Text>
               </Flex>
               <Button
                  my="1.5rem"
                  onClick={() => constructPayload()}
                  isLoading={isLoading}
               >
                  {t('default.word.continue')}
               </Button>
            </Box>
         </Box>
      </>
   )
}
