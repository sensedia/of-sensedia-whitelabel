import React from 'react'
import { Link } from '@chakra-ui/react'

type LinkProps = {
   text: string
   href?: string
   action?: () => void
}

export function LinkBrand(props: LinkProps) {
   return (
      <>
         <Link
            data-testid="redirect"
            color="brand.600"
            textDecoration="underline"
            fontSize="sm"
            fontWeight="bold"
            onClick={props.action}
         >
            {props.text}
         </Link>
      </>
   )
}
