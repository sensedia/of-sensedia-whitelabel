import React from 'react'
import { LinkBrand } from '.'
import { fireEvent, render } from '@testing-library/react'

describe('<LinkBrand />', () => {
   it('render with text', () => {
      const onClick = jest.fn()
      const { getByText } = render(
         <LinkBrand action={onClick} text="Testing" />,
      )

      expect(getByText('Testing')).toBeTruthy()
   })

   it('trigger action', () => {
      const onClick = jest.fn()

      const { getByText, unmount } = render(
         <LinkBrand action={onClick} text="Testing" />,
      )
      fireEvent.click(getByText('Testing'))
      expect(onClick).toBeCalled()
      unmount()
   })
})
