import React from 'react'
import { ScrollBox } from '.'
import { render } from '@testing-library/react'

describe('<ScrollBox />', () => {
   it('render correctly', () => {
      const list = [{ label: 'item1' }, { label: 'item2' }]
      const { getByText } = render(<ScrollBox body={list}></ScrollBox>)

      expect(getByText('item1')).toBeTruthy()
      expect(getByText('item2')).toBeTruthy()
   })
})
