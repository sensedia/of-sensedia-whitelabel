import { Box, Stack, Text } from '@chakra-ui/layout'
import {
   Button,
   Card,
   LabelValue,
   ModalComponent,
   RedirecionamentoPopup,
} from '@components'
import { Terms } from 'components/Terms'

import { useTranslation } from 'react-i18next'
import { useAlterShareContext } from '../context'

import { calculateDeadlineDate } from '@utils'
import { useState } from 'react'
import { useUpdateEffect } from '@hooks'
interface Step02Props {
   urlRedirect: string
}

export function Step02({ urlRedirect }: Step02Props) {
   const { t } = useTranslation()
   const [bank, setBank] = useState({
      img: '',
      name: '',
      link: '',
   })
   const [redirect, setRedirect] = useState<boolean>(false)

   const [{ finality, deadline, NewShareResponse }] = useAlterShareContext()

   useUpdateEffect(() => {
      if (urlRedirect) {
         setBank({
            ...bank,
            name: NewShareResponse?.authorisationServer
               .customerFriendlyName as string,
            link: urlRedirect,
         })
      }
   }, [urlRedirect])

   return (
      <Stack height="100%" backgroundColor="white">
         <Box bg="gray.200" borderRadius="0.5rem" p="1.25rem 1rem">
            <Text fontSize="md">{t('confirm.shares.header.subtitle')}</Text>
            <Text color="gray.900" fontWeight="700" fontSize="xl" mt="1rem">
               {NewShareResponse?.authorisationServer?.customerFriendlyName}
            </Text>
         </Box>
         <Box marginTop="1rem !important">
            <Text color="gray.900" fontWeight="700">
               {t('shared.page.share.details.abstract.title')}
            </Text>
            <Card>
               <LabelValue
                  label={t('default.selected.institution')}
                  isLoading={false}
                  values={[
                     NewShareResponse?.authorisationServer
                        ?.customerFriendlyName as string,
                  ]}
               />
               <LabelValue
                  label={t('default.data.use.purpose')}
                  isLoading={false}
                  values={[finality?.displayName]}
               />
               <LabelValue
                  label={t('default.sharing.term')}
                  isLoading={false}
                  values={[
                     `${deadline} meses | ${calculateDeadlineDate(deadline)}`,
                  ]}
               />
            </Card>
            <ModalComponent
               activator={
                  <Text
                     cursor="pointer"
                     textDecoration="underline"
                     fontWeight="700"
                     margin="2rem 0"
                     padding="0 1.25rem"
                     fontSize="md"
                     color="purple"
                     textAlign="center"
                  >
                     {t('confirm.shares.read.terms.title')}
                  </Text>
               }
               header={t('home.popup.terms.title')}
            >
               <Terms></Terms>
            </ModalComponent>
            <Button onClick={() => setRedirect(true)}>
               {t('default.word.continue')}
            </Button>
         </Box>
         <RedirecionamentoPopup
            isOpen={redirect}
            onClose={() => setRedirect(false)}
            bank={bank}
            message={t('redirect.popup.message')}
            redirectMessage={t(
               'redirect.popup.redirect.message.to.transmitter',
            )}
         />
      </Stack>
   )
}
