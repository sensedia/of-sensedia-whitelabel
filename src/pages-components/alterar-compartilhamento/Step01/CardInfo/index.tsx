import React, { ReactNode } from 'react'
import { Box, Divider, Flex, Text } from '@chakra-ui/react'
import { Card, ModalComponent } from '@components'
import { Trans, useTranslation } from 'react-i18next'
import Link from 'next/link'
import { ScrollBox } from '../ScrollBox/index'
import { openBankingSite } from '@constants'

type InfoCardProp = {
   title: string | ReactNode
   body: string
   scrollBoxBody: any
   itemDescription: string
}

export function CardInfo(props: InfoCardProp) {
   const { t } = useTranslation()

   return (
      <>
         <Card p={0} mb="1.5rem">
            <Box padding="0.75rem 1rem" fontWeight="bold" color="gray.900">
               {props.title}
            </Box>
            <Divider borderColor="gray.400"></Divider>
            <Text padding="0.5rem 1rem">{props.body}</Text>
            <Flex padding="1rem" justifyContent="center" bg="gray.100">
               <ModalComponent
                  header={t('dateselect.user.info.registry.data')}
                  padding="1rem 1rem 0rem 1rem"
                  activator={
                     <Text
                        data-testid="redirect"
                        color="brand.600"
                        textDecoration="underline"
                        fontSize="sm"
                        fontWeight="bold"
                     >
                        {t('dateselect.view.all.shared')}
                     </Text>
                  }
                  footer={
                     <Box
                        flexDirection="column"
                        alignItems="center"
                        justifyContent="center"
                        display="flex"
                        height="5rem"
                        width="100%"
                        bg="gray.100"
                     >
                        <Text fontSize="sm">
                           {t('institution.page.modal.details')}
                        </Text>
                        <Link href={openBankingSite}>
                           <a>
                              <Text
                                 cursor="pointer"
                                 textDecoration="underline"
                                 fontWeight="700"
                                 fontSize="sm"
                                 color="brand.600"
                              >
                                 {t('home.link.citzen')}
                              </Text>
                           </a>
                        </Link>
                     </Box>
                  }
               >
                  <Text paddingBottom="1rem">
                     <Trans
                        i18nKey="dateselect.modal.content"
                        values={{
                           data: props.itemDescription,
                        }}
                     />
                  </Text>
                  <ScrollBox body={props.scrollBoxBody}></ScrollBox>
               </ModalComponent>
            </Flex>
         </Card>
      </>
   )
}
