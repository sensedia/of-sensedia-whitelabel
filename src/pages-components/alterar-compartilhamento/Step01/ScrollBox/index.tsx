import React from 'react'
import { Box } from '@chakra-ui/react'
import { Text, ListItem, OrderedList } from '@chakra-ui/layout'

import theme from 'styles/theme'

type ScrollBoxProps = {
   body: any[]
}

export function ScrollBox(props: ScrollBoxProps) {
   return (
      <>
         <Box
            css={{
               '&::-webkit-scrollbar': {
                  width: '0.25rem',
               },
               '&::-webkit-scrollbar-track': {
                  width: '0.375rem',
               },
               '&::-webkit-scrollbar-thumb': {
                  background: `${theme.colors.brand[600]}`,
                  borderRadius: '1.5rem',
               },
            }}
            height="12.5rem"
            bg={'gray.100'}
            overflow="scroll"
            padding="1rem 1.5rem 1rem 1rem"
         >
            <OrderedList styleType="disc">
               {props.body?.map((data: any, i: number) => (
                  <ListItem key={i}>
                     {data.label}
                     {data.obs ? <Text color="gray.600">{data.obs}</Text> : ''}
                  </ListItem>
               ))}
            </OrderedList>
         </Box>
      </>
   )
}
