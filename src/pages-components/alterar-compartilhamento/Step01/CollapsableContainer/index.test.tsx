import React from 'react'
import { CollapsableContainer } from '.'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import { Text } from '@chakra-ui/layout'

describe('<CollapsableContainer />', () => {
   it('render correctly with title and content', () => {
      const { getByText } = render(
         <CollapsableContainer title="TitleTesting">
            <Text>Content</Text>
         </CollapsableContainer>,
      )

      expect(getByText('TitleTesting')).toBeTruthy()
      expect(getByText('Content')).toBeTruthy()
   })

   it('collapses', async () => {
      render(
         <CollapsableContainer title="TitleTesting">
            <Text>Content</Text>
         </CollapsableContainer>,
      )
      const collapse = screen.getByTestId('collapsable')
      fireEvent.click(collapse)

      await waitFor(() => {
         expect(screen.queryByText('Content')).not.toBeInTheDocument()
      })

      fireEvent.click(collapse)

      await waitFor(() => {
         expect(screen.queryByText('Content')).toBeInTheDocument()
      })
   })

   it('expands', async () => {
      render(
         <CollapsableContainer title="TitleTesting">
            <Text>Content</Text>
         </CollapsableContainer>,
      )
      const collapse = screen.getByTestId('collapsable')

      fireEvent.click(collapse)
      fireEvent.click(collapse)

      await waitFor(() => {
         expect(screen.queryByText('Content')).toBeInTheDocument()
      })
   })
})
