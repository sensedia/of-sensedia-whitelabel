import { createContext, Dispatch, SetStateAction, useContext } from 'react'

import { Finality, Institution, NewShareResponse, User } from '@models'

export interface AlterShareContextState {
   loggedUser: User
   finality: Finality
   institution: Institution
   deadline: number
   NewShareResponse?: NewShareResponse
}

export const StepsAlterShareContext = createContext(
   [] as unknown as [
      AlterShareContextState,
      Dispatch<SetStateAction<AlterShareContextState>>,
   ],
)

export const StepsAlterShareProvider = StepsAlterShareContext.Provider

export function useAlterShareContext() {
   return useContext(StepsAlterShareContext)
}
