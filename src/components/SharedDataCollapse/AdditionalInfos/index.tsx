import {
   Collapse,
   Flex,
   Box,
   Skeleton,
   Text,
   useDisclosure,
} from '@chakra-ui/react'
import { Card, ListItems, ToggleSize } from '@components'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { AdditionalInfo } from '@models'

interface Props {
   data: Array<AdditionalInfo>
   isLoading?: boolean
}

export function AdditionalInfos({ data, isLoading }: Props) {
   const { t } = useTranslation()
   const { isOpen, onToggle } = useDisclosure()

   return (
      <Box>
         <Card padding="0" mt="1rem">
            <Flex
               borderBottomWidth=".1rem"
               borderColor="gray.200"
               padding="1.1rem"
            >
               <Text color="gray.900" fontSize="md" fontWeight={700}>
                  {t('shared.component.sharedData.additionalData')}
               </Text>
            </Flex>
            <Collapse startingHeight={120} in={isOpen}>
               <Flex align="flex-start" direction="column" padding="1.1rem">
                  {isLoading ? (
                     <Skeleton
                        height="1.5rem"
                        width="80%"
                        data-testid="loading"
                     />
                  ) : (
                     data.map((info) => (
                        <ListItems
                           label={info.key}
                           key={info.key}
                           isLoading={isLoading}
                           items={[info.value]}
                        />
                     ))
                  )}
               </Flex>
            </Collapse>
            <ToggleSize show={isOpen} onClick={onToggle} />
         </Card>
      </Box>
   )
}
