import { AdditionalInfos } from '.'
import { render, screen } from '@testing-library/react'
import share from '../../../tests/default-value-share.json'

describe('<AdditionalInfos />', () => {
   it('should render component', () => {
      render(
         <AdditionalInfos
            data={(share[0] as any).resourceGroups[0].additionalInfos}
         />,
      )
      expect(screen.getByText('INTERNAL_ID')).toBeTruthy()
      expect(screen.queryByTestId('loading')).toBeNull()
   })

   it('should render loading component when loading is true', () => {
      render(
         <AdditionalInfos
            isLoading
            data={(share[0] as any).resourceGroups[0].additionalInfos}
         />,
      )
      expect(screen.getByTestId('loading')).toBeTruthy()
   })
})
