import { DataPermissions } from '.'
import { render, screen } from '@testing-library/react'
import share from '../../../tests/default-value-share.json'

describe('<DataPermissions />', () => {
   it('should render component', () => {
      render(
         <DataPermissions
            data={(share[0] as any).resourceGroups[0].dataPermissions}
         />,
      )
      expect(screen.getByText('string')).toBeTruthy()
      expect(screen.queryByTestId('loading')).toBeNull()
   })

   it('should render loading component when loading is true', () => {
      render(
         <DataPermissions
            isLoading
            data={(share[0] as any).resourceGroups[0].dataPermissions}
         />,
      )
      expect(screen.getByTestId('loading')).toBeTruthy()
   })
})
