import { Flex, Box, Skeleton } from '@chakra-ui/react'
import React from 'react'
import { DataPermission } from '@models'
import { CardInfo } from '@components/CardInfo'

interface Props {
   data: Array<DataPermission>
   isLoading?: boolean
}

export function DataPermissions({ data, isLoading }: Props) {
   return (
      <Box pt="1rem">
         {data.map((permission, index) => (
            <Flex align="flex-start" direction="column" key={index}>
               {isLoading ? (
                  <Skeleton height="1.5rem" width="80%" data-testid="loading" />
               ) : (
                  <CardInfo
                     title={permission.displayName}
                     itemDescription={permission.displayName}
                     body={permission.detail}
                     scrollBoxBody={
                        permission.items &&
                        permission.items.map((item) => ({
                           label: item,
                        }))
                     }
                  />
               )}
            </Flex>
         ))}
      </Box>
   )
}
