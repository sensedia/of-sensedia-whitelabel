import React, { useState } from 'react'
import { Box, BoxProps, Collapse, Stack, Text } from '@chakra-ui/react'
import { ChevronDownIcon } from '@chakra-ui/icons'
import { ResourceGroup } from '@models'
import { DataPermissions } from './DataPermissions'
import { AdditionalInfos } from './AdditionalInfos'

interface SharedDataCollapsePops extends BoxProps {
   resourceGroup?: Array<ResourceGroup>
   isLoading?: boolean
}

export function SharedDataCollapse({
   resourceGroup,
   backgroundColor,
   isLoading,
   ...props
}: SharedDataCollapsePops) {
   const [isOpen, setToggle] = useState('')

   function handleToggle(key: string) {
      if (isOpen === key) {
         return setToggle('')
      }

      return setToggle(key)
   }

   function handleRotate(key: string) {
      return key === isOpen ? -180 : 0
   }

   return (
      <Box {...props}>
         {resourceGroup?.map((item) => (
            <Stack key={item.resourceGroupId}>
               <Stack
                  align="center"
                  backgroundColor={backgroundColor}
                  borderBottomWidth=".1rem"
                  borderColor="gray.400"
                  direction="row"
                  justify="space-between"
                  onClick={() => handleToggle(item.resourceGroupId)}
                  px="1.1rem"
               >
                  <Text color="gray.900" fontSize="2xl" fontWeight={700}>
                     {item.displayName}
                  </Text>
                  <Box
                     transform={`rotate(${handleRotate(
                        item.resourceGroupId,
                     )}deg)`}
                     transitionDuration=".3s"
                  >
                     <ChevronDownIcon color="brand.600" w={10} h={10} />
                  </Box>
               </Stack>
               <Collapse
                  in={isOpen === item.resourceGroupId}
                  animateOpacity={false}
                  unmountOnExit={true}
               >
                  <Box
                     bg="gray.100"
                     borderBottomWidth=".1rem"
                     borderColor="gray.400"
                     p="0 1.1rem"
                  >
                     <DataPermissions
                        key={item.displayName}
                        data={item.dataPermissions}
                        isLoading={isLoading}
                     />
                     {item.additionalInfos && (
                        <AdditionalInfos
                           data={item.additionalInfos}
                           isLoading={isLoading}
                        />
                     )}
                  </Box>
               </Collapse>
            </Stack>
         ))}
      </Box>
   )
}
