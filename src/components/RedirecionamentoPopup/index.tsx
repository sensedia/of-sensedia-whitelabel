import { ArrowDownIcon } from '@chakra-ui/icons'
import {
   Modal,
   ModalContent,
   ModalHeader,
   ModalBody,
   Stack,
   Text,
   Box,
} from '@chakra-ui/react'
import { CancelButton, LogoObp, LogoBrandIcon } from '@components'
import styled from '@emotion/styled'
import { useTranslation } from 'react-i18next'
import { useRouter } from 'next/router'
import { useEffect } from 'react'

interface Props {
   onClose: () => void
   isOpen: boolean
   bank: {
      img: string
      name: string
      link: string
   }
   message: string
   redirectMessage: string
}

const ImgWrapper = styled.div`
   width: 13.3125rem;
   height: 2.3125rem;
`

export function RedirecionamentoPopup({
   bank,
   isOpen,
   onClose,
   message,
   redirectMessage,
}: Props) {
   const { t } = useTranslation()
   const router = useRouter()

   useEffect(() => {
      if (isOpen) {
         const timer = setTimeout(() => {
            router.push(bank.link)
         }, 7000)
         return () => clearTimeout(timer)
      }
   }, [isOpen])

   return (
      <>
         <Modal
            isOpen={isOpen}
            onClose={onClose}
            size="full"
            colorScheme="brand"
         >
            <ModalContent
               margin="0"
               borderRadius="none"
               display="flex"
               alignItems="center"
            >
               <Box maxWidth="40rem" width="inherit" padding="0.625rem">
                  <CancelButton onClick={onClose} role="close" />
                  <ModalHeader color="gray.900">
                     {t('redirect.popup.title')}
                  </ModalHeader>
                  <Stack paddingLeft="0.75rem">
                     <LogoObp margin="0" marginTop="1.25rem" />
                     <Text
                        color="gray.900"
                        fontSize="sm"
                        paddingLeft="0.75rem"
                        margin="0!important"
                     >
                        {message}
                     </Text>
                  </Stack>
                  <ModalBody marginTop="2.5rem">
                     <Stack backgroundColor="gray.100" borderRadius="0.625rem">
                        <Box padding="1.875rem" margin="0!important">
                           <ImgWrapper>
                              <LogoBrandIcon />
                           </ImgWrapper>
                        </Box>
                        <Box
                           padding="0.938rem"
                           paddingRight="1.875rem"
                           justifyContent="space-between"
                           alignItems="center"
                           display="flex"
                           backgroundColor="gray.200"
                           color="gray.700"
                           margin="0!important"
                        >
                           <ArrowDownIcon h={8} w={8} flex="0.3" />
                           <Text flex="1" fontWeight="bold" fontSize="0.75rem">
                              {redirectMessage}
                           </Text>
                        </Box>
                        <Box padding="1.875rem" margin="0rem!important">
                           <ImgWrapper>
                              {bank.img ? (
                                 <img src={bank.img} />
                              ) : (
                                 <Text fontSize="2xl">{bank.name}</Text>
                              )}
                           </ImgWrapper>
                        </Box>
                     </Stack>
                  </ModalBody>
               </Box>
            </ModalContent>
         </Modal>
      </>
   )
}
