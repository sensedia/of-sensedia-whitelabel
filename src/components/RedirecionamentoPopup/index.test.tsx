import React from 'react'
import { RedirecionamentoPopup } from '.'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'

describe('<BackButton />', () => {
   it('render correctly', () => {
      const onClose = jest.fn()
      const bank = {
         name: 'Sensedia Bank',
         img: '',
         link: '',
      }
      render(
         <RedirecionamentoPopup
            isOpen={true}
            onClose={onClose}
            bank={bank}
            message=""
            redirectMessage=""
         />,
      )

      expect(screen.getByText('Sensedia Bank')).toBeTruthy()
   })

   it('should call onClose when click on close button', async () => {
      const onClose = jest.fn()
      const bank = {
         name: 'Sensedia Bank',
         img: '',
         link: '',
      }
      render(
         <RedirecionamentoPopup
            isOpen={true}
            onClose={onClose}
            bank={bank}
            message=""
            redirectMessage=""
         />,
      )

      fireEvent.click(screen.getByRole('close'))

      await waitFor(() => {
         expect(onClose).toBeCalledTimes(1)
      })
   })
})
