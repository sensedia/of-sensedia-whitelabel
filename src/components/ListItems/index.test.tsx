import React from 'react'
import { ListItems } from '.'
import { render } from '@testing-library/react'

describe('<ListItems />', () => {
   it('render without crashing', () => {
      const { getByText, unmount } = render(
         <ListItems label="test-label" items={['test-value']} />,
      )
      expect(getByText('test-label')).toBeTruthy()
      expect(getByText('test-value')).toBeTruthy()
      unmount()
   })
})
