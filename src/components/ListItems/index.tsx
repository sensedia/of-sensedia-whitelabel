import { ListItem, Flex, Skeleton, Text, UnorderedList } from '@chakra-ui/react'

interface ListItemsProps {
   label: string
   isLoading?: boolean
   items: Array<any>
}

export function ListItems({ label, isLoading, items }: ListItemsProps) {
   return (
      <Flex direction="column" mb="0.8rem">
         <Text color="gray.900" fontWeight={700} fontSize="sm">
            {label}
         </Text>
         {isLoading ? (
            <Skeleton height="1.5rem" width="80%" />
         ) : (
            <UnorderedList ml="1.3rem">
               {items.map((item) => (
                  <ListItem color="gray.900" key={item} fontSize="sm">
                     {item}
                  </ListItem>
               ))}
            </UnorderedList>
         )}
      </Flex>
   )
}
