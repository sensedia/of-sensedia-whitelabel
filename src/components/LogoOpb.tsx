import { Box, BoxProps } from '@chakra-ui/react'
import styled from '@emotion/styled'
import { LogoOpbIcon } from '@components'

const LogoWrapper = styled.div`
   width: 12.25rem;
   height: 3.5rem;
`

export function LogoObp(props: BoxProps) {
   return (
      <Box
         height="3.88rem"
         alignItems="center"
         marginBottom="1rem"
         justifyContent="center"
         {...props}
      >
         <LogoWrapper>
            <LogoOpbIcon />
         </LogoWrapper>
      </Box>
   )
}
