import { Flex, IconButton, Text } from '@chakra-ui/react'
import React, { ReactElement, ReactNode } from 'react'

type IconTextBoxProp = {
   content: ReactNode
   icon?: ReactElement
   onClick?: () => void
}

export function IconTextBox(props: IconTextBoxProp) {
   return (
      <>
         <Flex
            boxShadow="lg"
            bg="white"
            borderRadius="0.5rem"
            cursor="pointer"
            height="auto"
            marginBottom="0.75rem"
            onClick={props.onClick}
         >
            <IconButton
               _active={{ backgroundColor: 'brand.600' }}
               _hover={{ backgroundColor: 'brand.600' }}
               minWidth="4.5rem"
               height="5rem"
               background="brand.600"
               fontSize="2.5rem"
               borderRadius="0.5rem 0 0 0.5rem"
               border="0"
               variant="outline"
               aria-label="Normal"
               icon={props.icon}
            />
            <Text
               fontWeight="700"
               margin="auto 0"
               padding="0 1rem"
               fontSize="lg"
               color="gray.900"
            >
               {' '}
               {props.content}{' '}
            </Text>
         </Flex>
      </>
   )
}
