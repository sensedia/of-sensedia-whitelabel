import React from 'react'
import { fireEvent, render } from '@testing-library/react'
import { IconTextBox } from '.'
import { PeopleIcon } from '../Icons/People'

describe('<IconTextBox />', () => {
   it('render correctly with texts and icons', () => {
      const { getByText } = render(
         <IconTextBox icon={<PeopleIcon />} content="Testing" />,
      )

      expect(getByText('Testing')).toBeTruthy()
      expect(
         getByText('Testing').previousElementSibling?.firstElementChild,
      ).toBeDefined()
   })

   it('render correctly without icon', () => {
      const { getByText } = render(<IconTextBox content="Testing" />)

      // eslint-disable-next-line no-unused-expressions
      expect(getByText('Testing').previousElementSibling?.firstElementChild)
         .toBeEmptyDOMElement
   })

   it('trigger action', () => {
      const onClick = jest.fn()

      const { getByText } = render(
         <IconTextBox
            onClick={onClick}
            icon={<PeopleIcon />}
            content="Testing"
         />,
      )
      fireEvent.click(getByText('Testing'))
      expect(onClick).toBeCalled()
   })
})
