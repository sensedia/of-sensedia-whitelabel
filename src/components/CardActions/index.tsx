import { ChevronRightIcon } from '@chakra-ui/icons'
import { Box, Flex, Text } from '@chakra-ui/react'

interface CardActionsProps {
   title: string
   description: string
   action?: () => void
}

export function CardActions({ action, title, description }: CardActionsProps) {
   return (
      <Box
         bg="white"
         borderRadius="0.5rem"
         cursor="pointer"
         mb="0.8rem"
         shadow="md"
         width="100%"
         onClick={action}
      >
         <Flex align="center" justify="space-between" px="1.1rem" py=".7rem">
            <Text fontSize="md" color="gray.900" fontWeight={700}>
               {title}
            </Text>
            <ChevronRightIcon color="brand.600" w={9} h={9} />
         </Flex>
         <Flex bg="gray.100" px="1.1rem" py=".9rem" borderBottomRadius="0.5rem">
            <Text color="gray.900" fontSize="md">
               {description}
            </Text>
         </Flex>
      </Box>
   )
}
