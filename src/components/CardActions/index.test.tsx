import React from 'react'
import { CardActions } from './index'
import { fireEvent, render } from '@testing-library/react'

describe('<CardActions />', () => {
   it('render without crashing', () => {
      const onClick = jest.fn()

      const { getByText, unmount } = render(
         <CardActions
            action={onClick}
            title="test title"
            description="test description"
         />,
      )
      expect(getByText('test title')).toBeTruthy()
      expect(getByText('test description')).toBeTruthy()
      unmount()
   })

   it('trigger action', () => {
      const onClick = jest.fn()

      const { getByText, unmount } = render(
         <CardActions
            action={onClick}
            title="test title"
            description="test description"
         />,
      )
      fireEvent.click(getByText('test description'))
      expect(onClick).toBeCalled()
      unmount()
   })
})
