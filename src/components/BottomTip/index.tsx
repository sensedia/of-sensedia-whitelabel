import { CloseIcon } from '@chakra-ui/icons'
import { Box, Text, Flex, Collapse, useDisclosure } from '@chakra-ui/react'
import React, { useState } from 'react'

type BottomTipProps = {
   title: string | JSX.Element
   body: string | JSX.Element
}

export function BottomTip(props: BottomTipProps) {
   const { isOpen, onToggle } = useDisclosure({ defaultIsOpen: true })
   const [overlayIsVisible, setOverlayIsVisible] = useState(true)

   const toggleCollapse = () => {
      onToggle()
      setOverlayIsVisible(!overlayIsVisible)
   }

   return (
      <>
         {overlayIsVisible && (
            <Box
               position="fixed"
               display="flex"
               left="0px"
               top="0px"
               width="100vw"
               height="100vh"
               background="gray.900"
               opacity="0.7"
               z-index="100"
            />
         )}
         <Box
            id="bottom-tip"
            position="fixed"
            bottom="0rem"
            w="100%"
            maxW="40rem"
            z-index="101"
         >
            <Collapse in={isOpen} animateOpacity={false} unmountOnExit={true}>
               <Box bg="gray.200" width="100%">
                  <Flex
                     width="100%"
                     justifyContent="flex-end"
                     padding="1rem 1rem 0 1rem"
                     marginBottom="1rem"
                  >
                     <CloseIcon
                        data-testid="close-button"
                        onClick={toggleCollapse}
                        color="brand.600"
                        cursor="pointer"
                        width="0.8rem"
                        height="0.8rem"
                        fontWeight="700"
                     ></CloseIcon>
                  </Flex>
                  <Box padding="0 1rem 1rem 1rem">
                     <Text mb="1rem" fontWeight="bold">
                        {props.title}
                     </Text>
                     <Text>{props.body}</Text>
                  </Box>
               </Box>
            </Collapse>
         </Box>
      </>
   )
}
