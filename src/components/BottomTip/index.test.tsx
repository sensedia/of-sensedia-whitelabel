import React from 'react'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import { BottomTip } from '.'

describe('<BottomTip />', () => {
   it('render correctly with texts', () => {
      render(<BottomTip title="TestingTitle" body="TestingBody" />)

      expect(screen.getByText('TestingTitle')).toBeTruthy()
      expect(screen.getByText('TestingBody')).toBeTruthy()
   })

   it('call onClick function on button press', async () => {
      render(<BottomTip title="TestingTitle" body="TestingBody" />)
      const closeButton = screen.getByTestId('close-button')
      fireEvent.click(closeButton)
      await waitFor(() => {
         expect(screen.queryByText('TestingTitle')).not.toBeInTheDocument()
         expect(screen.queryByText('TestingBody')).not.toBeInTheDocument()
      })
   })
})
