import { Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

export function Terms() {
   const { t } = useTranslation()

   return (
      <>
         <Text>{t('home.popup.terms.body')}</Text>
      </>
   )
}
