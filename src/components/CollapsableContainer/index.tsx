import React, { useState } from 'react'
import {
   Box,
   Flex,
   useDisclosure,
   Text,
   Collapse,
   Divider,
} from '@chakra-ui/react'
import { ChevronUpIcon } from '@chakra-ui/icons'

type CollapsableContainerProps = {
   children: React.ReactNode
   title: string
}

export function CollapsableContainer(props: CollapsableContainerProps) {
   const { isOpen, onToggle } = useDisclosure({ defaultIsOpen: true })
   const [rotate, setRotate] = useState('0')

   const toggleCollapse = () => {
      setRotate(rotate === '0' ? '-180' : '0')
      onToggle()
   }

   return (
      <>
         <Box background="gray.100">
            <Divider borderColor="gray.300" mb="1rem" />
            <Box
               p="0.375rem"
               px="1.80rem"
               borderTop="0.6rem"
               borderBottom="0.6rem"
               borderColor="gray.400"
               margin="0 !important"
               width="100%"
               overflow="hidden"
               {...props}
            >
               <Flex
                  width="100%"
                  alignItems="center"
                  justifyContent="space-between"
                  marginBottom="1rem"
                  data-testid="collapsable"
                  onClick={toggleCollapse}
               >
                  <Text fontSize="md" color="gray.800" fontWeight="bold">
                     {props.title}
                  </Text>
                  <ChevronUpIcon
                     transform={`rotate(${rotate}deg)`}
                     transitionDuration=".3s"
                     boxSize={7}
                     color="brand.600"
                  />
               </Flex>
               <Collapse
                  in={isOpen}
                  animateOpacity={false}
                  unmountOnExit={true}
               >
                  {props.children}
               </Collapse>
            </Box>
         </Box>
      </>
   )
}
