import { Flex, Text, Skeleton } from '@chakra-ui/react'

interface LabelValueProps {
   label: string
   isLoading?: boolean
   values: Array<string>
}

export function LabelValue({ label, isLoading, values }: LabelValueProps) {
   return (
      <Flex direction="column" mb="0.8rem">
         <Text color="gray.900" fontWeight={700} fontSize="md">
            {label}
         </Text>
         {isLoading || !values ? (
            <Skeleton height="1.5rem" width="80%" data-testid="loading" />
         ) : (
            values?.map((value, key) => (
               <Text fontSize="lg" key={key}>
                  {value}
               </Text>
            ))
         )}
      </Flex>
   )
}
