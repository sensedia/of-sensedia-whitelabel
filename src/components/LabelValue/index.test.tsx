import { LabelValue } from '.'
import { render, screen } from '@testing-library/react'

describe('<LabelValue />', () => {
   it('should display loading component when loading is true', () => {
      render(<LabelValue isLoading values={['1', '2']} label="test" />)
      expect(screen.getByTestId('loading')).toBeTruthy()
   })

   it('should display label and value when loading is false', () => {
      render(<LabelValue values={['1', '2']} label="test" />)
      expect(screen.queryByTestId('loading')).toBeNull()
      expect(screen.getByText('1')).toBeInTheDocument()
      expect(screen.getByText('2')).toBeInTheDocument()
      expect(screen.getByText('test')).toBeInTheDocument()
   })
})
