import Link from 'next/link'
import { formatDate } from '@utils'
import { useTranslation } from 'react-i18next'
import { Card } from '@components'
import { Flex, Stack, Skeleton, Text } from '@chakra-ui/react'
import { shareStatusEnum } from '@constants'
import { ShareType } from '@models'

interface CardShareProps {
   id: string
   status: string
   date: string
   title: string
   type: ShareType
}

export function CardShare(props: CardShareProps) {
   const { t } = useTranslation()
   const date = formatDate(props.date)

   return (
      <Link
         href={{
            pathname: `/compartilhamentos/detalhes/${props.id}`,
            query: { 'shared-type': props.type },
         }}
         passHref
      >
         <Card cursor="pointer">
            <Flex align="center" justify="space-between" mb="0.5rem" size="lg">
               <Text
                  fontSize="md"
                  fontWeight={700}
                  color={
                     props.status === shareStatusEnum.ACTIVE
                        ? 'success'
                        : props.status === shareStatusEnum.PENDING
                        ? 'warning'
                        : 'gray.600'
                  }
               >
                  {t('shared.page.share.status')}
                  {t(`shared.component.cardShare.status.${props.status}`)}
               </Text>
               <Text fontSize="md" fontWeight={700} color="gray.600">
                  {date}
               </Text>
            </Flex>
            <Text fontSize="xl" fontWeight={700} color="gray.900">
               {props.title}
            </Text>
         </Card>
      </Link>
   )
}

export function CardShareSkeleton() {
   return (
      <Card>
         <Stack>
            <Flex mb="1rem" direction="row" justify="space-between">
               <Skeleton height="0.875rem" width="30%" />
               <Skeleton height="0.875rem" width="40%" />
            </Flex>
            <Skeleton height="1.25rem" width="40%" />
         </Stack>
      </Card>
   )
}
