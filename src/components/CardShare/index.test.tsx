import Faker from 'faker'
import { CardShare } from '.'
import { render, screen } from '@testing-library/react'
import { shareStatusEnum } from '@constants'

describe('<CardShare />', () => {
   const id = Faker.datatype.uuid()
   const title = Faker.name.title()
   const date = '2021-11-31T18:23:03.112Z'

   test('Should render correctly', () => {
      const status = shareStatusEnum.PENDING

      render(<CardShare id={id} title={title} status={status} date={date} />)

      expect(screen.getByText(title)).toBeTruthy()
   })

   test('Should render with pending label', () => {
      const status = shareStatusEnum.PENDING

      render(<CardShare id={id} title={title} status={status} date={date} />)

      expect(
         screen.queryAllByText(`shared.component.cardShare.status.${status}`),
      ).toBeTruthy()
   })

   test('Should render with active label', () => {
      const status = shareStatusEnum.ACTIVE

      render(<CardShare id={id} title={title} status={status} date={date} />)

      expect(
         screen.queryAllByText(`shared.component.cardShare.status.${status}`),
      ).toBeTruthy()
   })

   test('Should render with expired label', () => {
      const status = shareStatusEnum.EXPIRED

      render(<CardShare id={id} title={title} status={status} date={date} />)

      expect(
         screen.queryAllByText(`shared.component.cardShare.status.${status}`),
      ).toBeTruthy()
   })
})
