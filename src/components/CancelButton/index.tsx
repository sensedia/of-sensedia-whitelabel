import { useTranslation } from 'react-i18next'
import { CloseIcon } from '@chakra-ui/icons'
import { Flex, Link as LinkStyle, Text } from '@chakra-ui/react'
import React from 'react'

interface CancelButtonProps extends React.HtmlHTMLAttributes<HTMLElement> {}

export function CancelButton(props: CancelButtonProps) {
   const { t } = useTranslation()
   return (
      <LinkStyle
         {...props}
         color="brand.600"
         fontWeight="bold"
         alignItems="center"
         display="inline-flex"
         marginLeft="1.5625rem"
         marginTop="1.25rem"
         style={{ textDecoration: 'none' }}
      >
         <Flex align="center">
            <CloseIcon w={3} h={3} />
            <Text fontSize="sm" paddingLeft="0.625rem">
               {t('default.word.cancel')}
            </Text>
         </Flex>
      </LinkStyle>
   )
}
