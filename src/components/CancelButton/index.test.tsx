import React from 'react'
import { CancelButton } from '.'
import { fireEvent, render, screen } from '@testing-library/react'

describe('<CancelButton />', () => {
   it('render correctly', () => {
      const onClick = jest.fn()
      render(<CancelButton onClick={onClick} />)

      expect(screen.getByText('default.word.cancel')).toBeTruthy()
   })

   it('call onClick when click button', () => {
      const onClick = jest.fn()
      render(<CancelButton onClick={onClick} role="click-me" />)

      fireEvent.click(screen.getByRole('click-me'))

      expect(onClick).toBeCalledTimes(1)
   })
})
