import { server } from '../../mocks/server'
import {
   screen,
   waitForElementToBeRemoved,
   fireEvent,
   waitFor,
} from '@testing-library/react'
import { useRouter } from 'next/router'
import { renderWithClient } from '../../tests/utils'
import { awaitingAuthPayment } from '@mocks/data/payments/paymentConsent'
import { PaymentCheckout } from '.'

jest.mock('next/router', () => ({
   useRouter: jest.fn(),
}))
const push = jest.fn()
;(useRouter as jest.Mock).mockImplementation(() => ({
   push,
}))

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

describe('<PaymentCheckout />', () => {
   it('renders correctly with payment and account info', async () => {
      renderWithClient(
         <PaymentCheckout payment={awaitingAuthPayment} onCancelPath="/" />,
      )

      await waitForElementToBeRemoved(() =>
         document.querySelector('.chakra-skeleton'),
      )

      expect(screen.getByText(/r\$ 100,12/i)).toBeInTheDocument()
      expect(
         screen.getByText(/payment\.checkout\.available r\$ 8\.200,00/i),
      ).toBeInTheDocument()
   }),
      it('changes selected account', async () => {
         renderWithClient(
            <PaymentCheckout payment={awaitingAuthPayment} onCancelPath="/" />,
         )

         await waitForElementToBeRemoved(() =>
            document.querySelector('.chakra-skeleton'),
         )

         expect(
            screen.getByText(/payment\.checkout\.available r\$ 8\.200,00/i),
         ).toBeInTheDocument()

         fireEvent.click(
            screen.getByRole('button', {
               name: /change account/i,
            }),
         )

         expect(
            screen.getByText('confirm.payment.account.modal.title'),
         ).toBeInTheDocument()

         fireEvent.click(screen.getByText(/conta poupança/i))
         fireEvent.click(
            screen.getByRole('button', { name: 'default.word.continue' }),
         )

         expect(
            screen.getByText(/payment\.checkout\.available r\$ 500,00/i),
         ).toBeInTheDocument()
      }),
      it('redirects user when payment is confirmed', async () => {
         renderWithClient(
            <PaymentCheckout payment={awaitingAuthPayment} onCancelPath="/" />,
         )

         await waitFor(() => {
            expect(
               document.querySelector('.chakra-skeleton'),
            ).not.toBeInTheDocument()
         })

         fireEvent.click(
            screen.getByRole('button', {
               name: 'confirm.payment.page.confirmation.button',
            }),
         )

         expect(
            screen.getByText('confirm.payment.modal.title'),
         ).toBeInTheDocument()

         fireEvent.click(
            screen.getByRole('button', {
               name: 'confirm.payment.modal.confirmation.button',
            }),
         )

         await waitFor(() => {
            expect(
               screen.getByText('redirect.popup.message.checkout'),
            ).toBeInTheDocument()
         })
      })
})
