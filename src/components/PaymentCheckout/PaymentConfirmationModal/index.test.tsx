import { render, screen, fireEvent } from '@testing-library/react'
import { PaymentConfirmationModal } from '.'

const onClose = jest.fn()
const onConfirm = jest.fn()

describe('<PaymentConfirmationModal />', () => {
   it('renders', () => {
      render(
         <PaymentConfirmationModal
            isLoading={false}
            isOpen={true}
            onClose={onClose}
            onConfirm={onConfirm}
         />,
      )
      expect(
         screen.getByText('confirm.payment.modal.title'),
      ).toBeInTheDocument()
   })

   it('calls onConfirm function when confirmation button is clicked', () => {
      render(
         <PaymentConfirmationModal
            isLoading={false}
            isOpen={true}
            onClose={onClose}
            onConfirm={onConfirm}
         />,
      )

      fireEvent.click(
         screen.getByRole('button', {
            name: 'confirm.payment.modal.confirmation.button',
         }),
      )

      expect(onConfirm).toBeCalled()
   })

   it('calls onClose function when cancel button is clicked', () => {
      render(
         <PaymentConfirmationModal
            isLoading={false}
            isOpen={true}
            onClose={onClose}
            onConfirm={onConfirm}
         />,
      )

      fireEvent.click(
         screen.getByRole('button', {
            name: 'confirm.payment.modal.cancel.button',
         }),
      )

      expect(onClose).toBeCalled()
   })
})
