import { useTranslation, Trans } from 'react-i18next'
import {
   Button,
   Stack,
   Text,
   Modal,
   ModalOverlay,
   ModalContent,
   ModalHeader,
   ModalFooter,
   ModalBody,
   ModalCloseButton,
} from '@chakra-ui/react'

interface PaymentConfirmationModalProps {
   isLoading: boolean
   isOpen: boolean
   onClose: () => void
   onConfirm: () => void
}

export function PaymentConfirmationModal({
   isLoading,
   isOpen,
   onClose,
   onConfirm,
}: PaymentConfirmationModalProps) {
   const { t } = useTranslation()

   function handleOnConfirm() {
      onConfirm()
      onClose()
   }

   return (
      <Modal onClose={onClose} isOpen={isOpen} isCentered size="xs">
         <ModalOverlay />
         <ModalContent>
            <ModalHeader>
               <Text color="gray.900">{t('confirm.payment.modal.title')}</Text>
            </ModalHeader>
            <ModalCloseButton data-testid="header-close-button" />
            <ModalBody>
               <Text>
                  <Trans
                     defaults="confirm.payment.modal.body"
                     components={[<strong key="strong" />]}
                  />
               </Text>
            </ModalBody>
            <ModalFooter>
               <Stack direction="column" width="100%">
                  <Button isLoading={isLoading} onClick={handleOnConfirm}>
                     {t('confirm.payment.modal.confirmation.button')}
                  </Button>
                  <Button onClick={onClose} variant="outline">
                     {t('confirm.payment.modal.cancel.button')}
                  </Button>
               </Stack>
            </ModalFooter>
         </ModalContent>
      </Modal>
   )
}
