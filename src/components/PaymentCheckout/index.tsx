import { useState } from 'react'
import { useRouter } from 'next/router'
import { useTranslation } from 'react-i18next'
import { useMutation, useQuery } from 'react-query'
import {
   Box,
   Button,
   Text,
   Flex,
   Divider,
   Skeleton,
   Stack,
   useToast,
} from '@chakra-ui/react'
import { AccountSelectionModal } from './AccountSelectionModal'
import { PaymentConfirmationModal } from './PaymentConfirmationModal'
import { LabelValue, SwapIcon, RedirecionamentoPopup } from '@components'
import { DebtorAccount, PaymentConsent } from '@models'
import { Accounts, Payments } from '@services'
import { formatDate, formatCurrency, formatCNPJ, formatCPF } from '@utils'

interface PaymentCheckoutProps {
   payment: PaymentConsent
   onCancelPath: string
}

export function PaymentCheckout({
   payment,
   onCancelPath,
}: PaymentCheckoutProps) {
   const { push } = useRouter()
   const toast = useToast()
   const errorToast = (title: string) =>
      toast({
         title,
         status: 'error',
         isClosable: true,
      })
   const { t } = useTranslation()

   const [bank, setBank] = useState({
      img: '',
      name: payment.organisationName,
      link: '',
   })
   const [redirect, setRedirect] = useState<boolean>(false)
   const [isSelectionModalOpen, setIsSelectionModalOpen] = useState(false)
   const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false)
   const [selectedAccountId, setSelectedAccountId] = useState('')

   const { data: accountsData } = useQuery('accounts', Accounts.get, {
      onSuccess: (data) => {
         setSelectedAccountId(data.data[0].accountId)
      },
      onError: () => {
         errorToast(t('confirm.payment.page.accounts.error'))
      },
      refetchOnWindowFocus: false,
   })

   const selectedAccount = accountsData?.data.find(
      (account) => account.accountId === selectedAccountId,
   )

   const confirmationMutation = useMutation(
      (account: DebtorAccount) =>
         Payments.approve(payment.consentId, {
            debtorAccount: account,
         }),
      {
         onError: () => {
            errorToast(t('confirm.payment.page.confirmation.error'))
         },
         onSuccess: (data) => {
            setBank((currentBank) => ({
               ...currentBank,
               link: data.data.redirectUri,
            }))
            setRedirect(true)
         },
      },
   )

   function handlePaymentConfirmation() {
      if (!selectedAccount) {
         errorToast(t('confirm.payment.page.selected.account.error'))
         return
      }

      const { amount, accountId, ...selectedAccountPayload } = selectedAccount
      confirmationMutation.mutate(selectedAccountPayload)
   }

   return (
      <>
         <Box bg="gray.200" p={4} borderRadius=".5rem" mt={6}>
            <Text fontSize="lg" color="gray.800">
               {t('payment.checkout.redirect.message')}
            </Text>
            <Text fontSize="2xl" fontWeight={700} color="gray.900">
               {payment.organisationName}
            </Text>
         </Box>
         <Text fontSize="lg" color="gray.800" fontWeight="bold" my="1rem">
            {t('payment.checkout.summary.title')}
         </Text>

         <Box bg="white" borderRadius=".5rem" shadow="xl" p={4}>
            <Text color="gray.900" fontWeight={700} fontSize="md">
               {t('payment.checkout.amount')}
            </Text>
            <Text fontSize="2xl" fontWeight={700} color="gray.900">
               {formatCurrency(payment.payment.amount)}
            </Text>
            {selectedAccount ? (
               <Text color="gray.900" fontWeight={500} fontSize="sm">
                  {t('payment.checkout.available')}{' '}
                  {formatCurrency(selectedAccount.amount)}
               </Text>
            ) : (
               <Skeleton height="1rem" startColor="gray.900" width="40%" />
            )}
            <Divider my={4} />
            <LabelValue
               label={t('payment.checkout.receiver')}
               values={[
                  `${
                     payment.creditor.personType === 'PESSOA_NATURAL'
                        ? `CPF ${formatCPF(payment.creditor.cpfCnpj)}`
                        : `CNPJ ${formatCNPJ(payment.creditor.cpfCnpj)}`
                  }`,
                  payment.creditor.name,
               ]}
            />
            <LabelValue
               label={t('payment.checkout.date')}
               values={[formatDate(payment.creationDateTime)]}
            />
            <Flex align="center" justify="space-between">
               <Box mb="0.8rem">
                  <Text color="gray.900" fontWeight={700} fontSize="md">
                     {t('payment.checkout.source')}
                  </Text>
                  {selectedAccount ? (
                     <Text color="brand.600" fontSize="lg">
                        {selectedAccount.accountType === 'CACC' ? 'CC' : 'CI'}{' '}
                        {selectedAccount.number}
                     </Text>
                  ) : (
                     <Skeleton height="1.5rem" width="40%" />
                  )}
               </Box>
               {selectedAccount && (
                  <Button
                     variant="ghost"
                     aria-label="change account"
                     onClick={() => setIsSelectionModalOpen(true)}
                  >
                     <SwapIcon
                        onClick={() => setIsSelectionModalOpen(true)}
                        color="brand.600"
                        boxSize={6}
                        role="button"
                     />
                  </Button>
               )}
            </Flex>
            <LabelValue
               label={t('payment.checkout.type')}
               values={['Pix - Open Banking']}
            />
         </Box>
         <Stack spacing={3} mt={6}>
            <Button
               bg="brand.500"
               variant="solid"
               onClick={() => setIsConfirmationModalOpen(true)}
            >
               {t('confirm.payment.page.confirmation.button')}
            </Button>
            <Button
               color="brand.500"
               variant="ghost"
               isFullWidth
               onClick={() => push(onCancelPath)}
            >
               {t('default.word.cancel')}
            </Button>
         </Stack>
         <PaymentConfirmationModal
            isLoading={confirmationMutation.isLoading}
            isOpen={isConfirmationModalOpen}
            onClose={() => setIsConfirmationModalOpen(false)}
            onConfirm={handlePaymentConfirmation}
         />
         <RedirecionamentoPopup
            bank={bank}
            isOpen={redirect}
            onClose={() => setRedirect(false)}
            message={t('redirect.popup.message.checkout')}
            redirectMessage={t('redirect.popup.redirect.message.checkout')}
         />
         {accountsData && selectedAccount && (
            <AccountSelectionModal
               isOpen={isSelectionModalOpen}
               onClose={() => setIsSelectionModalOpen(false)}
               accounts={accountsData?.data}
               onConfirmChange={setSelectedAccountId}
               selectedAccount={selectedAccount}
               selectedAccountId={selectedAccountId}
            />
         )}
      </>
   )
}
