import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import {
   Text,
   Stack,
   Flex,
   ModalContent,
   Modal,
   Box,
   Radio,
   RadioGroup,
} from '@chakra-ui/react'
import { Button, Logo } from '@components'
import { ChevronLeftIcon } from '@chakra-ui/icons'
import { AccountType, BankAccount } from '@models'
import { formatCurrency } from '@utils'

type AccountSelectionModalProps = {
   isOpen: boolean
   onClose: () => void
   accounts: BankAccount[]
   onConfirmChange: (id: string) => void
   selectedAccountId: string
   selectedAccount: BankAccount
}

export function AccountSelectionModal({
   isOpen,
   onClose,
   accounts,
   selectedAccountId,
   onConfirmChange,
   selectedAccount,
}: AccountSelectionModalProps) {
   const [localSelected, setLocalSelected] = useState(selectedAccountId)
   const { t } = useTranslation()

   function handleConfirmChange() {
      onConfirmChange(localSelected)
      onClose()
   }

   function handleCancelChange() {
      setLocalSelected(selectedAccountId)
      onClose()
   }

   function getAccountLabel(accountType: AccountType) {
      const accountTypes = {
         CACC: 'Conta Corrente',
         SLRY: 'Conta Salário',
         SVGS: 'Conta Poupança',
         TRAN: 'Conta de agamento pré-paga',
      }

      return accountTypes[accountType]
   }

   return (
      <Modal isOpen={isOpen} onClose={onClose} size="full">
         <ModalContent my="0rem" bg="gray.50">
            <Flex
               width="100%"
               margin="0 auto"
               pb="0.7rem"
               bg="brand.600"
               justifyContent="center"
               alignItems="center"
               height="4.7rem"
            >
               <Logo />
            </Flex>
            <Box width="100%" maxWidth="40rem" margin="0 auto" p={6} pt={4}>
               <Flex alignItems="center" justify="center" h={10}>
                  <ChevronLeftIcon
                     color="brand.600"
                     w={8}
                     h={8}
                     onClick={handleCancelChange}
                     data-testid="button-back"
                     cursor="pointer"
                     mr="auto"
                  />
                  <Text
                     fontSize="lg"
                     fontWeight={700}
                     color="gray.900"
                     mr="auto"
                     ml={-8}
                  >
                     {t('confirm.payment.account.modal.title')}
                  </Text>
               </Flex>
               <Box bg="white" borderRadius=".5rem" shadow="xl" p={4} mt={3}>
                  <RadioGroup onChange={setLocalSelected} value={localSelected}>
                     <Text fontWeight={700} color="gray.900" mb={3}>
                        {t('account.selection.modal.agency.word')}{' '}
                        {selectedAccount.issuer} | {selectedAccount.number}
                     </Text>
                     <Stack spacing={4}>
                        {accounts.map((account) => (
                           <Radio
                              size="lg"
                              colorScheme="brand"
                              value={account.accountId}
                              spacing={3}
                              key={account.accountId}
                           >
                              <Box>
                                 <Text
                                    fontWeight={700}
                                    color="brand.600"
                                    mb={-1}
                                 >
                                    {getAccountLabel(account.accountType)}
                                 </Text>
                                 <Text fontSize="sm">
                                    {t('account.selection.modal.balance')}{' '}
                                    {formatCurrency(account.amount)}
                                 </Text>
                              </Box>
                           </Radio>
                        ))}
                     </Stack>
                  </RadioGroup>
               </Box>

               <Stack spacing={3} mt={6}>
                  <Button
                     bg="brand.500"
                     variant="solid"
                     onClick={handleConfirmChange}
                     data-testid="button-continue"
                     isLoading={false}
                  >
                     {t('default.word.continue')}
                  </Button>
                  <Button
                     color="brand.500"
                     variant="ghost"
                     isFullWidth
                     onClick={handleCancelChange}
                     data-testid="button-cancel"
                  >
                     {t('default.word.cancel')}
                  </Button>
               </Stack>
            </Box>
         </ModalContent>
      </Modal>
   )
}
