import { accounts } from '@mocks/data/accounts/accounts'
import { render, screen, fireEvent } from '@testing-library/react'
import { AccountSelectionModal } from '.'

const onClose = jest.fn()
const onConfirm = jest.fn()

describe('<AccountSelectionModal />', () => {
   it('renders', () => {
      render(
         <AccountSelectionModal
            isOpen={true}
            onConfirmChange={onConfirm}
            onClose={onClose}
            accounts={accounts}
            selectedAccount={accounts[0]}
            selectedAccountId={accounts[0].accountId}
         />,
      )
      expect(
         screen.getByText('confirm.payment.account.modal.title'),
      ).toBeInTheDocument()
      expect(screen.getByText(/conta corrente/i)).toBeInTheDocument()
      expect(screen.getByText(/conta poupança/i)).toBeInTheDocument()
   })

   it('calls onConfirmChange with selected account id when confirm button is clicked', () => {
      render(
         <AccountSelectionModal
            isOpen={true}
            onConfirmChange={onConfirm}
            onClose={onClose}
            accounts={accounts}
            selectedAccount={accounts[0]}
            selectedAccountId={accounts[0].accountId}
         />,
      )
      fireEvent.click(screen.getByText(/conta poupança/i))
      fireEvent.click(
         screen.getByRole('button', { name: 'default.word.continue' }),
      )
      expect(onConfirm).toBeCalled()
      expect(onConfirm).toHaveBeenCalledWith(accounts[1].accountId)
   })
})
