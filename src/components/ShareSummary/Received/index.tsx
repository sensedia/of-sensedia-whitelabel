import { useTranslation } from 'react-i18next'
import { Text, Stack } from '@chakra-ui/react'
import { Card, LabelValue } from '@components'
import { formatDate, formatDateWithHour } from '@utils'

interface Props {
   isLoading?: boolean
   document?: {
      rel: string
      identification: string
   }
   name?: string
   finality?: string
   confirmationDate?: string
   deadline?: number
   expirationDateTime?: string
}

export function ReceivedShare(props: Props) {
   const { t } = useTranslation()

   function getTerm() {
      const startDate = new Date(props?.confirmationDate as '')
      const endDate = new Date(props?.deadline as number)

      return endDate.getMonth() - startDate.getMonth() + 1
   }

   return (
      <Stack>
         <Text fontSize="xl" fontWeight={700} mb="0.75rem">
            {t('shared.page.share.details.abstract.title')}
         </Text>
         <Card shadow="lg">
            <LabelValue
               label={t('shared.page.share.details.abstract.identification')}
               isLoading={props.isLoading}
               values={[
                  `${props.document?.rel} ${props.document?.identification}`,
                  `${props.name}`,
               ]}
            />
            <LabelValue
               label={t('shared.page.share.details.abstract.goal')}
               isLoading={props.isLoading}
               values={[props.finality as '']}
            />
            <LabelValue
               label={t('shared.page.share.details.abstract.confirmationDate')}
               isLoading={props.isLoading}
               values={[
                  props.confirmationDate
                     ? formatDateWithHour(props.confirmationDate)
                     : '',
               ]}
            />
            <LabelValue
               label={t('shared.page.share.details.abstract.sharingTerm')}
               isLoading={props.isLoading}
               values={[
                  `${getTerm()} ${t(
                     'shared.page.share.details.abstract.month',
                  )} | ${
                     props.expirationDateTime
                        ? formatDate(props.expirationDateTime)
                        : ''
                  }`,
               ]}
            />
         </Card>
      </Stack>
   )
}
