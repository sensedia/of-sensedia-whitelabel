import { ReceivedShare } from '.'
import { render, screen } from '@testing-library/react'

const mockProps = {
   document: {
      rel: 'CPF',
      identification: '067.123.321-23',
   },
   name: 'Fulaninho',
   transmitter: 'Crediconta',
   finality: 'Lorem ipsum',
   confirmationDate: '2021-05-31T18:23:03.112Z',
   expirationDateTime: '2021-11-31T18:23:03.112Z',
   isLoading: false,
}

describe('<AbstractRequest />', () => {
   it('Should render correctly', () => {
      render(<ReceivedShare {...mockProps} />)
      expect(screen.getAllByText(mockProps.finality)).toBeTruthy()
   })
})
