import { useTranslation } from 'react-i18next'
import { Text, Stack } from '@chakra-ui/react'
import { Card, LabelValue } from '@components'
import { formatDateWithHour } from '@utils'

interface Props {
   isLoading?: boolean
   identification?: {
      cpf: string
      name: string
   }
   finality?: string
   confirmationDate?: string
   deadline?: number
   expirationDateTime?: string
}

export function TransmittedShare(props: Props) {
   const { t } = useTranslation()

   function validateDate(date?: string) {
      return date ? formatDateWithHour(date) : ''
   }

   return (
      <Stack>
         <Text fontSize="xl" fontWeight={700} mb=".75rem">
            {t('shared.page.share.details.abstract.title')}
         </Text>
         <Card shadow="lg">
            <LabelValue
               label={t('shared.page.share.details.abstract.identification')}
               isLoading={props.isLoading}
               values={[
                  `CPF: ${props.identification?.cpf}`,
                  `${props.identification?.name}`,
               ]}
            />
            <LabelValue
               label={t('shared.page.share.details.abstract.goal')}
               isLoading={props.isLoading}
               values={[props.finality as '']}
            />
            <LabelValue
               label={t('shared.page.share.details.abstract.confirmationDate')}
               isLoading={props.isLoading}
               values={[validateDate(props.confirmationDate)]}
            />
            <LabelValue
               label={t('shared.page.share.details.abstract.sharingTerm')}
               isLoading={props.isLoading}
               values={[validateDate(props.expirationDateTime)]}
            />
         </Card>
      </Stack>
   )
}
