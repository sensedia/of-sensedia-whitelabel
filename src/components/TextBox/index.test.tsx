import React from 'react'
import { render } from '@testing-library/react'
import { TextBox } from './index'

describe('<TextBox />', () => {
   it('render correctly with text inside', () => {
      const { getByText } = render(<TextBox content="Testing"></TextBox>)

      expect(getByText('Testing')).toBeTruthy()
   })
})
