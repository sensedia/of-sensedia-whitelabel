import { Flex, Text } from '@chakra-ui/react'
import React from 'react'

type TextBoxProp = {
   content: string
}

export function TextBox(props: TextBoxProp) {
   return (
      <>
         <Flex
            borderRadius="0.5rem"
            cursor="pointer"
            marginBottom="0.75rem"
            height="3.5rem"
            boxShadow="lg"
            bg="white"
         >
            <Text
               fontWeight="700"
               margin="auto 0"
               padding="0 1rem"
               fontSize="md"
               color="gray.900"
            >
               {props.content}
            </Text>
         </Flex>
      </>
   )
}
