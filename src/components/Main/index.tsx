import { Stack, StackProps } from '@chakra-ui/react'

export function Main(props: StackProps) {
   return (
      <Stack
         spacing="1.5rem"
         width="100%"
         maxWidth="40rem"
         backgroundColor="white"
         height="calc(100% - 4.75rem)"
         {...props}
      />
   )
}
