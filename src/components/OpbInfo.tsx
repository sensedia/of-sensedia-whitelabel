import React from 'react'
import Link from 'next/link'
import { Text, Tabs, TabList, Tab, TabPanels, TabPanel } from '@chakra-ui/react'
import { useSwipeable } from 'react-swipeable'
import { useTranslation } from 'react-i18next'
import { LogoObp } from './LogoOpb'
import { HandshakeIcon, PeopleIcon } from '@components'

export function OpbInfo() {
   const handlers = useSwipeable({
      onSwipedLeft: (eventData) => handleSliderChange(eventData),
      onSwipedRight: (eventData) => handleSliderChange(eventData),
   })

   const [tabIndex, setTabIndex] = React.useState(0)

   const tabsMaxIndex = 1

   const handleSliderChange = (eventData: any) => {
      let tempIndex = tabIndex
      if (eventData.dir === 'Left') {
         tempIndex = tabIndex + 1
      } else {
         tempIndex = tabIndex - 1
      }
      if (tempIndex >= 0 && tempIndex <= tabsMaxIndex) {
         setTabIndex(tempIndex)
      }
   }

   const handleTabsChange = (index: number) => {
      setTabIndex(index)
   }

   const { t } = useTranslation()

   return (
      <>
         <Tabs
            {...handlers}
            index={tabIndex}
            onChange={handleTabsChange}
            variant="unstyled"
            align="center"
         >
            <TabPanels>
               <TabPanel padding="0">
                  <LogoObp></LogoObp>
                  <HandshakeIcon
                     color="brand.600"
                     boxSize={100}
                  ></HandshakeIcon>
                  <Text color="gray.900" fontSize="sm" marginTop="1.5rem">
                     {t('home.popup.opbinfo.tab1.body')}
                  </Text>
               </TabPanel>
               <TabPanel padding="0">
                  <LogoObp></LogoObp>
                  <PeopleIcon color="brand.600" boxSize={100}></PeopleIcon>
                  <Text color="gray.900" fontSize="sm" margin="2.5rem 0 1rem 0">
                     {t('home.popup.opbinfo.tab2.body')}
                  </Text>
                  <Link href="https://openbankingbrasil.org.br">
                     <a>
                        <Text
                           textDecoration="underline"
                           fontWeight="700"
                           fontSize="md"
                           color="purple"
                           textAlign="center"
                        >
                           {t('home.popup.opbinfo.tab2.link')}
                        </Text>
                     </a>
                  </Link>
               </TabPanel>
            </TabPanels>

            <TabList border="0" marginTop="2rem">
               <Tab
                  padding="0"
                  width="0.5rem"
                  height="0.5rem"
                  borderRadius="0.5rem"
                  marginRight="0.5rem"
                  bg="brand.300"
                  _selected={{ bg: 'brand.600', width: '2.13rem' }}
               ></Tab>
               <Tab
                  padding="0"
                  width="0.5rem"
                  height="0.5rem"
                  borderRadius="0.5rem"
                  bg="brand.300"
                  _selected={{ bg: 'brand.600', width: '2.13rem' }}
               ></Tab>
            </TabList>
         </Tabs>
      </>
   )
}
