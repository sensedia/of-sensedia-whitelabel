import { Box, Flex, Text } from '@chakra-ui/layout'
import { ExchangeIcon } from '@components/Icons/Exchange'

export default function CardSelectInstitution() {
   return (
      <Box
         bg="white"
         borderRadius="0.5rem"
         cursor="pointer"
         mb="0.8rem"
         shadow="md"
         width="100%"
      >
         <Flex
            align="center"
            justify="space-between"
            padding="0.7rem"
            borderBottom="0.0625rem solid #eee"
         >
            <Text color="gray.900" fontWeight={700}>
               Instituição Selecionada
            </Text>
         </Flex>
         <Flex
            justifyContent="space-between"
            alignContent="center"
            padding="0.7rem"
            py="1.8rem"
         >
            <Text color="gray.900" fontWeight="700" fontSize="xl">
               Wiscredi
            </Text>
            <ExchangeIcon fontSize="3xl" />
         </Flex>
      </Box>
   )
}
