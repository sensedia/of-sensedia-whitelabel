import React from 'react'
import { CardInfo } from '.'
import { render } from '@testing-library/react'

describe('<CardInfo />', () => {
   it('render correctly with text inside', () => {
      const { getByText } = render(
         <CardInfo title="Title" body="Body" href="/testing" />,
      )

      expect(getByText('Title')).toBeTruthy()
      expect(getByText('Body')).toBeTruthy()
   })
})
