import { CardToggle } from '.'
import { fireEvent, render, screen } from '@testing-library/react'

describe('<CardToggle />', () => {
   it('Should render correctly', () => {
      render(<CardToggle title="Lorem" />)
      expect(screen.queryByText('Lorem')).toBeTruthy()
   })

   it('Should expand children', () => {
      const text = 'Inside Card Text'
      render(
         <CardToggle title="Lorem">
            <p>{text}</p>
         </CardToggle>,
      )
      fireEvent.click(screen.getByTestId('button'))
      expect(screen.getByText(text)).toBeTruthy()
   })

   it('Change button text on collapse', () => {
      render(<CardToggle title="Lorem"></CardToggle>)

      fireEvent.click(screen.getByTestId('button'))

      expect(screen.getByText('card.component.collapse')).toBeTruthy()

      fireEvent.click(screen.getByTestId('button'))

      expect(screen.getByText('card.component.expand')).toBeTruthy()
   })
})
