import { ReactNode } from 'react'
import { Card, ToggleSize } from '@components'
import { Collapse, Flex, Text, useDisclosure } from '@chakra-ui/react'

interface CardToggleProps {
   children?: ReactNode
   title: string
}

export function CardToggle(props: CardToggleProps) {
   const { isOpen, onToggle } = useDisclosure()

   return (
      <Card padding={0} shadow="lg">
         <Flex
            borderBottomWidth="0.0625rem"
            borderColor="gray.200"
            direction="column"
            padding="1.1rem"
         >
            <Text color="gray.900" fontSize="xl" fontWeight={700}>
               {props.title}
            </Text>
            <Collapse in={isOpen} animateOpacity={false} unmountOnExit={true}>
               {props.children}
            </Collapse>
         </Flex>
         <ToggleSize data-testid="button" onClick={onToggle} show={isOpen} />
      </Card>
   )
}
