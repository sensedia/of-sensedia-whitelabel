import Link from 'next/link'
import { useTranslation } from 'react-i18next'
import { ChevronLeftIcon } from '@chakra-ui/icons'
import { Flex, Link as LinkStyle, Text } from '@chakra-ui/react'

interface BackButtonProps {
   path: string
}

export function BackButton({ path }: BackButtonProps) {
   const { t } = useTranslation()
   return (
      <Link href={path} passHref>
         <LinkStyle
            color="brand.600"
            fontWeight="bold"
            alignItems="center"
            display="inline-flex"
         >
            <Flex align="center">
               <ChevronLeftIcon w={8} h={8} />
               <Text fontSize="sm">{t('default.word.previous')}</Text>
            </Flex>
         </LinkStyle>
      </Link>
   )
}
