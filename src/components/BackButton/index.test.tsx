import React from 'react'
import { BackButton } from '.'
import { render } from '@testing-library/react'

describe('<BackButton />', () => {
   it('render correctly', () => {
      const { getByText } = render(<BackButton path="" />)

      expect(getByText('default.word.previous')).toBeTruthy()
   })
})
