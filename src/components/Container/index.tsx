import { Flex, FlexProps } from '@chakra-ui/react'

export function Container(props: FlexProps) {
   return (
      <Flex
         alignItems="center"
         justifyContent="center"
         flexDirection="column"
         bgColor="brand.600"
         overflow="hidden"
         {...props}
      />
   )
}
