import { ReactNode } from 'react'
import { Box, BoxProps } from '@chakra-ui/react'

interface CardProps extends BoxProps {
   children: ReactNode
   cursor?: 'grab' | 'pointer'
}

export function Card(props: CardProps) {
   return (
      <Box
         bg="white"
         borderRadius="0.5rem"
         cursor={props.cursor ? props.cursor : 'grab'}
         mb=".8rem"
         padding="1.1rem"
         shadow="md"
         width="100%"
         {...props}
      >
         {props.children}
      </Box>
   )
}
