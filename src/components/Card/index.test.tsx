import React from 'react'
import { Card } from '.'
import { Text } from '@chakra-ui/react'
import { render } from '@testing-library/react'

describe('<Card />', () => {
   it('render correctly with text inside', () => {
      const { getByText } = render(
         <Card>
            <Text>Testing</Text>
         </Card>,
      )

      expect(getByText('Testing')).toBeTruthy()
   })
})
