import React from 'react'
import { ModalFinishShare } from '.'
import { fireEvent, screen, render } from '@testing-library/react'

const onClose = jest.fn()
const onConfirm = jest.fn()
describe('<ModalComponent />', () => {
   it('renders correctly', () => {
      render(
         <ModalFinishShare
            isOpen={true}
            onClose={onClose}
            onConfirm={onConfirm}
         />,
      )
      expect(
         screen.queryByText(
            'shared.page.share.details.actions.finish.modal.title',
         ),
      ).toBeTruthy()
   })

   it('Should call onClose on trigger action', () => {
      render(
         <ModalFinishShare
            isOpen={true}
            onClose={onClose}
            onConfirm={onConfirm}
         />,
      )

      fireEvent.click(screen.getByText('default.word.cancel'))
      expect(onClose).toBeCalled()
   })

   it('Should call onClose on trigger action in header close button', () => {
      render(
         <ModalFinishShare
            isOpen={true}
            onClose={onClose}
            onConfirm={onConfirm}
         />,
      )

      fireEvent.click(screen.getByTestId('header-close-button'))
      expect(onClose).toBeCalled()
   })

   it('Should call onConfirm on trigger action', () => {
      render(
         <ModalFinishShare
            isOpen={true}
            onClose={onClose}
            onConfirm={onConfirm}
         />,
      )

      fireEvent.click(
         screen.getByText(
            'shared.page.share.details.actions.finish.modal.accept',
         ),
      )
      expect(onConfirm).toBeCalled()
   })
})
