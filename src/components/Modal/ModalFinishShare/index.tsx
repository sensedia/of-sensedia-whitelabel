import { useTranslation, Trans } from 'react-i18next'
import {
   chakra,
   Button,
   Stack,
   Text,
   Modal,
   ModalOverlay,
   ModalContent,
   ModalHeader,
   ModalFooter,
   ModalBody,
   ModalCloseButton,
   Divider,
} from '@chakra-ui/react'

interface ModalFinishShareProps {
   isLoading: boolean
   isOpen: boolean
   onClose: () => void
   onConfirm: () => void
}

export function ModalFinishShare({
   isLoading,
   isOpen,
   onClose,
   onConfirm,
}: ModalFinishShareProps) {
   const { t } = useTranslation()
   return (
      <Modal onClose={onClose} isOpen={isOpen} isCentered size="xs">
         <ModalOverlay />
         <ModalContent>
            <ModalHeader>
               <Text color="gray.900">
                  {t('shared.page.share.details.actions.finish.modal.title')}
               </Text>
               <Divider
                  position="absolute"
                  marginTop="0.75rem"
                  borderColor="gray.500"
                  left="0"
               />
            </ModalHeader>
            <ModalCloseButton
               color="brand.500"
               data-testid="header-close-button"
            />
            <ModalBody>
               <Text color="gray.900" marginBottom="1.5rem">
                  <Trans
                     defaults="shared.page.share.details.actions.finish.modal.description"
                     components={[
                        <strong key="strong" />,
                        <chakra.span color="danger" key="color" />,
                     ]}
                  />
               </Text>
               <Text>
                  <Text color="gray.900" fontWeight={700}>
                     {t(
                        'shared.page.share.details.actions.finish.modal.confirmMessage',
                     )}
                  </Text>
               </Text>
            </ModalBody>
            <ModalFooter>
               <Stack direction="column" width="100%">
                  <Button
                     isFullWidth
                     isLoading={isLoading}
                     onClick={onConfirm}
                     bg="danger"
                     _active={{ backgroundColor: 'dangerHover' }}
                     _hover={{ backgroundColor: 'dangerHover' }}
                  >
                     {t(
                        'shared.page.share.details.actions.finish.modal.accept',
                     )}
                  </Button>
                  <Button isFullWidth onClick={onClose} variant="outline">
                     {t('default.word.cancel')}
                  </Button>
               </Stack>
            </ModalFooter>
         </ModalContent>
      </Modal>
   )
}
