import {
   Text,
   useDisclosure,
   Modal,
   ModalOverlay,
   ModalContent,
   ModalHeader,
   ModalBody,
   ModalCloseButton,
   Divider,
   ModalFooter,
   Box,
   Button,
   Flex,
} from '@chakra-ui/react'
import React, { ReactNode } from 'react'
import theme from 'styles/theme'

type FooterButton = {
   caption: string
   onClick?: () => void
   close?: boolean
   color?: string
   variant?: string
}

type ModalProps = {
   activator: ReactNode
   header?: ReactNode
   footerButtons?: FooterButton[]
   footer?: ReactNode
   children: ReactNode
   padding?: string
}

export function ModalComponent(props: ModalProps) {
   const { isOpen, onOpen, onClose } = useDisclosure()

   return (
      <>
         <Box onClick={onOpen}>{props.activator}</Box>

         <Modal scrollBehavior="inside" isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />

            <ModalContent margin="11vh 1.5rem" height="auto" maxHeight="80vh">
               <ModalHeader display={props.header ? 'block' : 'none'}>
                  <Text color="gray.900">{props.header}</Text>
                  <Divider
                     position="absolute"
                     marginTop="0.75rem"
                     borderColor="gray.500"
                     left="0"
                  />
               </ModalHeader>
               <ModalCloseButton
                  display={props.header ? 'block' : 'none'}
                  paddingTop="0.2rem"
                  fontWeight="bold"
                  color="brand.600"
                  data-testid="header-close-button"
               />
               <ModalBody
                  css={{
                     '&::-webkit-scrollbar': {
                        width: '0.25rem',
                     },
                     '&::-webkit-scrollbar-track': {
                        width: '0.375rem',
                     },
                     '&::-webkit-scrollbar-thumb': {
                        background: `${theme.colors.brand[600]}`,
                        borderRadius: '1.5rem',
                     },
                  }}
                  overflow="scroll"
                  padding={!props.padding ? '1rem' : props.padding}
                  {...props}
               >
                  {props.children}
               </ModalBody>

               <ModalFooter padding="0">
                  {props.footer}
                  <Flex
                     padding={!props.padding ? '1rem' : props.padding}
                     display={props.footerButtons ? 'flex' : 'none'}
                     justifyContent="space-between"
                     width="100%"
                  >
                     {props.footerButtons?.map(
                        (button: FooterButton, i: number) => {
                           return (
                              <Button
                                 key={i}
                                 variant={button.variant}
                                 width={
                                    props.footerButtons?.length === 1
                                       ? '100%'
                                       : 'auto'
                                 }
                                 onClick={
                                    button.close ? onClose : button.onClick
                                 }
                                 size="lg"
                                 backgroundColor={
                                    button.color ? button.color : 'brand.600'
                                 }
                                 _active={
                                    button.color
                                       ? { backgroundColor: button.color }
                                       : { backgroundColor: 'brand.600' }
                                 }
                                 _hover={
                                    button.color
                                       ? { backgroundColor: button.color }
                                       : { backgroundColor: 'brand.600' }
                                 }
                              >
                                 {button.caption}
                              </Button>
                           )
                        },
                     )}
                  </Flex>
               </ModalFooter>
            </ModalContent>
         </Modal>
      </>
   )
}
