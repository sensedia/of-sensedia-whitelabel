import React from 'react'
import { fireEvent, screen, render } from '@testing-library/react'
import { ModalComponent } from '.'
import { Text } from '@chakra-ui/layout'

describe('<ModalComponent />', () => {
   it('renders without header and footer', () => {
      const activator = <Text>ShowModal</Text>
      const { getByText } = render(
         <ModalComponent activator={activator}>Content</ModalComponent>,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Content')).toBeTruthy()
   })

   it('renders with header', () => {
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalComponent header="Header" activator={activator}>
            Content
         </ModalComponent>,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Content')).toBeTruthy()
      expect(getByText('Header')).toBeTruthy()
   })

   it('renders with one footer button', () => {
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalComponent
            footerButtons={[
               {
                  caption: 'Button1',
               },
            ]}
            activator={activator}
         >
            Content
         </ModalComponent>,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Button1')).toBeTruthy()
      expect(getByText('Content')).toBeTruthy()
   })

   it('renders with two footer buttons', () => {
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalComponent
            footerButtons={[
               {
                  caption: 'Button1',
               },
               {
                  caption: 'Button2',
               },
            ]}
            activator={activator}
         >
            Content
         </ModalComponent>,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Button1')).toBeTruthy()
      expect(getByText('Button2')).toBeTruthy()
      expect(getByText('Content')).toBeTruthy()
   })

   it('fires event to close modal with custom button', () => {
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalComponent
            footerButtons={[
               {
                  caption: 'Button1',
                  close: true,
               },
            ]}
            activator={activator}
         >
            Content
         </ModalComponent>,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Button1')).toBeTruthy()
      expect(getByText('Content')).toBeTruthy()

      fireEvent.click(getByText('Button1'))

      expect(getByText('Button1')).not.toBeVisible()
      expect(getByText('Content')).not.toBeVisible()
   })

   it('fires event to close modal with header button', () => {
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalComponent header="Header" activator={activator}>
            Content
         </ModalComponent>,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('Content')).toBeTruthy()

      fireEvent.click(screen.getByTestId('header-close-button'))

      expect(getByText('ShowModal')).toBeVisible()
   })

   it('fires custom event with custom button', () => {
      const onClick = jest.fn()
      const activator = <Text>ShowModal</Text>

      const { getByText } = render(
         <ModalComponent
            footerButtons={[
               {
                  caption: 'CustomEventButton',
                  close: false,
                  onClick: onClick,
               },
            ]}
            activator={activator}
         >
            Content
         </ModalComponent>,
      )

      fireEvent.click(getByText('ShowModal'))

      expect(getByText('CustomEventButton')).toBeTruthy()
      expect(getByText('Content')).toBeTruthy()

      fireEvent.click(getByText('CustomEventButton'))

      expect(onClick).toBeCalled()
   })
})
