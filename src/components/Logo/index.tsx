import { Box } from '@chakra-ui/react'
import { LogoIcon } from '@components'
import styled from '@emotion/styled'

const LogoWrapper = styled.div`
   width: 9.375rem;
   height: 1.625rem;
`

export function Logo() {
   return (
      <Box p={6} height="3.875rem" alignItems="center" justifyContent="center">
         <LogoWrapper>
            <LogoIcon />
         </LogoWrapper>
      </Box>
   )
}
