import React from 'react'
import ReactDOM from 'react-dom'
import { DotsIcon } from '.'

describe('<DotsIcon />', () => {
   it('renders without crashing', () => {
      const div = document.createElement('div')
      ReactDOM.render(<DotsIcon />, div)
      ReactDOM.unmountComponentAtNode(div)
   })
})
