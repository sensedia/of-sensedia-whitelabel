import React from 'react'
import ModalSelectInstitution from '.'
import { render } from '@testing-library/react'
import { QueryClient, QueryClientProvider } from 'react-query'
import userEvent from '@testing-library/user-event'

describe('<ModalSelectInstitution />', () => {
   it('render correctly', () => {
      const mockFn = jest.fn()
      const queryClient = new QueryClient()

      const { getByTestId } = render(
         <QueryClientProvider client={queryClient}>
            <ModalSelectInstitution
               isOpen={true}
               changeInstitution={() => mockFn()}
               closeModal={() => mockFn()}
            />
         </QueryClientProvider>,
      )

      expect(getByTestId('wrapper-main')).toBeTruthy()
   })
   it('test actions', () => {
      const mockFn = jest.fn()
      const queryClient = new QueryClient()

      const { getByTestId } = render(
         <QueryClientProvider client={queryClient}>
            <ModalSelectInstitution
               isOpen={true}
               changeInstitution={() => mockFn()}
               closeModal={() => mockFn()}
            />
         </QueryClientProvider>,
      )

      userEvent.click(getByTestId('button-back'))
      expect(mockFn).toBeCalled()

      userEvent.type(getByTestId('field-filter'), 'testing')
      userEvent.click(getByTestId('button-clear-filter'))
   })
})
