/* eslint-disable react/no-children-prop */
import { Institutions } from '@services'
import { Institution } from '@models'
import { useQuery } from 'react-query'
import {
   Box,
   Text,
   Stack,
   useToast,
   Flex,
   InputGroup,
   InputLeftElement,
   Input,
   InputRightElement,
   Divider,
   Link,
   ModalContent,
   Modal,
   ModalBody,
   ModalOverlay,
   ModalHeader,
   ModalCloseButton,
   ModalFooter,
   UnorderedList,
   ListItem,
} from '@chakra-ui/react'
import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { ChevronLeftIcon, CloseIcon, SearchIcon } from '@chakra-ui/icons'
import { useUpdateEffect } from '@hooks'
import { CardShareSkeleton, Logo } from '@components'
import theme from 'styles/theme'

type ModalProps = {
   isOpen: boolean
   closeModal: () => void
   changeInstitution: (value: Institution) => void
   title: string
   description?: string
}

function ModalSelectInstitution({
   isOpen,
   closeModal,
   changeInstitution,
   title,
   description,
}: ModalProps) {
   const toast = useToast()
   const { t } = useTranslation()
   const institutionsQuery = useQuery('institutionData', Institutions.get)
   const participantsQuery = useQuery(
      'participantData',
      Institutions.getParticipants,
   )
   const [institution, setInstitution] = useState<Institution>()
   const [institutions, setInstitutions] = useState<Institution[] | undefined>()
   const [participants, setParticipants] = useState<Institution[]>([])
   const [modalIsOpen, setModalIsOpen] = useState<boolean>(false)
   const [filter, setFilter] = useState('')

   useUpdateEffect(() => {
      if (!institutionsQuery.isLoading && !institutionsQuery.error) {
         const dataInstitutions = institutionsQuery.data?.data
         const dataParticipants = participants

         dataInstitutions?.forEach((institution) => {
            if (institution.brands.length > 0) {
               dataParticipants?.push(institution)

               institution.brands.forEach((brand) => {
                  dataInstitutions.push(brand)
               })
            }
         })
         setParticipants(dataParticipants)
         setInstitutions(dataInstitutions)
      } else if (institutionsQuery.error && !institutionsQuery.isLoading) {
         toast({
            title: t('default.request.error'),
            status: 'error',
            isClosable: true,
         })
      }
   }, [institutionsQuery.isLoading])

   const institutionsFilter = institutions?.length
      ? institutions.filter((x) =>
           x.customerFriendlyName.toLowerCase().includes(filter.toLowerCase()),
        )
      : []
   const participantsFilter = participants?.length
      ? participants.filter((x) =>
           x.customerFriendlyName.toLowerCase().includes(filter.toLowerCase()),
        )
      : []

   const onPressClearSearch = () => {
      setFilter('')
   }

   const openModal = (part: Institution) => {
      setModalIsOpen(true)
      setInstitution(part)
   }

   return (
      <Modal isOpen={isOpen} onClose={() => closeModal()} size="full">
         <ModalContent my="0rem" bg="gray.200">
            <Flex
               width="100%"
               margin="0 auto"
               pb="0.7rem"
               bg="brand.600"
               justifyContent="center"
               alignItems="center"
               height="4.7rem"
            >
               <Logo />
            </Flex>
            <Flex
               width="100%"
               margin="0 auto"
               bg="white"
               justifyContent="center"
            >
               <Stack bg="white" mt="1rem" maxWidth="40rem" width="100%">
                  <Flex px="1.25rem" alignItems="center">
                     <ChevronLeftIcon
                        color="brand.600"
                        w={8}
                        h={8}
                        onClick={() => closeModal()}
                        data-testid="button-back"
                        cursor="pointer"
                        ml="-0.5rem"
                     />
                     <Text
                        fontSize="2xl"
                        fontWeight={700}
                        textAlign="center"
                        color="gray.900"
                        mb="1rem"
                        w="calc(100% - 3.75rem)"
                        m="0rem"
                     >
                        {title}
                     </Text>
                  </Flex>
                  {description && (
                     <Text
                        fontSize="md"
                        color="gray.800"
                        mb="1rem"
                        px="1.25rem"
                     >
                        {description}
                     </Text>
                  )}
                  <Stack px="1.25rem" pb="1.25rem" pt="0.313rem">
                     <InputGroup>
                        <InputLeftElement
                           pointerEvents="none"
                           children={<SearchIcon color="gray.600" />}
                        />
                        <Input
                           type="text"
                           color="gray.900"
                           value={filter}
                           onChange={(event) => setFilter(event.target.value)}
                           data-testid="field-filter"
                        />
                        {filter !== '' && (
                           <InputRightElement
                              children={
                                 <CloseIcon
                                    color="brand.600"
                                    cursor="pointer"
                                    onClick={() => onPressClearSearch()}
                                 />
                              }
                              data-testid="button-clear-filter"
                           />
                        )}
                     </InputGroup>
                  </Stack>
               </Stack>
            </Flex>
            <Box
               maxWidth="40rem"
               width="100%"
               data-testid="wrapper-main"
               margin="0 auto"
               bg="gray.200"
            >
               <Stack p="1.25rem" height="100%" bg="gray.200">
                  {institutionsQuery.isLoading ? (
                     <>
                        <CardShareSkeleton />
                        <CardShareSkeleton />
                        <CardShareSkeleton />
                     </>
                  ) : (
                     institutionsFilter.map((inst: Institution, index) => (
                        <Box
                           borderRadius="lg"
                           shadow="lg"
                           borderWidth="0.063rem"
                           overflow="hidden"
                           cursor="pointer"
                           key={index}
                        >
                           <Text
                              fontSize="xl"
                              color="brand.600"
                              fontWeight={700}
                              bg="white"
                              px="0.938rem"
                              py="1.25rem"
                              onClick={() => changeInstitution(inst)}
                           >
                              {inst.customerFriendlyName}
                           </Text>
                           <Divider borderColor="gray.200" />
                           <Stack bg="gray.100" py="0.625rem">
                              <Text
                                 as="u"
                                 fontSize="sm"
                                 textAlign="center"
                                 color="brand.600"
                                 fontWeight={700}
                                 onClick={() => openModal(inst)}
                              >
                                 {t('institution.page.link.brand.details')}
                              </Text>
                           </Stack>
                        </Box>
                     ))
                  )}

                  {participantsQuery.isLoading ? (
                     <>
                        <CardShareSkeleton />
                     </>
                  ) : (
                     participantsFilter.map((part: Institution, index) => (
                        <Box
                           borderRadius="lg"
                           shadow="lg"
                           borderWidth="0.063rem"
                           overflow="hidden"
                           key={index}
                           cursor="not-allowed"
                        >
                           <Text
                              fontSize="xl"
                              color="gray.900"
                              fontWeight={700}
                              bg="white"
                              p="0.938rem"
                           >
                              {part.customerFriendlyName}
                           </Text>
                           <Divider borderColor="gray.200" />
                           <Stack py="0.625rem" px="0.938rem" bg="white">
                              <Text fontSize="lg" color="gray.900">
                                 <Trans
                                    i18nKey={
                                       part.brands[0]?.customerFriendlyName
                                    }
                                 />
                              </Text>
                              <Text fontSize="sm" color="gray.900">
                                 {t('institution.page.brand.count', {
                                    count:
                                       part.brands.length <= 1
                                          ? 0
                                          : part.brands.length - 1,
                                 })}
                              </Text>
                           </Stack>
                           <Stack bg="gray.100" py="0.625rem">
                              <Link
                                 href="https://openbankingbrasil.org.br/"
                                 textAlign="center"
                                 isExternal
                              >
                                 <Text
                                    as="u"
                                    fontSize="sm"
                                    color="brand.600"
                                    fontWeight={700}
                                    textAlign="center"
                                 >
                                    {t(
                                       'institution.page.link.participant.details',
                                    )}
                                 </Text>
                              </Link>
                           </Stack>
                        </Box>
                     ))
                  )}
               </Stack>
               <Modal
                  isOpen={modalIsOpen}
                  onClose={() => setModalIsOpen(false)}
               >
                  <ModalOverlay />
                  <ModalContent margin="13vh 1.5rem" height="auto">
                     <ModalHeader>
                        <Text color="gray.900">
                           {institution?.customerFriendlyName}
                        </Text>
                        <Divider
                           position="absolute"
                           marginTop="0.75rem"
                           borderColor="gray.500"
                           left="0"
                        />
                     </ModalHeader>
                     <ModalCloseButton
                        paddingTop="0.2rem"
                        fontWeight="bold"
                        color="brand.600"
                     />
                     <ModalBody px="0rem">
                        <Box>
                           <Text
                              fontSize="md"
                              color="gray.900"
                              fontWeight={400}
                              py="0.625rem"
                              px="1.5rem"
                              lineHeight="1.5rem"
                           >
                              {institution?.customerFriendlyDescription}
                           </Text>
                           <Divider borderColor="gray.500" py="0.313rem" />
                           <Text
                              fontSize="sm"
                              color="brand.600"
                              py="0.938rem"
                              px="1.5rem"
                           >
                              <Trans
                                 i18nKey="institution.page.modal.brand.count"
                                 values={{
                                    count: institution?.brands.length,
                                    organization:
                                       institution?.customerFriendlyName,
                                 }}
                              />
                           </Text>

                           <Box
                              maxH="15.625rem"
                              overflow="scroll"
                              bg="gray.100"
                              mx="1.5rem"
                              px="0.938rem"
                              pt="0.938rem"
                              borderRadius="lg"
                              css={{
                                 '&::-webkit-scrollbar': {
                                    width: '0.25rem',
                                 },
                                 '&::-webkit-scrollbar-track': {
                                    width: '0.375rem',
                                 },
                                 '&::-webkit-scrollbar-thumb': {
                                    background: `${theme.colors.brand[600]}`,
                                    borderRadius: '1.5rem',
                                 },
                              }}
                           >
                              <UnorderedList>
                                 {institution?.brands?.map((brand, index) => (
                                    <ListItem
                                       color="gray.800"
                                       fontSize="sm"
                                       colorScheme="brand.600"
                                       key={index}
                                    >
                                       {brand.customerFriendlyName}
                                    </ListItem>
                                 ))}
                              </UnorderedList>
                           </Box>
                        </Box>
                     </ModalBody>
                     <ModalFooter px="0rem">
                        <Flex
                           direction="column"
                           alignItems="center"
                           w="100%"
                           bg="gray.100"
                           py="1.25rem"
                        >
                           <Text>{t('institution.page.modal.details')}</Text>
                           <Link
                              href="https://openbankingbrasil.org.br/"
                              textAlign="center"
                              isExternal
                           >
                              <Text
                                 as="u"
                                 fontSize="md"
                                 color="brand.600"
                                 fontWeight={700}
                                 textAlign="center"
                              >
                                 {t('institution.page.modal.link')}
                              </Text>
                           </Link>
                        </Flex>
                     </ModalFooter>
                  </ModalContent>
               </Modal>
            </Box>
         </ModalContent>
      </Modal>
   )
}

export default ModalSelectInstitution
