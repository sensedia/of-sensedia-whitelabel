import { Container } from '../Container'
import { Main } from '../Main'
import { ReactNode } from 'react'
import { Logo } from '../Logo'

type Props = {
   children: ReactNode
}

export function Layout({ children }: Props) {
   return (
      <Container
         height="100vh"
         justifyContent="space-between"
         overflowY="scroll"
      >
         <Logo />
         <Main> {children}</Main>
      </Container>
   )
}
