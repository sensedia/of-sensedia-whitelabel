import { useTranslation } from 'react-i18next'
import { Flex, Link as LinkStyle, Text } from '@chakra-ui/react'

interface ToggleSizeProps {
   onClick: () => void
   show: boolean
}

export function ToggleSize({ show, onClick }: ToggleSizeProps) {
   const { t } = useTranslation()
   return (
      <Flex
         align="center"
         bg="gray.100"
         borderBottomRadius="0.5rem"
         borderTopWidth="0.0625rem"
         borderColor="gray.200"
         justify="center"
         padding="1.1rem"
      >
         <LinkStyle onClick={onClick}>
            <Text
               color="brand.600"
               data-testid="button"
               fontWeight="bold"
               textDecoration="underline"
               textAlign="center"
               role="expand-collapse"
               fontSize="xs"
            >
               {show
                  ? t('card.component.collapse')
                  : t('card.component.expand')}
            </Text>
         </LinkStyle>
      </Flex>
   )
}
