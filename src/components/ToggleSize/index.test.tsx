import { ToggleSize } from '.'
import { fireEvent, screen, render } from '@testing-library/react'

const onClick = jest.fn()

describe('<ToggleSize />', () => {
   it('Should render correctly collapsed', () => {
      render(<ToggleSize onClick={onClick} show={false} />)

      expect(screen.queryByText('card.component.expand')).toBeTruthy()
   })

   it('Should trigger action correctly', () => {
      render(<ToggleSize onClick={onClick} show={false} />)

      fireEvent.click(screen.getByTestId('button'))
      expect(onClick).toBeCalled()
   })

   it('Should render correctly expanded', () => {
      render(<ToggleSize onClick={onClick} show={true} />)

      expect(screen.queryByText('card.component.collapse')).toBeTruthy()
   })
})
