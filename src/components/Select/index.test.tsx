import React from 'react'
import { SelectCustom } from '.'
import { fireEvent, render, screen } from '@testing-library/react'

describe('<SelectCustom />', () => {
   it('renders', async () => {
      const { getByText } = render(
         <SelectCustom
            label="teste"
            options={[
               { label: 'option1', value: 1 },
               { label: 'option2', value: 2 },
            ]}
            setValue={() => {}}
         />,
      )
      fireEvent.click(screen.getByTestId('select'))

      expect(getByText('option1')).toBeTruthy()
      expect(getByText('option2')).toBeTruthy()
   })

   it('returns custom function', async () => {
      const funcMock = jest.fn()
      const mock = new funcMock()

      const { getByText } = render(
         <SelectCustom
            label="teste"
            options={[
               { label: 'option1', value: 1 },
               { label: 'option2', value: 2 },
            ]}
            setValue={() => mock()}
         />,
      )

      fireEvent.click(screen.getByTestId('select'))
      fireEvent.click(getByText('option2'))

      expect(funcMock).toBeCalled()
   })
})
