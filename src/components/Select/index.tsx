import { Box, Select, Text } from '@chakra-ui/react'
import React from 'react'

type Option = {
   label: string
   value: number | string
}

type SelectProps = {
   options: Option[]
   setValue?: (val: number) => void
   label: string
}

export function SelectCustom(props: SelectProps) {
   function handleChange(event: any) {
      props.setValue && props.setValue(event.target.value)
   }

   return (
      <Box
         position="relative"
         w="100%"
         marginBottom="1.2rem"
         borderColor="gray.900"
         border="0.0625rem solid"
         bg="white"
         borderRadius="0.5rem"
      >
         <Select
            data-testid="select"
            size="lg"
            color="gray.900"
            pt="1rem"
            border="0"
            focusBorderColor="transparent"
            onChange={(event) => {
               handleChange(event)
            }}
         >
            {props.options.map((opt) => (
               <option value={opt.value} key={opt.value}>
                  {opt.label}
               </option>
            ))}
         </Select>
         <Text
            left="1rem"
            top="0.45rem"
            position="absolute"
            fontWeight="bold"
            color="gray.900"
            fontSize="xs"
         >
            {props.label}
         </Text>
      </Box>
   )
}
