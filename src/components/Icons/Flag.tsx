import { Icon } from '@chakra-ui/icons'
export const FlagIcon = (props: any) => (
   <Icon viewBox="-5 0 40 40" {...props}>
      <path
         fill="none"
         d="M1.66663 24.0002C1.66663 24.0002 3.33329 22.3335 8.3333 22.3335C13.3333 22.3335 16.6666 25.6668 21.6666 25.6668C26.6666 25.6668 28.3333 24.0002 28.3333 24.0002V4.00016C28.3333 4.00016 26.6666 5.66683 21.6666 5.66683C16.6666 5.66683 13.3333 2.3335 8.3333 2.3335C3.33329 2.3335 1.66663 4.00016 1.66663 4.00016V24.0002Z"
         stroke="currentColor"
         strokeWidth="3"
         strokeLinecap="round"
         strokeLinejoin="round"
      />
      <path
         d="M1.66663 35.6667V24"
         stroke="currentColor"
         strokeWidth="3"
         strokeLinecap="round"
         strokeLinejoin="round"
      />
      ),
   </Icon>
)
