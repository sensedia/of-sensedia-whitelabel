export const FAKE_API = process.env.NEXT_PUBLIC_FAKE_API as ''
export const SANDBOX_API = process.env.NEXT_PUBLIC_SANDBOX as ''
export const LOGGED_USER_API = process.env.NEXT_PUBLIC_LOGGED_USER as ''
export const ACCOUNTS_API = process.env.NEXT_PUBLIC_ACCOUNTS as ''
export const JOURNEY_TPP_API = process.env.NEXT_PUBLIC_JOURNEY_TPP_API
export const JOURNEY_ASPSP_API = process.env.NEXT_PUBLIC_JOURNEY_ASPSP_API

export const JOURNEY_TPP = process.env.NEXT_PUBLIC_TPP_API
export const JOURNEY_ASPSP = process.env.NEXT_PUBLIC_ASPSP_API
export const JANS_AUTH = 'https://auth-testing.sensedia.com/jans-auth/postlogin'

export const shareStatusEnum = {
   ACTIVE: 'ACTIVE',
   INACTIVE: 'INACTIVE',
   EXPIRED: 'EXPIRED',
   PENDING: 'PENDING',
   INTERRUPTED: 'INTERRUPTED',
}

export const openBankingSite = 'https://openbankingbrasil.org.br'
