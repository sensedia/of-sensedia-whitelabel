import { AccountType, BankAccount } from '@models'

export const accounts: BankAccount[] = [
   {
      accountId: '92792126019929279212650822221989319252576',
      ispb: '142536',
      issuer: '4451',
      number: '123456-8',
      accountType: AccountType.CACC,
      amount: '8200,00',
   },
   {
      accountId: '92792126019929279212650822221989319252578',
      ispb: '142536',
      issuer: '4451',
      number: '123456-8',
      accountType: AccountType.SVGS,
      amount: '500,00',
   },
]
