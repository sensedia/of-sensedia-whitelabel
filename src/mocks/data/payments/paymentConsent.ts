import { AccountType, ConsentStatus, PaymentConsent, PersonType } from '@models'

export const awaitingAuthPayment: PaymentConsent = {
   consentId: 'urn:sensediabank:C1DD33123',
   creationDateTime: '2021-07-26T08:30:00Z',
   expirationDateTime: '2021-07-26T08:30:00Z',
   statusUpdateDateTime: '2021-07-26T08:30:00Z',
   status: ConsentStatus.AwaitingAuthorisation,
   organisationName: 'Nuvem Pay',
   organisationId: 'abc123',
   loggedUser: {
      document: {
         identification: '57541842044',
         rel: 'CPF',
      },
      name: 'User Name',
      email: 'user@sensedia.com',
   },
   businessEntity: {
      document: {
         identification: '96177704000103',
         rel: 'CNPJ',
      },
   },
   creditor: {
      personType: PersonType.PessoaNatural,
      cpfCnpj: '31093962038',
      name: 'Juscelino Kubitschek',
   },
   payment: {
      type: 'PIX',
      date: '2021-07-26',
      currency: 'BRL',
      amount: '100.12',
   },
   debtorAccount: {
      ispb: '12345678',
      issuer: '1774',
      number: '1234567890',
      accountType: AccountType.CACC,
   },
}

export const rejectedPaymentConsent: PaymentConsent = {
   ...awaitingAuthPayment,
   status: ConsentStatus.Rejected,
}

export const authorisedPaymentConsent: PaymentConsent = {
   ...awaitingAuthPayment,
   status: ConsentStatus.Authorised,
}
