import { rest } from 'msw'
import { JOURNEY_ASPSP_API } from '@constants'

import {
   awaitingAuthPayment,
   rejectedPaymentConsent,
   authorisedPaymentConsent,
} from '@mocks/data/payments/paymentConsent'
import { DebtorAccount } from '@models'

interface AuthorisePatchReqBody {
   debtorAccount: DebtorAccount
}

export const paymentHandlers = [
   rest.get(`${JOURNEY_ASPSP_API}/payments/consents/:id`, (req, res, ctx) => {
      const { id } = req.params

      if (id === '456') {
         return res(ctx.status(200), ctx.json(rejectedPaymentConsent))
      } else if (id === '678') {
         return res(ctx.status(200), ctx.json(authorisedPaymentConsent))
      } else {
         return res(ctx.status(200), ctx.json(awaitingAuthPayment))
      }
   }),

   rest.patch<AuthorisePatchReqBody>(
      `${JOURNEY_ASPSP_API}/payments/consents/:id/authorised`,
      (req, res, ctx) => {
         if (!req.body.debtorAccount) {
            return res(
               ctx.status(400),
               ctx.json({ message: 'No debitor account provided' }),
            )
         }
         return res(
            ctx.status(200),
            ctx.json({
               redirectUri: '/pagamentos/efetivacao#code=123&state=456',
            }),
         )
      },
   ),
]
