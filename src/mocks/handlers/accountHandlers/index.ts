import { rest } from 'msw'
import { ACCOUNTS_API } from '@constants'

import { accounts } from '../../data/accounts/accounts'

export const accountsHandlers = [
   rest.get(`${ACCOUNTS_API}`, (_req, res, ctx) => {
      return res(ctx.status(200), ctx.json(accounts))
   }),
]
