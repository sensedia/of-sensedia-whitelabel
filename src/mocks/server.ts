import { setupServer } from 'msw/node'
import * as handlers from './handlers'

const allHandlers = Object.values(handlers).reduce((acc, handlers) => {
   return [...acc, ...handlers]
}, [])

export const server = setupServer(...allHandlers)
