export function calculateDeadlineDate(value: number) {
   const today = new Date()
   today.setMonth(today.getMonth() + Number(value))

   return templateDeadlineDate(today)
}

function addZero(numero: number) {
   if (numero <= 9) {
      return '0' + numero
   } else {
      return numero
   }
}

export function templateDeadlineDate(date: Date) {
   return (
      addZero(date.getDate()) +
      '/' +
      addZero(date.getMonth() + 1) +
      '/' +
      date.getFullYear()
   )
}
