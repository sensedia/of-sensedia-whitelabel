import { Share, ShareTransmitted } from '@models'

export function formatDateWithHour(stringDate: string): string {
   const date = new Date(stringDate)
   const formattedDate = new Intl.DateTimeFormat('pt-BR').format(date)
   const formattedTime = new Intl.DateTimeFormat('pt-BR', {
      hour: 'numeric',
      minute: 'numeric',
   }).format(date)

   return `${formattedDate} - ${formattedTime}`
}

export function formatDate(stringDate: string): string {
   const date = new Date(stringDate)

   return new Intl.DateTimeFormat('pt-BR').format(date)
}

export function sortSharesByDate<T extends Share | ShareTransmitted>(
   shares: Array<T>,
): Array<T> {
   const sort = shares?.sort((a, b) =>
      a.createDateTime < b.createDateTime
         ? 1
         : b.createDateTime < a.createDateTime
         ? -1
         : 0,
   )
   return sort
}
