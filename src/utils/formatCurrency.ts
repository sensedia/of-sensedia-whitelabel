export function formatCurrency(amount: number | string): string {
   let amountToFormat = amount

   if (typeof amountToFormat === 'string') {
      amountToFormat = Number(amountToFormat.replace(',', '.'))
   }

   return new Intl.NumberFormat('pt-BR', {
      style: 'currency',
      currency: 'BRL',
   }).format(amountToFormat)
}
