# Sensedia White Label

[![License: Apache 2.0](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue)](https://choosealicense.com/licenses/apache-2.0/)

<!-- TOC -->

- [Sensedia White Label](#sensedia-white-label)
- [Português](#português)
  - [Open Banking](#open-banking)
  - [White Label](#white-label)
- [English](#english)
  - [Open Banking](#open-banking-1)
  - [White Label](#white-label-1)
- [Technologies](#technologies)
- [Starting the development environment](#starting-the-development-environment)
- [Adding a new branding](#adding-a-new-branding)
- [Using Docker](#using-docker)
  - [Send image to Docker Hub](#send-image-to-docker-hub)
- [Using Helm](#using-helm)
- [Tutorials](#tutorials)
- [License](#license)

<!-- TOC -->

![GitHub Logo](images/api-management-platform-sensedia-features-apis.png)

# Português

## Open Banking

**Open Banking** é um conjunto de regras e tecnologias que permitirá o compartilhamento de dados e serviços de clientes entre instituições financeiras. Os dados pertencem aos clientes dessa forma o cliente vai ter total controle de seus dados e com que os mesmos podem ser compartilhados. O Open Banking visa facilitar e trazer melhores condições de negócios para clientes e instituições financeiras.

Mais informações em:

* https://openbankingbrasil.org.br
* https://openbanking-brasil.github.io/areadesenvolvedor/#introducao
* https://br.sensedia.com/open-banking-and-finance

## White Label

**White Label** é uma solução da Sensedia de código fonte aberto que pode ser usada como referência por qualquer empresa para facilitar a implantação da jornada de consentimento e gerenciamento de dados dos clientes no ambiente das instituições financeiras.

Com essa solução é possível criar e configurar rápidamente um fluxo de consentimento para Open Banking e adicionar a identidade visual desejada.

# English

## Open Banking

**Open Banking** is a set of rules and technologies that will allow the sharing of customer data and services between financial institutions. The data belongs to the customers so the customer will have full control of their data and with which it can be shared. Open Banking aims to facilitate and bring better business conditions for clients and financial institutions.

More information at:

* https://openbankingbrasil.org.br
* https://openbanking-brasil.github.io/areadesenvolvedor/#introduction
* https://sensedia.com/open-banking-and-finance

## White Label

**White Label** is Sensedia's open source solution that can be referenced by any company to facilitate the deployment of the consent journey and customer data management in the financial institutions environment.

With this solution it is possible to quickly create and configure a consent flow for Open Banking and add the desired visual identity.

# Technologies

- [Nextjs](https://nextjs.org/)
- [Reactjs](https://reactjs.org/)
- [React Query](https://react-query.tanstack.com/)
- [ChakraUI](https://chakra-ui.com/)
- [Emotionjs](https://emotion.sh/docs/introduction)

# Starting the development environment

To start the development environment it is necessary to have installed `Nodejs` and a package manager `npm` or `yarn` and after having cloned the project (see [Doc](CONTRIBUTING.md)) access the directory of project, switch to the `develop` branch and run the following commands:

Using `npm`:

```bash
npm install

npm run dev
```

Using `yarn`:

```bash
yarn install

yarn dev
```

A aplicação irá iniciar neste endereço: http://localhost:3000

# Adding a new branding

All color and design style settings can be done by changing the `src/styles/theme.ts` file. In this file it is possible to change from the color of a button to the spacing between components, texts, images, logos and fonts.

```typescript

const theme = extendTheme({
  colors: {
    'white': '#FFFFFF',
    'danger': '#D35210',
    'success': '#00EB78',

    brand: {
      50: "#FFFFF0",
      ...
    },
  },
  fonts,
  components: {
    Tabs: {
      defaultProps: {
        colorScheme: 'brand',
      }
    },
    Button: {
      defaultProps: {
        colorScheme: 'brand',
      }
    },
  },
})

```

# Using Docker

Install Docker following the instructions in [this tutorial](REQUIREMENTS.md).

Generate new image with command:

```bash
make image
```

Or:

```bash
docker build -t of-sensedia-whitelabel:latest .
```

List the Docker images:

```bash
docker images
```

Run a container:

```bash
docker run -d --rm -p 3000:5000 \
  --name whitelabel \
  of-sensedia-whitelabel:latest
```

Access http://localhost:3000.

More information about ``docker run`` command: https://docs.docker.com/engine/reference/run/

## Send image to Docker Hub

Run the command:

```
docker login -u ACCOUNT
```

Push image:

```
docker tag of-sensedia-whitelabel:latest sensedia/of-sensedia-whitelabel:latest

docker push sensedia/of-sensedia-whitelabel:latest
```

Access https://hub.docker.com/r/sensedia/of-sensedia-whitelabel

# Using Helm

See the follow docs:

* [Install kind, kubectl and helm](REQUIREMENTS.md)
* [Install White Label using helm chart](helm-charts/of-sensedia-whitelabel/README.md)
# Tutorials

* [How to contribute with this project](CONTRIBUTING.md)
* [Install requirements](REQUIREMENTS.md)
  
# License

MIT/Apache 2.0

Reference: https://choosealicense.com/licenses/apache-2.0/
