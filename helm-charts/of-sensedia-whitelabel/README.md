# of-sensedia-whitelabel

 ![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square)

Sensedia Open Finance White Label public module

# Prerequisites

Install ``kubectl`` and ``helm`` commands following the instructions on the documentation: https://sensedia.atlassian.net/wiki/spaces/CLARK/pages/415597313/Softwares%2Bteis%2Bao%2Btrabalho%2Bno%2Btime%2Bde%2BCloud%2BArchitect

# Installing the Chart

* First, access a Kubernetes cluster.

Export default values of chart to file ``values.yaml``:

```bash
helm show values helm-charts/of-sensedia-whitelabel > $HOME/values.yaml
```

* Change the values according to the need of the environment in ``$HOME/values.yaml`` file.

* Create namespace ``open-finance`` in Kubernetes cluster.

```bash
kubectl create namespace open-finance
```

* Test the installation with command:

```bash
helm upgrade --install of-sensedia-whitelabel -f $HOME/values.yaml helm-charts/of-sensedia-whitelabel -n open-finance --dry-run
```

* To install/upgrade the chart with the release name `of-sensedia-whitelabel`:

```bash
helm upgrade --install of-sensedia-whitelabel -f $HOME/values.yaml helm-charts/of-sensedia-whitelabel -n open-finance
```

These commands install chart on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

* List all releases using follow command:

```bash
helm list --all --all-namespaces

#or

helm list -f 'of-sensedia-whitelabel' -n open-finance
```

* See the history of versions of ``of-sensedia-whitelabel`` application with command.

```bash
helm history of-sensedia-whitelabel -n open-finance
```

## Uninstalling the Chart

To uninstall the `of-sensedia-whitelabel` deployment:

```bash
helm uninstall of-sensedia-whitelabel -n open-finance
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

> If the application is removed without the ``--keep-history`` option, the history will be lost and it will not be possible to roll back.

# Documentation of Helm Chart

* Install ``helm-docs`` command following the instructions on this page https://sensedia.atlassian.net/wiki/spaces/CLARK/pages/415597313/Softwares%2Bteis%2Bao%2Btrabalho%2Bno%2Btime%2Bde%2BCloud%2BArchitect

* Generate docs with ``helm-docs`` command.

```bash
cd of-sensedia-whitelabel

helm-docs
```

The markdown generation is entirely gotemplate driven. The tool parses metadata from charts and generates a number of sub-templates that can be referenced in a template file (by default ``README.md.gotmpl``). If no template file is provided, the tool has a default internal template that will generate a reasonably formatted README.

## Parameters

The following tables lists the configurable parameters of the chart and their default values.

Change the values according to the need of the environment in ``values.yaml`` file.

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| autoscaling.averageUtilization | int | `70` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `12` |  |
| autoscaling.minReplicas | int | `1` |  |
| fullnameOverride | string | `""` |  |
| healthCheckPath | string | `"/"` |  |
| image.pullPolicy | string | `"Always"` |  |
| image.repository | string | `"sensedia/of-sensedia-whitelabel"` |  |
| image.tag | string | `"lastest"` |  |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | list | `[]` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts | list | `[]` |  |
| ingress.tls | list | `[]` |  |
| livenessProbe.failureThreshold | int | `10` |  |
| livenessProbe.initialDelaySeconds | int | `90` |  |
| livenessProbe.periodSeconds | int | `10` |  |
| livenessProbe.successThreshold | int | `1` |  |
| livenessProbe.timeoutSeconds | int | `5` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| properties | object | `{}` |  |
| readinessProbe.failureThreshold | int | `3` |  |
| readinessProbe.initialDelaySeconds | int | `30` |  |
| readinessProbe.periodSeconds | int | `5` |  |
| readinessProbe.successThreshold | int | `5` |  |
| readinessProbe.timeoutSeconds | int | `5` |  |
| replicaCount | int | `1` |  |
| resources.limits.cpu | string | `"1"` |  |
| resources.limits.memory | string | `"1Gi"` |  |
| resources.requests.cpu | string | `"200m"` |  |
| resources.requests.memory | string | `"768Mi"` |  |
| service.annotations | list | `[]` |  |
| service.loadBalancerIP | string | `""` |  |
| service.port | int | `80` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceMonitor.additionalLabels | object | `{}` |  |
| serviceMonitor.enabled | bool | `false` |  |
| serviceMonitor.namespaceSelector | object | `{}` |  |
| serviceMonitor.path | string | `"/metrics"` |  |
| serviceMonitor.scrapeTimeout | string | `"5s"` |  |
| tolerations | list | `[]` |  |
| updateStrategy | object | `{}` |  |

