{{ template "chart.header" . }}
{{ template "chart.typeBadge" . }} {{ template "chart.versionBadge" . }}

{{ template "chart.description" . }}

# Prerequisites

Install ``kubectl`` and ``helm`` commands following the instructions on the documentation: https://sensedia.atlassian.net/wiki/spaces/CLARK/pages/415597313/Softwares%2Bteis%2Bao%2Btrabalho%2Bno%2Btime%2Bde%2BCloud%2BArchitect

# Installing the Chart

* First, access a Kubernetes cluster.

Export default values of chart to file ``values.yaml``:

```bash
helm show values helm-charts/{{ template "chart.name" . }} > $HOME/values.yaml
```

* Change the values according to the need of the environment in ``$HOME/values.yaml`` file.

* Create namespace ``open-finance`` in Kubernetes cluster.

```bash
kubectl create namespace open-finance
```

* Test the installation with command:

```bash
helm upgrade --install {{ template "chart.name" . }} -f $HOME/values.yaml helm-charts/{{ template "chart.name" . }} -n open-finance --dry-run
```

* To install/upgrade the chart with the release name `{{ template "chart.name" . }}`:

```bash
helm upgrade --install {{ template "chart.name" . }} -f $HOME/values.yaml helm-charts/{{ template "chart.name" . }} -n open-finance
```

These commands install chart on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

* List all releases using follow command:

```bash
helm list --all --all-namespaces

#or

helm list -f '{{ template "chart.name" . }}' -n open-finance
```

* See the history of versions of ``{{ template "chart.name" . }}`` application with command.

```bash
helm history {{ template "chart.name" . }} -n open-finance
```

## Uninstalling the Chart

To uninstall the `{{ template "chart.name" . }}` deployment:

```bash
helm uninstall {{ template "chart.name" . }} -n open-finance
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

> If the application is removed without the ``--keep-history`` option, the history will be lost and it will not be possible to roll back.

# Documentation of Helm Chart

* Install ``helm-docs`` command following the instructions on this page https://sensedia.atlassian.net/wiki/spaces/CLARK/pages/415597313/Softwares%2Bteis%2Bao%2Btrabalho%2Bno%2Btime%2Bde%2BCloud%2BArchitect

* Generate docs with ``helm-docs`` command.

```bash
cd {{ template "chart.name" . }}

helm-docs
```

The markdown generation is entirely gotemplate driven. The tool parses metadata from charts and generates a number of sub-templates that can be referenced in a template file (by default ``README.md.gotmpl``). If no template file is provided, the tool has a default internal template that will generate a reasonably formatted README.

## Parameters

The following tables lists the configurable parameters of the chart and their default values.

Change the values according to the need of the environment in ``values.yaml`` file.

{{ template "chart.valuesTable" . }}

